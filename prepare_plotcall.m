function [Data,dimVec,DataLabels,cfg] = prepare_plotcall(plotSort,result)
switch plotSort
	case 'multiplotER'
		cfg = [];
		dimVec = EEG.times;
		Data = mean(EEG.data,3);
		%use either all channels or the ones specified
		if isempty(result{1})
			DataLabels={EEG.chanlocs.labels};
		else
			DataLabels=strsplit(result{1});
		end
		% set layout file
		cfg.side = layouts((result{3})).name(1:end-4); % get the layout file
		
	case 'topoplotER'
		cfg=[];
		cfg.channel={'all'};
		cfg.interplimits ='head';
		cfg.interpolation='nearest';
		cfg.plotSort='ft_topoplotER';
		cfg.side = layouts((result{4})).name(1:end-4);; % get the layout file
		
		
		
		dimVec=1;
		
		if isempty(result{2})
			cindex = [1 length(EEG.times)];
		else
			[~,cindex,~] = closest(EEG.times, str2num(result{2}));
		end
		
		if numel(cindex)==1
			Data=mean(mean(EEG.data(:,cindex(1):cindex(1),:),3),2);
		else
			Data=mean(mean(EEG.data(:,cindex(1):cindex(2),:),3),2);
		end
		
		if isempty(result{1})
			DataLabels={EEG.chanlocs.labels};
		else
			DataLabels=strsplit(result{1});
		end
		
		% This gives the component maps
	case 'ICA topography'
		cfg=[];
		cfg.channel={'all' };
		cfg.interplimits ='head';
		cfg.interpolation='nearest';
		cfg.plotSort='ft_topoplotER';
		cfg.newfig='no';
		dimVec=1;
		figure;
		for k=1:EEG.nbchan
			subplot(4,5,k);
			Data=EEG.icawinv(:,k);
			DataLabels={EEG.chanlocs.labels};
			ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg)
		end
		
		% for each component the time course and the respective topography
		% are shown
	case 'ICA components'
		for comp=1:EEG.nbchan
			cfg=[];
			dimVec=EEG.times;
			
			Data=mean(EEG.icaact(comp,:,:),3);
			DataLabels={EEG.chanlocs.labels};
			cfg.newfig='no';
			figure
			subplot(2,1,1)
			plot(Data);
			title(['Component ' num2str(comp)]);
			subplot(2,1,2)
			cfg=[];
			cfg.channel={'all' };
			cfg.interplimits ='head';
			cfg.interpolation='nearest';
			cfg.plotSort='ft_topoplotER';
			cfg.newfig='no';
			dimVec=1;
			Data=EEG.icawinv(:,comp);
			DataLabels={EEG.chanlocs.labels};
			ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);
		end
	otherwise
end
end