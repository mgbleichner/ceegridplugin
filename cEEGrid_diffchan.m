% cEEGrid_diffchan(EEG,ch_minuend, ch_subtrahend); -  Computes difference channels.

%Usage:
% >> [OUTEEG, diff_chan] =cEEGrid_diffchan(INEEG, ch_minuend, ch_subtrahend);
%
% Inputs:
%   INEEG           - input EEG dataset
%   ch_minuend      - minuends (have to be given as cell array, e.g.:
%                      {{'R02'} {'R01' 'R02'}})
%   ch_subtrahend   - subtrahends (have to be given as cell array, e.g.:
%                      {{'R06' 'R07'} {'R07' 'R08'}})
%
%             -Minuends and subtrahends can only contain channelnames, that
%             exist in the given EEG data set. 
%             -The number of minuends and subtrahends must match.

% Outputs:
%   OUTEEG          - output EEG set (identical to INEEG)
%   diff_chan       - difference channels
%
% Example:
% [EEG, diff_chan] = cEEGrid_diffchan( EEG,{{'R02'},{'R01','R02'}}, {{'R06','R07'},{'R07','R08'}});
% will calculate R02 - (R06+R07)/2 AND (R01+R02)/2 - (R07+R08)/2 and return
% them in diff_chan
%
% See also:
%    EEGLAB


% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, diff_chan, com] = cEEGrid_diffchan(EEG,ch_minuend, ch_subtrahend)

com='';
%

diff_chan=[];
if numel(ch_minuend)~=numel(ch_subtrahend)
    fprintf(['Wrong inputs! Number of minuends and subtrahends must match!']);
    return
end

data_min=[];
data_sub=[];
%% find channels
for i=1:numel(ch_minuend)
    %     channel_num_min=[];
    %     channel_num_sub=[];
    [channel_num_min, ~] = match_str({EEG.chanlocs.labels}, ch_minuend{i});
    [channel_num_sub, ~] = match_str({EEG.chanlocs.labels}, ch_subtrahend{i});
    
    %% detect incorrect channelnames
    if numel(channel_num_min)~=numel(ch_minuend{i})||numel(channel_num_sub)~=numel(ch_subtrahend{i})
        %channelnames wrong
        wrong_in_str='';
        for k=1:numel(ch_minuend)
            for j=1:numel(ch_minuend{k})
                [check_str, ~] = match_str({EEG.chanlocs.labels}, ch_minuend{k}{j});
                if isempty(check_str)
                    if ~strcmp(wrong_in_str,'')
                         wrong_in_str=[wrong_in_str ', '];
                    end
                    wrong_in_str=[wrong_in_str ch_minuend{k}{j}];
                end
            end
            for j=1:numel(ch_subtrahend{k})
                [check_str, ~] = match_str({EEG.chanlocs.labels}, ch_subtrahend{k}{j});
                if isempty(check_str)
                    if ~strcmp(wrong_in_str,'')
                         wrong_in_str=[wrong_in_str ', '];
                    end
                    wrong_in_str=[wrong_in_str ch_subtrahend{k}{j}];
                end
            end
            
        end
        fprintf(['Wrong input! Incorrect channelnames: ' wrong_in_str '\n']);
        return;
    end
    %% compute difference channels
    data_min(i,:,:)=EEG.data(channel_num_min(1),:,:);
%     figure
%     hold on
    %     plot(mean(EEG.data(channel_num_min(1),:,:),3))
    for j=2:numel(channel_num_min)
        data_min(i,:,:)=data_min(i,:,:)+EEG.data(channel_num_min(j),:,:);
        %plot(mean(EEG.data(channel_num_min(j),:,:),3))
    end
    data_min(i,:,:)=data_min(i,:,:)./numel(channel_num_min);
%     plot(mean(data_min(i,:,:),3))
    %     hold off
    %     legend
    
    data_sub(i,:,:)=EEG.data(channel_num_sub(1),:,:);
    %     figure
    %     hold on
    %     plot(mean(EEG.data(channel_num_sub(1),:,:),3))
    for j=2:numel(channel_num_sub)
        data_sub(i,:,:)=data_sub(i,:,:)+EEG.data(channel_num_sub(j),:,:);
        %plot(mean(EEG.data(channel_num_sub(j),:,:),3))
    end
    data_sub(i,:,:)=data_sub(i,:,:)./numel(channel_num_sub);
%     plot(mean(data_sub(i,:,:),3))
    %     hold off
    %     legend
    
    diff_chan(i,:,:)=data_min(i,:,:)-data_sub(i,:,:);
%     plot(mean(diff_chan(i,:,:),3));
%     hold off
%     legend
%fprintf(['Done calculating difference channels.\n']);

com = sprintf('[EEG, diff_chan]=cEEGrid_diffchan( %s,{%s}, {%s});', inputname(1), vararg2str(ch_minuend), vararg2str(ch_subtrahend));
end



