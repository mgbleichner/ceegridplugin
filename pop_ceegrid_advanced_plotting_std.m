
% pop_ceegrid_advanced_plotting_std(nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> [STUDY, ALLEEG, com] =pop_ceegrid_basic_plotting(nargin);
%
% Inputs:
%   nargin        - The first argument is the STUDY structure containing
%					the dataset information, study designs etc.
%				  - The second argument is the ALLEEG structure containing
%					the single datasets
%
% For command line use (no GUI), this function works as follows:

% >> [STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, plot_function, content ,DataLabels,cfg, varargin);

% Inputs          - The first argument is the STUDY structure containing
%					the dataset information, study designs etc.
%				  - The second argument is the ALLEEG structure containing
%					the single datasets
%                 - plot_function - contains the plotting function:
%                   'multiplotER' or 'topoplotER'
%                 - content - can be 'erp' or 'spec' 
%                 - DataLabels - contains a cell array with channel names
%                 - cfg - contains the configurations for the plot. See
%                   cEEGrid_multiplot.m and cEEGrid_topoplot.m, can also be
%                   []
%                 - varargin - contains plotting parameters as key value
%                   pairs, see cEEGrid_multiplot.m and cEEGrid_topoplot.m
%                   if plot_function is 'topoplotER' the first argument has
%                   to be timepoints (single number or set of two values)
%
% Example:
%[STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, 'multiplotER', 'erp' ,{'R01','R02','R03','R04','R05','R06','R07','R08','L01','L02','L03','L04','L04a','L04b','L05','L06','L07','L08'},[], 'showlabels', 'yes');
%[STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, 'topoplotER', 'spec' ,{'R01','R02','R03','R04','R05','R06','R07','R08','L01','L02','L03','L04','L04a','L04b','L05','L06','L07','L08'},[],0, 'markersize', 3, 'markersymbol', '+');

% Outputs:
%   STUDY		  - output STUDY structure
%	ALLEEG		  - output ALLEEG structure
%	com			  - history of com
%
% See also:
%    pop_ceegrid_advanced_plotting(); pop_ceegrid_multiplot();
%    pop_ceegrid_topoplot(); ceegrid_advanced_plotting()
%
% Copyright (C) 2020 Bertille Somon
%
% This function allows to plot the cEEGrid data at the study level in the
% time domain (ERP) and frequency domain (SPEC) through the FieldTrip
% functions (topoplotER and multiplotER) in the cEEGrid layout.
%
%
% Usage:
%	>> [STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG);
%	% Opens the cEEGrid plotting pop-up
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [STUDY, ALLEEG, com] = pop_ceegrid_advanced_plotting_std(varargin)


com = ''; % this initialization ensure that the function will return something

if nargin < 1
    help pop_ceegrid_advanced _plotting_std;
    return;
end
% if the user press the cancel button

% check for required fieldtrip functions
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false);
end
if nargin <3
    % plotSort= varargin{1};
    % datatype = varargin{2}; %whether it is ERP or Spec
    % get all layouts from the layouts directory
    scriptName = mfilename('fullpath');
    [currentpath, filename, fileextension]= fileparts(scriptName);
    layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
    layoutoptions=[''];
    for k=1:numel(layouts)
        layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
        if k<numel(layouts)
            layoutoptions=strcat(layoutoptions,' | ');
        end
    end
    if ~ischar(varargin{1})
        if nargin < 2
            error('pop_chanplot(): You must provide ALLEEG and STUDY structures');
        end
        STUDY  = varargin{1};
        STUDY.tmphist = '';
        ALLEEG = varargin{2};
        fig_arg{1}{1} = STUDY;
        fig_arg{1}{2} = ALLEEG;
        
        guititle = 'cEEGrid plotting for studies';
        geom = { [1 1] [1 1]};
        % The ICA functionality doesn't work at the study level yet
        uilist   = { ...
            {'style' 'checkbox'   'enable'   'on' 'string' 'ERP'	    'Callback' 'pop_ceegrid_advanced_plotting_std(''erp'',gcf);'} ...
            {'style' 'checkbox'   'enable'   'on' 'string' 'Spec'		'Callback' 'pop_ceegrid_advanced_plotting_std(''spec'',gcf);'} ...
            %		{'style' 'checkbox'	  'enable'   'on' 'string' 'ICA'		    'Callback' 'pop_ceegrid_advanced_plotting_std(''ica'', gcf)'} ...
            {'style' 'pushbutton' 'enable'   'on' 'string' 'Plot chanels' 'Callback' 'pop_ceegrid_advanced_plotting_std(''multiplotER'',gcf);'} ...
            {'style' 'pushbutton' 'enable'   'on' 'string' 'Plot topo'    'Callback' 'pop_ceegrid_advanced_plotting_std(''topoplotER'',gcf);'}  ...
            };
        
        [result, userdat]=inputgui( 'geometry', geom , 'uilist', uilist, 'helpcom',...
            'pophelp(''pop_ceegrid_advanced_plotting_std'')', 'title', guititle,...
            'userdata', fig_arg);
        
        % history update
        % -------
        if ~isempty(userdat)
            STUDY = userdat{1}{1};
        end
        com = STUDY.tmphist;
        STUDY = rmfield(STUDY, 'tmphist');
        
        
    else
        hdl = varargin{2};  %figure handle
        userdat  = get(varargin{2}, 'userdat');
        STUDY   = userdat{1}{1};
        ALLEEG    = userdat{1}{2};
        try
            switch varargin{1}
                case {'erp', 'spec'}
                    %checks only one checkbox at the same time
                    if strcmp(varargin{1}, 'erp')
                        set(hdl.Children(5), 'Value', 0)
                        set(hdl.Children(7), 'Value', 1)
                    else
                        set(hdl.Children(5), 'Value', 1)
                        set(hdl.Children(7), 'Value', 0)
                    end
                    %Read and average the ERP data at the study level
                    % erpdata = [cell array] size nCond x nGroup
                    %			each cell contains a matrix of size nTimes x nChan x nSubj
                    [STUDY, erpdata, erptimes] = std_readdata(STUDY, ALLEEG, 'datatype', varargin{1},...
                        'channels', {STUDY.changrp(:).name});
                    % Average across subjects and transpose for FieldTrip:
                    % erpdata = [cell array] nCond x nGroup
                    %			each cell contains nChan x nTimes
                    % Concatenate conditions in one matrix
                    erpdata = cellfun(@(x)mean(x,3)', erpdata, 'UniformOutput', false);
                    ERP = cat(3, erpdata{1:end});
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    Conditions = cell(1, length(STUDY.design(STUDY.currentdesign).variable));
                    for iCond = 1:length(STUDY.design(STUDY.currentdesign).variable)  %Gives the number of variables
                        Conditions{iCond} = STUDY.design(STUDY.currentdesign).variable(iCond).value;
                        if strcmpi(STUDY.design(STUDY.currentdesign).variable(iCond).label, 'group')
                            ncols = [iCond size(Conditions{iCond},2)]; end
                        if strcmpi(STUDY.design(STUDY.currentdesign).variable(iCond).label, 'condition')
                            nlines = [iCond size(Conditions{iCond},2)]; end
                        idxCell = find(double(cellfun(@iscell, Conditions{iCond})));
                        if ~isempty(idxCell)
                            Conditions{iCond}{idxCell} = cell2mat(Conditions{iCond}{idxCell});
                        end
                    end
                    if length(STUDY.design(STUDY.currentdesign).variable)==1
                        ceCond = Conditions{1};
                    else
                        for iCol = 1:ncols(2)
                            for iLine = 1:nlines(2)
                                sCond = ncols(2)*(iLine-1)+iCol;
                                ceCond{sCond} = [Conditions{ncols(1)}{iCol}, '_', Conditions{nlines(1)}{iLine}];
                            end
                        end
                    end
                    userdat{1}{3} = ERP;
                    userdat{1}{4} = erptimes;
                    userdat{1}{5} = ceCond;
                    userdat{1}{6} = varargin{1};
                    set(hdl, 'userdat', userdat);
                case 'multiplotER'
                    hdl = varargin{2};  %figure handle
                    userdat  = get(varargin{2}, 'userdat');
                    STUDY   = userdat{1}{1};
                    ALLEEG    = userdat{1}{2};
                    ERP = userdat{1}{3};
                    erptimes = userdat{1}{4};
                    Cond = userdat{1}{5};
                    datatype = userdat{1}{6};
                   
                    Labels=STUDY.changrp(:).name;
                    [plot_call, DataLabels]= cEEGrid_multiplot_GUI(Labels,length(STUDY.changrp));  %% Calls GUI function
                    if isempty(DataLabels)
                        DataLabels={STUDY.changrp(:).name};
                    end
                    if ~isempty(plot_call) % if GUI cancel button was used, do not plot
                        

                        Data{1} = ERP;
                        Data{2} = Cond;
                        plot_call_fin =[plot_call ',''study'',''yes'',''dimVec'',erptimes'];
                        %Plot
                        eval(['cEEGrid_multiplot(Data,DataLabels' plot_call_fin ');']);
                        
                        %History update
                        a = ['[STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, '''...
                            varargin{1},''', ''' datatype ''' ', fastif(isempty(DataLabels),',[]',[',{' vararg2str(DataLabels) '}']) plot_call ');'];
                        STUDY.tmphist =  sprintf('%s\n%s',  STUDY.tmphist, a);
                        userdat{1}{1} = STUDY;
                        set(hdl, 'userdat', userdat);
                    end
                case 'topoplotER'
                    hdl = varargin{2};  %figure handle
                    userdat  = get(varargin{2}, 'userdat');
                    STUDY   = userdat{1}{1};
                    ALLEEG    = userdat{1}{2};
                    ERP = userdat{1}{3};
                    erptimes = userdat{1}{4};
                    Cond = userdat{1}{5};
                    datatype = userdat{1}{6};
                    

                    Labels=STUDY.changrp(:).name;
                    [plot_call_cfg,plot_call_vararg,timepoints, DataLabels]= cEEGrid_topoplot_GUI(Labels,...
                        length(STUDY.changrp),ALLEEG(1).xmin,ALLEEG(1).xmax); %% Calls GUI function
                    if isempty(DataLabels)
                        DataLabels={STUDY.changrp(:).name};
                    end
                    if ~isempty(plot_call_cfg) % if GUI cancel button was used, do not plot
    %                     if isempty(result{2})
    %                         cindex = [1 length(erptimes)];
    %                     else
    %                         [~,cindex,~] = closest(erptimes, str2num(result{2}));
    %                     end
                        [~,cindex,~] = closest(erptimes, timepoints);
%                         dimVec = 1;

                        if numel(cindex)==1
                            Data{1}=mean(ERP(:,cindex(1):cindex(1),:),2);
                            Data{2} = Cond;
                        else
                            Data{1}=mean(ERP(:,cindex(1):cindex(2),:),2);
                            Data{2} = Cond;
                        end

                        plot_call_fin =[plot_call_vararg ',''study'',''yes'''];
                        %Plot
                        eval(['cEEGrid_topoplot(Data,DataLabels' plot_call_cfg ', timepoints' plot_call_fin ');']);

%                       

                        %History update
                        a = ['[STUDY, ALLEEG] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG, '''...
                            varargin{1},''', ''' datatype ''' ', fastif(isempty(DataLabels),',[]',[',{' vararg2str(DataLabels) '}'])...
                            plot_call_cfg ',' vararg2str(timepoints) plot_call_vararg ');'];
           
                        STUDY.tmphist =  sprintf('%s\n%s',  STUDY.tmphist, a);
                        userdat{1}{1} = STUDY;
                        set(hdl, 'userdat', userdat);
                    end
                otherwise
                    display('Unknown plotting option.');
                    return;
            end
        catch
            eeglab_error;
        end
        
    end
else
    STUDY   = varargin{1};
    ALLEEG    = varargin{2};
    DataLabels = varargin{5};
    try
        %Read and average the ERP data at the study level
        % erpdata = [cell array] size nCond x nGroup
        %			each cell contains a matrix of size nTimes x nChan x nSubj
        [STUDY, erpdata, erptimes] = std_readdata(STUDY, ALLEEG, 'datatype', varargin{4},...
            'channels', {STUDY.changrp(:).name});
        % Average across subjects and transpose for FieldTrip:
        % erpdata = [cell array] nCond x nGroup
        %			each cell contains nChan x nTimes
        % Concatenate conditions in one matrix
        erpdata = cellfun(@(x)mean(x,3)', erpdata, 'UniformOutput', false);
        ERP = cat(3, erpdata{1:end});
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Conditions = cell(1, length(STUDY.design(STUDY.currentdesign).variable));
        for iCond = 1:length(STUDY.design(STUDY.currentdesign).variable)  %Gives the number of variables
            Conditions{iCond} = STUDY.design(STUDY.currentdesign).variable(iCond).value;
            if strcmpi(STUDY.design(STUDY.currentdesign).variable(iCond).label, 'group')
                ncols = [iCond size(Conditions{iCond},2)]; end
            if strcmpi(STUDY.design(STUDY.currentdesign).variable(iCond).label, 'condition')
                nlines = [iCond size(Conditions{iCond},2)]; end
            idxCell = find(double(cellfun(@iscell, Conditions{iCond})));
            if ~isempty(idxCell)
                Conditions{iCond}{idxCell} = cell2mat(Conditions{iCond}{idxCell});
            end
        end
        if length(STUDY.design(STUDY.currentdesign).variable)==1
            ceCond = Conditions{1};
        else
            for iCol = 1:ncols(2)
                for iLine = 1:nlines(2)
                    sCond = ncols(2)*(iLine-1)+iCol;
                    ceCond{sCond} = [Conditions{ncols(1)}{iCol}, '_', Conditions{nlines(1)}{iLine}];
                end
            end
        end
        
        switch varargin{3}
            case 'multiplotER'
                Data{1} = ERP;
                Data{2} = ceCond;
                cfg=varargin{6};
                if nargin == 6
                   
                    if isempty(cfg)
                        plot_call=',[]';
                    else
                        plot_call=[',' vararg2str(cfg)];
                    end
                else
                   
                    if isempty(cfg)
                        plot_call=[',[],' vararg2str(varargin(7:nargin))];
                    else
                        plot_call=[',' vararg2str(cfg) ',' vararg2str(varargin(7:nargin))];
                    end
                end
                
                plot_call =[ plot_call ',''study'',''yes'',''dimVec'',erptimes'];
                eval(['cEEGrid_multiplot(Data,DataLabels' plot_call ');']);
            case 'topoplotER'
                Data{1} = ERP;
                Data{2} = ceCond;
                cfg=varargin{6};
                timepoints=varargin{7}
                if isempty(cfg)
                    plot_call_cfg=',[]';%',[], timepoints';
                else
                    plot_call_cfg=[',' vararg2str(cfg) ];
                end
                if nargin < 8
                    
                    plot_call_vararg='';
                else
                    
                    plot_call_vararg=[',' vararg2str(varargin(8:nargin))];
                end
                plot_call_fin =[plot_call_vararg ',''study'',''yes'''];
                eval(['cEEGrid_topoplot(Data,DataLabels' plot_call_cfg ', timepoints' plot_call_fin ');']);
                
            otherwise
                display('Unknown plotting option.');
                return;
        end
    catch
        eeglab_error;
    end
end
end
