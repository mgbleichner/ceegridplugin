% cEEGrid_icaplot(INEEG, DataLabels,cfg); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =cEEGrid_icaplot(INEEG, DataLabels,cfg);
%
% Inputs:
%   INEEG         - input EEG dataset
%   DataLabels    - cell array of electrode names (e.g. {EEG.chanlocs.labels})
%   cfg           - contains the parameters for the fieldtrip function (see
%                   below)
% The configuration can have the following parameters
%   cfg.parameter          = field that contains the data to be plotted as color, for example 'avg', 'powspctrm' or 'cohspctrm' (default is automatic)
%   cfg.maskparameter      = field in the data to be used for masking of data. It should have alues between 0 and 1, where 0 corresponds to transparent.
%   cfg.xlim               = limit for 1st dimension in data (e.g., time),
%                            can be 'maxmin' or [xmin xmax] (default = 'maxmin')
%                            (actually not variable, since this function
%                            will only use one value in the 1st dimension
%   cfg.zlim               = limits for color dimension, 'maxmin', 'maxabs', 'zeromax', 'minzero', or [zmin zmax] (default = 'maxmin')
%   cfg.channel            = Nx1 cell-array with selection of channels (default = 'all'), see FT_CHANNELSELECTION for details
%   cfg.refchannel         = name of reference channel for visualising connectivity, can be 'gui'
%   cfg.baseline           = 'yes','no' or [time1 time2] (default = 'no'), see FT_TIMELOCKBASELINE or FT_FREQBASELINE
%   cfg.baselinetype       = 'absolute' or 'relative' (default = 'absolute')
%   cfg.trials             = 'all' or a selection given as a 1xN vector (default = 'all')
%   cfg.colormap           = any sized colormap, see COLORMAP
%   cfg.marker             = 'on', 'labels', 'numbers', 'off'
%   cfg.markersymbol       = channel marker symbol (default = 'o')
%   cfg.markercolor        = channel marker color (default = [0 0 0] (black))
%   cfg.markersize         = channel marker size (default = 2)
%   cfg.markerfontsize     = font size of channel labels (default = 8 pt)
%   cfg.highlight          = 'off', 'on', 'labels', 'numbers'
%   cfg.highlightchannel   =  Nx1 cell-array with selection of channels, or vector containing channel indices see FT_CHANNELSELECTION
%   cfg.highlightsymbol    = highlight marker symbol (default = 'o')
%   cfg.highlightcolor     = highlight marker color (default = [0 0 0] (black))
%   cfg.highlightsize      = highlight marker size (default = 6)
%   cfg.highlightfontsize  = highlight marker size (default = 8)
%   cfg.hotkeys            = enables hotkeys (pageup/pagedown/m) for dynamic zoom and translation (ctrl+) of the color limits
%   cfg.colorbar           = 'yes'
%                            'no' (default)
%                            'North'              inside plot box near top
%                            'South'              inside bottom
%                            'East'               inside right
%                            'West'               inside left
%                            'NorthOutside'       outside plot box near top
%                            'SouthOutside'       outside bottom
%                            'EastOutside'        outside right
%                            'WestOutside'        outside left
%   cfg.colorbartext       =  string indicating the text next to colorbar
%   cfg.interplimits       = limits for interpolation (default = 'head')
%                            'electrodes' to furthest electrode
%                            'head' to edge of head
%   cfg.interpolation      = 'linear','cubic','nearest','v4' (default = 'nearest') see GRIDDATA
%   cfg.style              = plot style (default = 'straight')
%                            'straight' colormap only
%                            'contour' contour lines only
%                            'both' (default) both colormap and contour lines
%                            'fill' constant color between lines
%                            'blank' only the head shape
%   cfg.gridscale          = scaling grid size (default = 67)
%                            determines resolution of figure
%   cfg.shading            = 'flat' or 'interp' (default = 'flat')
%   cfg.comment            = 'no', 'auto' or 'xlim' (default = 'auto')
%                            'auto': date, xparam and zparam limits are printed
%                            'xlim': only xparam limits are printed
%   cfg.commentpos         = string or two numbers, position of the comment (default = 'leftbottom')
%                            'lefttop' 'leftbottom' 'middletop' 'middlebottom' 'righttop' 'rightbottom'
%                            'title' to place comment as title
%                            'layout' to place comment as specified for COMNT in layout
%                            [x y] coordinates
%   cfg.interactive        = Interactive plot 'yes' or 'no' (default = 'yes')
%                            In a interactive plot you can select areas and produce a new
%                            interactive plot when a selected area is clicked. Multiple areas
%                            can be selected by holding down the SHIFT key.
%   cfg.directionality     = '', 'inflow' or 'outflow' specifies for
%                            connectivity measures whether the inflow into a
%                            node, or the outflow from a node is plotted. The
%                            (default) behavior of this option depends on the dimor
%                            of the input data (see below).
%   cfg.interpolatenan     = string 'yes', 'no' (default = 'yes')
%                            interpolate over channels containing NaNs
%   cfg.side               = string containing the name of the layout(e.g.
%                            'cEEGridLayout')
%   cfg.component          = Vector containing indizes of the components
%                            that are to be plotted
%   cfg.timecourses        = plot time courses above topographies, 'yes' or 'no'
%   cfg.sepplots           = Create seperate plots 'yes' or 'no'
%   cfg.subcount           = Number of subplots per figure
%   cfg.autogrid           = Calculate subplot grid dimensions close to a square number, 'yes' or 'no'
%   cfg.column_count       = Amount of subplot columns
%   cfg.showtitle          = Show component number in title, 'yes' or 'no'
%
% Some parameters can also be entered via console
% as key-value pairs. These parameters are:
% Parameter:         Var-Type   possible values     default values           description
%      'side'           'string'	 []             'cEEGridLayout'
%      'layout'         'struct'	 []             layout struct that is selected with 'side'
%      'component'	 	'real'       []             [1:18]
%      'timecourses'	'string'	 []             'no'
%      'timepoints'     'real'       []             [min(EEG.times) max(EEG.times)]
%      'sepplots'	 	'string'	 []             'no'
%      'subcount'	 	'real'       []             1
%      'autogrid'	 	'string'	 []             'yes'
%      'column_count'	'real'       []             3
%      'showtitle'	 	'string'	 []             'yes'
%      'channel'	 	'string'	 []             'all'
%      'interplimits'	'string'	 []             'head'
%      'interpolation'	'string'	 []             'nearest'
%      'plotSort'	 	'string'	 []             'ft_topoplotER'
%      'newfig'         'string'	 []             'no'
%      'style'          'string'	 []             'straight'
%      'fontsize'	 	'real'       []             8

%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = cEEGrid_icaplot(EEG, DataLabels,cfg,varargin)

com = ''; % this initialization ensure that the function will return something
if isempty(DataLabels)
    DataLabels={EEG.chanlocs.labels};
end
if nargin < 3
    help cEEGrid_icaplot;
    return;
end;
%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false);
end

%% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions={};
for k=1:numel(layouts)
    layoutoptions{k}=layouts(k).name(1:end-4);
end
%% Default values
if ~isfield(cfg,'side') cfg.side = 'cEEGridLayout';end
if ~isfield(cfg,'layout')
    layoutFile=load([currentpath filesep 'Layouts' filesep cfg.side]);
    fields=fieldnames(layoutFile);
    cfg.layout= layoutFile.(fields{1});
end
if ~isfield(cfg,'colorbar') cfg.colorbar='no';end
if ~isfield(cfg,'colorbartext')  cfg.colorbartext=''; end
if ~isfield(cfg,'comment') cfg.comment='auto'; end
if ~isfield(cfg,'component') cfg.component=1:EEG.nbchan; end
if ~isfield(cfg,'timecourses') cfg.timecourses='no'; end
if ~isfield(cfg,'timepoints') cfg.timepoints=[min(EEG.times) max(EEG.times)]; end
if ~isfield(cfg,'sepplots') cfg.sepplots='no'; end
if ~isfield(cfg,'subcount') cfg.subcount=3; end
if ~isfield(cfg,'autogrid') cfg.autogrid='yes'; end
if ~isfield(cfg,'column_count') cfg.column_count=3; end
if ~isfield(cfg,'showtitle') cfg.showtitle='yes'; end
if ~isfield(cfg,'channel') cfg.channel='all'; end
if ~isfield(cfg,'interplimits') cfg.interplimits='head'; end
if ~isfield(cfg,'interpolation') cfg.interpolation='nearest'; end
if ~isfield(cfg,'plotSort') cfg.plotSort='ft_topoplotER'; end
if ~isfield(cfg,'newfig') cfg.newfig='no'; end
if ~isfield(cfg,'style') cfg.style= 'straight'; end
if ~isfield(cfg,'fontsize') cfg.fontsize=8;end

if isempty(DataLabels)
    DataLabels={EEG.chanlocs.labels};% all channels by default
end
dimVec=1;

%% check inputs

fieldlist = {'side'	 	'string'	 [] 	 cfg.side;
    'layout'           'struct'	 [] 	 cfg.layout;
    'colorbar'         'string'	 [] 	 cfg.colorbar;
    'colorbartext'     'string'	 [] 	 cfg.colorbartext;
    'comment'          'string'	 [] 	 cfg.comment;
    'component'	 	'real'	     [] 	 cfg.component;
    'timecourses'	 	'string'	 [] 	 cfg.timecourses;
    'timepoints'       'real'       []      cfg.timepoints;
    'sepplots'         'string'	 [] 	 cfg.sepplots;
    'subcount'         'real'	     [] 	 cfg.subcount;
    'autogrid'         'string'	 [] 	 cfg.autogrid;
    'column_count'	 	'real'	     [] 	 cfg.column_count;
    'showtitle'	 	'string'	 [] 	 cfg.showtitle;
    'channel'          'string'	 [] 	 cfg.channel;
    'interplimits'	 	'string'	 [] 	 cfg.interplimits;
    'interpolation'	'string'	 [] 	 cfg.interpolation;
    'plotSort'         'string'	 [] 	 cfg.plotSort;
    'newfig'           'string'	 [] 	 cfg.newfig;
    'style'            'string'	 [] 	 cfg.style;
    'fontsize'         'real'	     [] 	 cfg.fontsize;
    
    };
[cfg varargin] = finputcheck( varargin, fieldlist, 'cEEGrid_filterplot', 'ignore');
%% Generating Figure/Subplot configurations

% get amount of figures
if strcmp(cfg.sepplots,'yes')
    fig_amount = ceil(size(cfg.component,2)/cfg.subcount);%amount of figures
    
    % get vector with number of plots each figure for subplot-for-loop
    plots_per_fig(1:(fig_amount-1))=cfg.subcount; %all figures but maybe the last one are filled with plots
    plots_per_fig(fig_amount)=size(cfg.component,2)-(cfg.subcount*(fig_amount-1));
else
    fig_amount =1;
    plots_per_fig=size(cfg.component,2);
end

% get subplot dimensions
if strcmp(cfg.autogrid,'yes') % calculates subplot grid dimensions close to a square number
    y=ceil(sqrt(plots_per_fig(1)));
    x=y;
    if (x*(y-1)>=plots_per_fig(1))
        y=y-1;
    end
else % otherwise calculate dimensions by given count of columns
    x=cfg.column_count;
    y=ceil(plots_per_fig(1)/x);
end

if strcmp(cfg.timecourses,'yes') % for plot of the time courses, number of rows has to be doubled
    % also, two vectors are created, containing
    % the subplot positions for topoplots and
    % time course plots
    y=y*2;
    for i=1:plots_per_fig(1)
        subplot_time_vec(i)=floor((i-1)/x)*x+i;
        subplot_topo_vec(i)=(floor((i-1)/x)+1)*x+i;
    end
else
    subplot_topo_vec=1:plots_per_fig(1);
end

%% plots
for plot_count=1:fig_amount
    figure;
    for comp=1:plots_per_fig(plot_count)
        comp_ind=(plot_count-1)*plots_per_fig(1)+comp; %get index of desired component
        subplot(y,x,subplot_topo_vec(comp));
        
        if ~strcmp(cfg.timecourses,'yes')
            if strcmp(cfg.showtitle, 'yes')
                title(['Component ' num2str(cfg.component(comp_ind))]);
            end
        end
        
        Data=EEG.icawinv(:,cfg.component(comp_ind));
        cfg.figure ='gcf'; % makes sure that it is plotted into the correct axis
        ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);
    end
    
    if strcmp(cfg.timecourses,'yes')
        
        [~,cindex,~] = closest(EEG.times, cfg.timepoints);
        if numel(cindex)==1
            timepoint_a=cindex(1);
            timepoint_b=cindex(1);
        else
            timepoint_a=cindex(1);
            timepoint_b=cindex(2);
        end
        
        for comp=1:plots_per_fig(plot_count)
            comp_ind=(plot_count-1)*plots_per_fig(1)+comp;
            subplot(y,x,subplot_time_vec(comp));
            
            Data=mean(EEG.icaact(cfg.component(comp_ind),[timepoint_a:timepoint_b],:),3);
            plot(EEG.times(timepoint_a:timepoint_b),Data);
            xlabel('time [ms]');
            if strcmp(cfg.showtitle, 'yes')
                title(['Component ' num2str(cfg.component(comp_ind))]);
            end
        end
    end
    
end


% return the string command
% -------------------------
com = sprintf(['cEEGrid_icaplot(' inputname(1) ',{' vararg2str(DataLabels) '},' vararg2str(cfg) ');']);


end
