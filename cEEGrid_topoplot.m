% cEEGrid_topoplot(INEEG,DataLabels,cfg,timepoints, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =cEEGrid_topoplot(INEEG,DataLabels,cfg,timepoints, nargin);
%
% Inputs:
%   INEEG         - input EEG dataset
%   DataLabels    - cell array of electrode names (e.g. {EEG.chanlocs.labels})
%   cfg           - contains the parameters for the fieldtrip function (see
%                   below)
%   timepoints    - Plotting ERP scalp maps at this latency. Can be a
%                   single or a set of two values.
% Additionally, the important cfg parameters can be added as inputs using a
% key value mechanism. To use that, just append " 'parameter', 'value' " in
% function call as shown in following examples:
%
% >> OUTEEG = cEEGrid_topoplot(EEG,{EEG.chanlocs.labels}, [],[0 1000],'highlight','labels','highlightchannel',2,'highlightfontsize',15);
% >> OUTEEG = cEEGrid_topoplot(EEG,{EEG.chanlocs.labels}, [],[0 1000],'highlight','on','highlightchannel',[1 2 3 4],'highlightsize',15);
% >> OUTEEG = cEEGrid_topoplot(EEG,{EEG.chanlocs.labels}, [],0,'highlight','numbers','highlightchannel',[1 2 3 5],'colorbar','NorthOutside');
%
% Parameters, that can be added that way are (for explanations see cfg parameters below):
% interactive, parameter, channel, side, newfig, interplimits, interpolation, style, shading, xlim, zlim,
% marker, markersymbol, markercolor, markersize, markerfontsize,
% highlight, highlightchannel, highlightsymbol, highlightcolor, highlightsize, highlightfontsize,
% colorbar, colorbartext, comment, fontsize
%
%
% The configuration can have the following parameters
%   cfg.parameter          = field that contains the data to be plotted as color, for example 'avg', 'powspctrm' or 'cohspctrm' (default is automatic)
%   cfg.maskparameter      = field in the data to be used for masking of data. It should have alues between 0 and 1, where 0 corresponds to transparent.
%   cfg.xlim               = limit for 1st dimension in data (e.g., time),
%                            can be 'maxmin' or [xmin xmax] (default = 'maxmin')
%                            (actually not variable, since this function
%                            will only use one value in the 1st dimension
%   cfg.zlim               = limits for color dimension, 'maxmin', 'maxabs', 'zeromax', 'minzero', or [zmin zmax] (default = 'maxmin')
%   cfg.channel            = Nx1 cell-array with selection of channels (default = 'all'), see FT_CHANNELSELECTION for details
%   cfg.refchannel         = name of reference channel for visualising connectivity, can be 'gui'
%   cfg.baseline           = 'yes','no' or [time1 time2] (default = 'no'), see FT_TIMELOCKBASELINE or FT_FREQBASELINE
%   cfg.baselinetype       = 'absolute' or 'relative' (default = 'absolute')
%   cfg.trials             = 'all' or a selection given as a 1xN vector (default = 'all')
%   cfg.colormap           = any sized colormap, see COLORMAP
%   cfg.marker             = 'on', 'labels', 'numbers', 'off'
%   cfg.markersymbol       = channel marker symbol (default = 'o')
%   cfg.markercolor        = channel marker color (default = [0 0 0] (black))
%   cfg.markersize         = channel marker size (default = 2)
%   cfg.markerfontsize     = font size of channel labels (default = 8 pt)
%   cfg.highlight          = 'off', 'on', 'labels', 'numbers'
%   cfg.highlightchannel   =  Nx1 cell-array with selection of channels, or vector containing channel indices see FT_CHANNELSELECTION
%   cfg.highlightsymbol    = highlight marker symbol (default = 'o')
%   cfg.highlightcolor     = highlight marker color (default = [0 0 0] (black))
%   cfg.highlightsize      = highlight marker size (default = 6)
%   cfg.highlightfontsize  = highlight marker size (default = 8)
%   cfg.hotkeys            = enables hotkeys (pageup/pagedown/m) for dynamic zoom and translation (ctrl+) of the color limits
%   cfg.colorbar           = 'yes'
%                            'no' (default)
%                            'North'              inside plot box near top
%                            'South'              inside bottom
%                            'East'               inside right
%                            'West'               inside left
%                            'NorthOutside'       outside plot box near top
%                            'SouthOutside'       outside bottom
%                            'EastOutside'        outside right
%                            'WestOutside'        outside left
%   cfg.colorbartext       =  string indicating the text next to colorbar
%   cfg.interplimits       = limits for interpolation (default = 'head')
%                            'electrodes' to furthest electrode
%                            'head' to edge of head
%   cfg.interpolation      = 'linear','cubic','nearest','v4' (default = 'nearest') see GRIDDATA
%   cfg.style              = plot style (default = 'straight')
%                            'straight' colormap only
%                            'contour' contour lines only
%                            'both' (default) both colormap and contour lines
%                            'fill' constant color between lines
%                            'blank' only the head shape
%   cfg.gridscale          = scaling grid size (default = 67)
%                            determines resolution of figure
%   cfg.shading            = 'flat' or 'interp' (default = 'flat')
%   cfg.comment            = 'no', 'auto' or 'xlim' (default = 'auto')
%                            'auto': date, xparam and zparam limits are printed
%                            'xlim': only xparam limits are printed
%   cfg.commentpos         = string or two numbers, position of the comment (default = 'leftbottom')
%                            'lefttop' 'leftbottom' 'middletop' 'middlebottom' 'righttop' 'rightbottom'
%                            'title' to place comment as title
%                            'layout' to place comment as specified for COMNT in layout
%                            [x y] coordinates
%   cfg.interactive        = Interactive plot 'yes' or 'no' (default = 'yes')
%                            In a interactive plot you can select areas and produce a new
%                            interactive plot when a selected area is clicked. Multiple areas
%                            can be selected by holding down the SHIFT key.
%   cfg.directionality     = '', 'inflow' or 'outflow' specifies for
%                            connectivity measures whether the inflow into a
%                            node, or the outflow from a node is plotted. The
%                            (default) behavior of this option depends on the dimor
%                            of the input data (see below).
%   cfg.interpolatenan     = string 'yes', 'no' (default = 'yes')
%                            interpolate over channels containing NaNs
%   cfg.side               = string containing the name of the layout(e.g.
%                            'cEEGridLayout')


% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = cEEGrid_topoplot(EEG,DataLabels,cfg,timepoints,varargin)
                 
com = ''; % this initialization ensure that the function will return something
% string of cfg from call
if isempty(cfg)
    call_cfg='[]';
else
    call_cfg=vararg2str(cfg);
end

if nargin < 4
    help cEEGrid_topoplot;
    return;
end;

%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false);
end
%%
% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions={};
for k=1:numel(layouts)
    layoutoptions{k}=layouts(k).name(1:end-4);
end

%default values
if ~isfield(cfg,'interactive') cfg.interactive='yes'; end
if ~isfield(cfg,'parameter') cfg.parameter ='amp';end

if ~isfield(cfg,'channel') cfg.channel='all';end
if ~isfield(cfg,'side') cfg.side = 'cEEGridLayout';end
if ~isfield(cfg,'plotSort') cfg.plotSort='ft_topoplotER';end
if ~isfield(cfg,'layout')
    layoutFile=load([currentpath filesep 'Layouts' filesep cfg.side]);
    fields=fieldnames(layoutFile);
    cfg.layout= layoutFile.(fields{1});
end
if ~isfield(cfg,'newfig') cfg.newfig='yes';end
if ~isfield(cfg,'interplimits') cfg.interplimits ='head';end
if ~isfield(cfg,'interpolation') cfg.interpolation='nearest';end
if ~isfield(cfg,'style') cfg.style = 'straight';end
if ~isfield(cfg,'shading') cfg.shading='flat';end
if ~isfield(cfg,'xlim') cfg.xlim='maxmin';end
if ~isfield(cfg,'zlim') cfg.zlim='maxmin';end

if ~isfield(cfg,'marker') cfg.marker='on';end
if ~isfield(cfg,'markersymbol') cfg.markersymbol='o';end
if ~isfield(cfg,'markercolor') cfg.markercolor=[0 0 0];end
if ~isfield(cfg,'markersize') cfg.markersize=2';end
if ~isfield(cfg,'markerfontsize') cfg.markerfontsize=8;end

if ~isfield(cfg,'highlight') cfg.highlight='off';end
if ~isfield(cfg,'highlightchannel') cfg.highlightchannel='all';end
if ~isfield(cfg,'highlightsymbol') cfg.highlightsymbol='o';end
if ~isfield(cfg,'highlightcolor') cfg.highlightcolor=[0 0 0];end
if ~isfield(cfg,'highlightsize') cfg.highlightsize=6;end
if ~isfield(cfg,'highlightfontsize') cfg.highlightfontsize=8;end

if ~isfield(cfg,'colorbar') cfg.colorbar='no';end
if ~isfield(cfg,'colorbartext') cfg.colorbartext='';end

if ~isfield(cfg,'comment') cfg.comment='auto';end
if ~isfield(cfg,'fontsize') cfg.fontsize=8;end
if ~isfield(cfg,'study');   cfg.study = 'no';end

%% parse input arguments for extra cfg options

p = inputParser;
%validScalarPosNum = @(x) isnumeric(x) && isscalar(x) && (x > 0);

% addParameter(p,'titel',fastif(~isempty(EEG.setname), [EEG.setname], ''));
%addParameter(p,'layoutname',cfg.side ,@(x) any(validatestring(x,layoutoptions)));
addParameter(p,'interactive',cfg.interactive,@(x) any(validatestring(x,{'yes', 'no'})));
addParameter(p,'parameter',cfg.parameter);
addParameter(p,'channel',cfg.channel);
addParameter(p,'side',cfg.side,@(x) any(validatestring(x,layoutoptions)));
addParameter(p,'newfig', cfg.newfig,@(x) any(validatestring(x,{'yes', 'no'})));
addParameter(p,'interplimits', cfg.interplimits,@(x) any(validatestring(x,{'head', 'electrodes'})));
addParameter(p,'interpolation', cfg.interpolation,@(x) any(validatestring(x,{'linear', 'cubic','nearest','v4'})));
addParameter(p,'style', cfg.style,@(x) any(validatestring(x,{'straight', 'contour','both','fill','blank'})));
addParameter(p,'shading', cfg.shading,@(x) any(validatestring(x,{'flat', 'interp'})));

addParameter(p,'xlim',cfg.xlim);
addParameter(p,'zlim',cfg.zlim);

addParameter(p,'marker',cfg.marker,@(x) any(validatestring(x,{'on', 'labels', 'numbers', 'off'})));
addParameter(p,'markersymbol',cfg.markersymbol);
addParameter(p,'markercolor',cfg.markercolor);
addParameter(p,'markersize',cfg.markersize,@(x) isnumeric(x) && isscalar(x) && (x > 0));
addParameter(p,'markerfontsize',cfg.markerfontsize,@(x) isnumeric(x) && isscalar(x) && (x > 0));

addParameter(p,'highlight',cfg.highlight,@(x) any(validatestring(x,{'on', 'labels', 'numbers', 'off'})));
addParameter(p,'highlightchannel',cfg.highlightchannel);
addParameter(p,'highlightsymbol',cfg.highlightsymbol);
addParameter(p,'highlightcolor',cfg.highlightcolor);
addParameter(p,'highlightsize',cfg.highlightsize,@(x) isnumeric(x) && isscalar(x) && (x > 0));
addParameter(p,'highlightfontsize',cfg.highlightfontsize,@(x) isnumeric(x) && isscalar(x) && (x > 0));

addParameter(p,'colorbar',cfg.colorbar);
addParameter(p,'colorbartext',cfg.colorbartext);

addParameter(p,'comment',cfg.comment);
addParameter(p,'fontsize',cfg.fontsize);

addParameter(p,'study',cfg.study);

    parse(p,varargin{:});
    
    
    %% store input data to cfg:
    
%     EEG.setname=p.Results.titel;
    
    cfg.interactive=p.Results.interactive;
    cfg.parameter=p.Results.parameter;
    cfg.channel=p.Results.channel;
    cfg.side = p.Results.side;
    cfg.newfig=p.Results.newfig;
    cfg.interplimits=p.Results.interplimits;
    cfg.interpolation=p.Results.interpolation;
    cfg.style=p.Results.style;
    cfg.shading=p.Results.shading;
    
    cfg.xlim=p.Results.xlim;
    cfg.zlim=p.Results.zlim;
    
    cfg.marker=p.Results.marker;
    cfg.markersymbol=p.Results.markersymbol;
    cfg.markercolor=p.Results.markercolor;
    cfg.markersize=p.Results.markersize;
    cfg.markerfontsize=p.Results.markerfontsize;
    
    cfg.highlight=p.Results.highlight;
    cfg.highlightchannel=p.Results.highlightchannel;
    cfg.highlightsymbol=p.Results.highlightsymbol;
    cfg.highlightcolor=p.Results.highlightcolor;
    cfg.highlightsize=p.Results.highlightsize;
    cfg.highlightfontsize=p.Results.highlightfontsize;
    
    cfg.colorbar=p.Results.colorbar;
    cfg.colorbartext=p.Results.colorbartext;
    
    cfg.comment=p.Results.comment;
    cfg.fontsize=p.Results.fontsize;
    
    cfg.study=p.Results.study;
 
%DataLabels={EEG.chanlocs.labels};

%% process data for advanced plotting function

if strcmp(cfg.study,'yes') %check if function is called from pop_ceegrid_advanced_plotting_std
   
    Data=EEG;
else
    if isempty(DataLabels)
        DataLabels={EEG.chanlocs.labels};
    end
    [x,cindex,z] = closest(EEG.times, timepoints);
    if numel(cindex)==1
        Data=mean(mean(EEG.data(:,cindex(1):cindex(1),:),3),2);
    else
        Data=mean(mean(EEG.data(:,cindex(1):cindex(2),:),3),2);
    end
end

dimVec=1;

ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);


% return the string command
% -------------------------
com = sprintf(['cEEGrid_topoplot(' inputname(1) ',{' vararg2str(DataLabels) '},' call_cfg ',' vararg2str(timepoints) fastif(strcmp(vararg2str(varargin),''), '' ,',') vararg2str(varargin) ');']);
return;
end