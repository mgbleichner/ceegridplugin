% pop_cEEGrid_topoplot(INEEG,DataLabels,cfg,timepoints, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
%
% Usage:
% >> OUTEEG =pop_cEEGrid_topoplot(INEEG,DataLabels,cfg,timepoints, nargin);
%
% Inputs:
%   INEEG         - input EEG dataset
%   DataLabels    - cell array of electrode names (e.g. {EEG.chanlocs.labels})
%   cfg           - contains the parameters for the fieldtrip function (see
%                   below)
%   timepoints    - Plotting ERP scalp maps at this latency. Can be a
%                   single or a set of two values.
% Additionally, the important cfg parameters can be added as inputs using a
% key value mechanism. To use that, just append " 'parameter', 'value' " in
% function call as shown in following examples:
%
% >> OUTEEG = pop_cEEGrid_topoplot(EEG,{EEG.chanlocs.labels}, [],[0 1000],'highlight','labels','highlightchannel',2,'highlightfontsize',15);
% >> OUTEEG = pop_cEEGrid_topoplot(EEG,{EEG.chanlocs.labels}, [],[0 1000],'highlight','on','highlightchannel',[1 2 3 4],'highlightsize',15);
% >> OUTEEG = pop_cEEGrid_topoplot(EEG,{EEG.chanlocs.labels}, [],0,'highlight','numbers','highlightchannel',[1 2 3 5],'colorbar','NorthOutside');
%
% Parameters, that can be added that way are (for explanations see cfg parameters below):
% interactive, parameter, channel, side, newfig, interplimits, interpolation, style, shading, xlim, zlim,
% marker, markersymbol, markercolor, markersize, markerfontsize,
% highlight, highlightchannel, highlightsymbol, highlightcolor, highlightsize, highlightfontsize,
% colorbar, colorbartext, comment, fontsize
%
%
% The configuration can have the following parameters
%   cfg.parameter          = field that contains the data to be plotted as color, for example 'avg', 'powspctrm' or 'cohspctrm' (default is automatic)
%   cfg.maskparameter      = field in the data to be used for masking of data. It should have alues between 0 and 1, where 0 corresponds to transparent.
%   cfg.xlim               = limit for 1st dimension in data (e.g., time),
%                            can be 'maxmin' or [xmin xmax] (default = 'maxmin')
%                            (actually not variable, since this function
%                            will only use one value in the 1st dimension
%   cfg.zlim               = limits for color dimension, 'maxmin', 'maxabs', 'zeromax', 'minzero', or [zmin zmax] (default = 'maxmin')
%   cfg.channel            = Nx1 cell-array with selection of channels (default = 'all'), see FT_CHANNELSELECTION for details
%   cfg.refchannel         = name of reference channel for visualising connectivity, can be 'gui'
%   cfg.baseline           = 'yes','no' or [time1 time2] (default = 'no'), see FT_TIMELOCKBASELINE or FT_FREQBASELINE
%   cfg.baselinetype       = 'absolute' or 'relative' (default = 'absolute')
%   cfg.trials             = 'all' or a selection given as a 1xN vector (default = 'all')
%   cfg.colormap           = any sized colormap, see COLORMAP
%   cfg.marker             = 'on', 'labels', 'numbers', 'off'
%   cfg.markersymbol       = channel marker symbol (default = 'o')
%   cfg.markercolor        = channel marker color (default = [0 0 0] (black))
%   cfg.markersize         = channel marker size (default = 2)
%   cfg.markerfontsize     = font size of channel labels (default = 8 pt)
%   cfg.highlight          = 'off', 'on', 'labels', 'numbers'
%   cfg.highlightchannel   =  Nx1 cell-array with selection of channels, or vector containing channel indices see FT_CHANNELSELECTION
%   cfg.highlightsymbol    = highlight marker symbol (default = 'o')
%   cfg.highlightcolor     = highlight marker color (default = [0 0 0] (black))
%   cfg.highlightsize      = highlight marker size (default = 6)
%   cfg.highlightfontsize  = highlight marker size (default = 8)
%   cfg.hotkeys            = enables hotkeys (pageup/pagedown/m) for dynamic zoom and translation (ctrl+) of the color limits
%   cfg.colorbar           = 'yes'
%                            'no' (default)
%                            'North'              inside plot box near top
%                            'South'              inside bottom
%                            'East'               inside right
%                            'West'               inside left
%                            'NorthOutside'       outside plot box near top
%                            'SouthOutside'       outside bottom
%                            'EastOutside'        outside right
%                            'WestOutside'        outside left
%   cfg.colorbartext       =  string indicating the text next to colorbar
%   cfg.interplimits       = limits for interpolation (default = 'head')
%                            'electrodes' to furthest electrode
%                            'head' to edge of head
%   cfg.interpolation      = 'linear','cubic','nearest','v4' (default = 'nearest') see GRIDDATA
%   cfg.style              = plot style (default = 'straight')
%                            'straight' colormap only
%                            'contour' contour lines only
%                            'both' (default) both colormap and contour lines
%                            'fill' constant color between lines
%                            'blank' only the head shape
%   cfg.gridscale          = scaling grid size (default = 67)
%                            determines resolution of figure
%   cfg.shading            = 'flat' or 'interp' (default = 'flat')
%   cfg.comment            = 'no', 'auto' or 'xlim' (default = 'auto')
%                            'auto': date, xparam and zparam limits are printed
%                            'xlim': only xparam limits are printed
%   cfg.commentpos         = string or two numbers, position of the comment (default = 'leftbottom')
%                            'lefttop' 'leftbottom' 'middletop' 'middlebottom' 'righttop' 'rightbottom'
%                            'title' to place comment as title
%                            'layout' to place comment as specified for COMNT in layout
%                            [x y] coordinates
%   cfg.interactive        = Interactive plot 'yes' or 'no' (default = 'yes')
%                            In a interactive plot you can select areas and produce a new
%                            interactive plot when a selected area is clicked. Multiple areas
%                            can be selected by holding down the SHIFT key.
%   cfg.directionality     = '', 'inflow' or 'outflow' specifies for
%                            connectivity measures whether the inflow into a
%                            node, or the outflow from a node is plotted. The
%                            (default) behavior of this option depends on the dimor
%                            of the input data (see below).
%   cfg.interpolatenan     = string 'yes', 'no' (default = 'yes')
%                            interpolate over channels containing NaNs
%   cfg.side               = string containing the name of the layout(e.g.
%                            'cEEGridLayout')

% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_topoplot(EEG,DataLabels, cfg, timepoints,varargin)

com = ''; % this initialization ensure that the function will return something

if nargin < 1
    help pop_cEEGrid_topoplot;
    return;
end
%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false);
end


if nargin < 2
%     %%

    Labels={EEG.chanlocs.labels};
    [plot_call_cfg,plot_call_vararg,timepoints, DataLabels]= cEEGrid_topoplot_GUI(Labels,EEG.nbchan,EEG.xmin,EEG.xmax);%% Calls GUI function
    if isempty(plot_call_cfg) % handles cancel event
        return
    end
else
    
    
   
    %cfg.side = layoutname;
    %DataLabels={EEG.chanlocs.labels};
    if isempty(cfg)
            plot_call_cfg=',[]';%',[], timepoints';
        else
            plot_call_cfg=[',' vararg2str(cfg) ];
    end
    if isempty(varargin)
             
        plot_call_vararg='';
    else
       
        plot_call_vararg=[',' vararg2str(varargin)];
    end
    
end
%pop_cEEGrid_topoplot(EEG,DataLabels, cfg, timepoints,varargin)
eval(['cEEGrid_topoplot(EEG,DataLabels' plot_call_cfg ', timepoints' plot_call_vararg ');']);
% if isempty(varargin)
%     [EEG, com]= cEEGrid_topoplot(EEG,DataLabels, cfg, timepoints);
% else
%     [EEG, com]= cEEGrid_topoplot(EEG,DataLabels, cfg, timepoints,varargin);
% end
%     [x,cindex,z] = closest(EEG.times, timepoints);
%     if numel(cindex)==1
%         Data=mean(mean(EEG.data(:,cindex(1):cindex(1),:),3),2);
%     else
%         Data=mean(mean(EEG.data(:,cindex(1):cindex(2),:),3),2);
%     end
%
%     cfg.channel={'all'};
%     cfg.interplimits ='head';
%     cfg.interpolation='nearest';
%     cfg.plotSort='ft_topoplotER';
%      dimVec=1;
%     ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg)


% return the string command
% -------------------------
%com = sprintf('pop_cEEGrid_topoplot( %s, %s);', inputname(1), vararg2str({cfg.side,timepoints}));
com = sprintf(['pop_cEEGrid_topoplot(' inputname(1) fastif(isempty(DataLabels),',[]',[',{' vararg2str(DataLabels) '}']) plot_call_cfg ',' vararg2str(timepoints) plot_call_vararg ');']);
%com = sprintf(['pop_cEEGrid_topoplot(' inputname(1) ',{' vararg2str(DataLabels) '},' vararg2str(cfg) ',' vararg2str(timepoints) fastif(strcmp(vararg2str(varargin),''), '' ,',') vararg2str(varargin) ');']);
return;