% pop_cEEGrid_filterplot(INEEG,cfg_filt,varargin); -  plots filtered data over time.
%
% Usage:
% >> [OUTEEG,EEG_filter_dat] =pop_cEEGrid_filterplot(INEEG,cfg_filt,varargin);
%
% Inputs:
%   INEEG         - input EEG dataset
%
%   cfg_filt       - contains the parameters for the filter boundaries and
%                   smoothing
%   cfg_filt parameters:
% Parameter:         Var-Type   possible values                default values           description
% 'raw_lo'           'real'     [0 Inf]                        0.1
% 'raw_hi'           'real'     [0 Inf]                        45  
% 'delta_lo'         'real'     [0 Inf]                        0.5 
% 'delta_hi'         'real'     [0 Inf]                        3.5
% 'theta_lo'         'real'     [0 Inf]                        4
% 'theta_hi'         'real'     [0 Inf]                        7.5
% 'alpha_lo'         'real'     [0 Inf]                        8
% 'alpha_hi'         'real'     [0 Inf]                        12
% 'beta_lo'          'real'     [0 Inf]                        13
% 'beta_hi'          'real'     [0 Inf]                        35
% 'trials'           'integer'  e.g. [1 2 3 4 5 6]             vector with all trials
% 'channels'         'integer'  e.g. [1 2 3 4 5 6]             vector with all channels
% 'do_smooth_time'   'string'   {'yes' 'no'}                   'no'
% 'do_smooth_spat'   'string'   {'yes' 'no'}                   'no'
% 'smooth_time_val'  'real'     []                             50 ;
% 'smooth_spat_fact' 'real'     []                             1 ;
% 'side'             'string'   []                             'cEEGridLayout'
% 'layout'           'struct'   []                             layout struct that is selected with 'side'
% 'plot_filter'      'string'   {'yes' 'no'}                   'no'                     show plots from filters
% 'xlabel'           'string'   {'samples','time'}             'samples'
% 'use_raw'          'string'   {'yes' 'no'}                   'yes'                    use filters
% 'use_delta'        'string'   {'yes' 'no'}                   'yes'
% 'use_theta'        'string'   {'yes' 'no'}                   'yes'
% 'use_alpha'        'string'   {'yes' 'no'}                   'yes'
% 'use_beta'         'string'   {'yes' 'no'}                   'yes'
% 'conf_bound'       'real'     [0 Inf]                        3
% 'rem_outliers'     'integer'  [0 1]                          0
% 'time_range'       'real'     []                             [EEG.xmin*1000 EEG.xmax*1000]
% 'refchannel'       'integer'  []                             0
%
% Parameters can be passed to this function via key-value pair. E.g.:
% cEEGrid_filterplot(EEG,[], 'raw_lo', 0.2, 'trials', [1  2  3  5  6], 'xlabel', 'time', 'use_delta', 'no', 'rem_outliers', 1, 'conf_bound', 2.5)
%
% Parameters can also be passed via cfg_filt like:
% cfg_filt.raw_lo = 0.2;
% cfg_filt.trials=[1  2  3  5  6];
% cfg_filt.xlabel='time'
% cEEGrid_filterplot(EEG,cfg_filt)
% parameters passed via cfg_filt are overwritten by the key value pairs (if
% matching parameters are entered as key value pair.)
%
%  If only the INEEG is entered, the function will show a GUI to adjust the
%  input parameters for this function
%
% Outputs:
%   OUTEEG          - output EEG set (identical to INEEG)
%   EEG_filter_dat  - output datasets for different filters
%                       for example:
%                       EEG data from 'raw' filter can be found under:
%                       EEG_filter_dat.raw
% See also:
%    EEGLAB


% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG,EEG_filter_dat, com]=pop_cEEGrid_filterplot(EEG,cfg_filt,varargin)
com = '';
EEG_filter_dat=[];

%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false);
end
%%
if nargin < 1
    help pop_cEEGrid_filterplot;
    return;
end


%% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=[''];
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,'|');
    end
end

if nargin < 2
    %% GUI
    
    guititle='cEEGrid_filterplot -- pop_cEEGrid_filterplot';
    
    geom={[1 1] [1] [1] [2 1 1 1 1 1] [2 1 1 1 1 1] [2 1 1 1 1 1] [2 1 1 1 1 1] [2 1 1 1 1 1] [1] [1 1] [1] [1 1] [1] [1] [2 3 1] [2 3 1] [1] [1] [2 3 1 1]  [3 1] [1 1 2] [1 1 2]};
    
    txt_time_range=['Time range (two values between ' num2str(round(EEG.xmin*1000)) ' and ' num2str(round(EEG.xmax*1000)) ' ms)'];
    refoptions=['|'];
    
    for k=1:EEG.nbchan
        refoptions=strcat(refoptions,EEG.chanlocs(k).labels);
        if k<EEG.nbchan
            refoptions=strcat(refoptions,'|');
        end
    end
    
    uilist={
        { 'style', 'text', 'string', 'Select Grid:', 'fontweight', 'bold' },...
        { 'style', 'popupmenu', 'string', layoutoptions}...
        {}...
        { 'style'   'text'     'string' 'Filter values: ', 'fontweight', 'bold'  } ...
        { 'Style'   'text'     'string' 'Raw Data: ', 'fontweight', 'bold'}...
        { 'Style'   'text'     'string' 'low: '}...
        { 'Style',  'edit',    'string', '0.1' 'tag' 'raw_lo'  } ...
        { 'Style'   'text'     'string' 'high: '}...
        { 'Style',  'edit',    'string', '45' 'tag' 'raw_hi'  } ...
        { 'Style', 'checkbox', 'string' 'on' 'value' 1 'tag' 'use_raw' }...
        { 'Style'   'text'     'string' 'Delta: ', 'fontweight', 'bold'}...
        { 'Style'   'text'     'string' 'low: '}...
        { 'Style',  'edit',    'string', '0.5' 'tag' 'delta_lo'  } ...
        { 'Style'   'text'     'string' 'high: '}...
        { 'Style',  'edit',    'string', '3.5' 'tag' 'delta_hi'  } ...
        { 'Style', 'checkbox', 'string' 'on' 'value' 1 'tag' 'use_delta' }...
        { 'Style'   'text'     'string' 'Theta: ', 'fontweight', 'bold'}...
        { 'Style'   'text'     'string' 'low: '}...
        { 'Style',  'edit',    'string', '4' 'tag' 'theta_lo'  } ...
        { 'Style'   'text'     'string' 'high: '}...
        { 'Style',  'edit',    'string', '7.5' 'tag' 'theta_hi'  } ...
        { 'Style', 'checkbox', 'string' 'on' 'value' 1 'tag' 'use_theta' }...
        { 'Style'   'text'     'string' 'Alpha: ', 'fontweight', 'bold'}...
        { 'Style'   'text'     'string' 'low: '}...
        { 'Style',  'edit',    'string', '8' 'tag' 'alpha_lo'  } ...
        { 'Style'   'text'     'string' 'high: '}...
        { 'Style',  'edit',    'string', '12' 'tag' 'alpha_hi'  } ...
        { 'Style', 'checkbox', 'string' 'on' 'value' 1 'tag' 'use_alpha' }...
        { 'Style'   'text'     'string' 'Beta: ', 'fontweight', 'bold'}...
        { 'Style'   'text'     'string' 'low: '}...
        { 'Style',  'edit',    'string', '13' 'tag' 'beta_lo'  } ...
        { 'Style'   'text'     'string' 'high: '}...
        { 'Style',  'edit',    'string', '35' 'tag' 'beta_hi'  } ...
        { 'Style', 'checkbox', 'string' 'on' 'value' 1 'tag' 'use_beta' }...
        {}...
        { 'Style'   'text'     'string' 'Channels (E.g.: [1 2 3 5 6]): ', 'fontweight', 'bold'}...
        { 'Style',  'edit',    'string', '' 'tag' 'channels'  } ...
        { 'Style'   'text'     'string' '(Vector containing channel indizes ([]=all) '}...
        { 'Style'   'text'     'string' 'Trials (E.g.: [1 2 3 5 6]): ', 'fontweight', 'bold'}...
        { 'Style',  'edit',    'string', '' 'tag' 'trials'  } ...
        { 'Style'   'text'     'string' '(Vector containing trial indizes ([]=all) '}...
        { 'Style'   'text'     'string' 'Smoothing: ', 'fontweight', 'bold'}...
        { 'Style', 'checkbox', 'string' 'Smooth time dim.' 'value' 0 'tag' 'do_smooth_time' }, ...
        { 'Style'   'text'     'string' 'Amount of Values to smooth over: '}...
        { 'Style',  'edit',    'string', '50' 'tag' 'smooth_time_val'  } ...
        { 'Style', 'checkbox', 'string' 'Smooth spatial dim.' 'value' 0 'tag' 'do_smooth_spat' }, ...
        { 'Style'   'text'     'string' 'Size of the 2d gauss for convolution: '}...
        { 'Style',  'edit',    'string', '200' 'tag' 'smooth_spat_fact'  } ...
        { 'Style'   'text'     'string' ''}...
        { 'Style'   'text'     'string' 'Plot options: ', 'fontweight', 'bold'}...
        { 'Style', 'checkbox', 'string' 'Ignore outliers' 'value' 0 'tag' 'rem_outliers'}...
        { 'Style'   'text'     'string' 'Confidence interval (mean+-c*\sigma) c = '}...
        { 'Style',  'edit',    'string', '3' 'tag' 'conf_bound' }...
        {}...
        { 'Style'   'text'     'string' txt_time_range }...
        { 'Style',  'edit',    'string', '' 'tag' 'time_range'}...
        { 'Style'   'text'     'string' 'X-Axis data: '}...
        { 'style', 'popupmenu', 'string', 'Samples|Time' 'tag' 'xlabel'}...
        { 'Style', 'checkbox', 'string' 'Plot extra filter data' 'value' 0 'tag' 'plot_filter'}...
        { 'Style'   'text'     'string' 'Plot REF channel: '}...
        { 'style', 'popupmenu', 'string', refoptions  'tag' 'refchannel'}...
        { 'Style'   'text'     'string' '(no REF channel is plotted if left empty)'}...
        };
    %sprintf('(range: %d to %d ms, two values -> mean over time range):',round(EEG.xmin*1000), round(EEG.xmax*1000));
    
    [result userdat strhalt outstruct]=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_filterplot'')', guititle, [], 'normal');
    
    % catch cancelling-event
    if isempty(result), return; end
    %% Get values from GUI
    yes_no = { 'no','yes'};
    x_label={'samples','time'};
    cfg_filt.side = layouts((result{1})).name(1:end-4); % get the layout file
    cfg_filt.raw_lo=str2double(outstruct.raw_lo);
    cfg_filt.raw_hi=str2double(outstruct.raw_hi);
    cfg_filt.delta_lo=str2double(outstruct.delta_lo);
    cfg_filt.delta_hi=str2double(outstruct.delta_hi);
    cfg_filt.theta_lo=str2double(outstruct.theta_lo);
    cfg_filt.theta_hi=str2double(outstruct.theta_hi);
    cfg_filt.alpha_lo=str2double(outstruct.alpha_lo);
    cfg_filt.alpha_hi=str2double(outstruct.alpha_hi);
    cfg_filt.beta_lo=str2double(outstruct.beta_lo);
    cfg_filt.beta_hi=str2double(outstruct.beta_hi);
    if strcmp(outstruct.trials,'') %If trials input is empty, use all trials. Else convert input string to vector.
        cfg_filt.trials = [1:size(EEG.data,3)];
    else
        cfg_filt.trials = str2num(outstruct.trials);
    end
    if strcmp(outstruct.channels,'') %If trials input is empty, use all trials. Else convert input string to vector.
        cfg_filt.channels = [1:size(EEG.data,1)];
    else
        cfg_filt.channels = str2num(outstruct.channels);
    end
    cfg_filt.do_smooth_time = yes_no{outstruct.do_smooth_time+1};
    cfg_filt.do_smooth_spat = yes_no{outstruct.do_smooth_spat+1};
    cfg_filt.smooth_time_val= str2double(outstruct.smooth_time_val);
    cfg_filt.smooth_spat_fact =str2double(outstruct.smooth_spat_fact);
    cfg_filt.xlabel=x_label{outstruct.xlabel};
    cfg_filt.plot_filter= yes_no{outstruct.plot_filter+1};
    
    cfg_filt.use_raw=yes_no{outstruct.use_raw+1};
    cfg_filt.use_delta=yes_no{outstruct.use_delta+1};
    cfg_filt.use_theta=yes_no{outstruct.use_theta+1};
    cfg_filt.use_alpha=yes_no{outstruct.use_alpha+1};
    cfg_filt.use_beta=yes_no{outstruct.use_beta+1};
    
    cfg_filt.rem_outliers=outstruct.rem_outliers;
    cfg_filt.conf_bound=str2num(outstruct.conf_bound);
    if ~strcmp(outstruct.time_range,'')
        cfg_filt.time_range=str2num(outstruct.time_range);
    end
    cfg_filt.refchannel=outstruct.refchannel-1;
    
    %% Prepare function call
    % default reference values (taken from cEEGrid_filterplot.m)
    cfg_ref=[];
    if ~isfield(cfg_ref,'raw_lo')          cfg_ref.raw_lo     =0.1; end
    if ~isfield(cfg_ref,'raw_hi')          cfg_ref.raw_hi     =45; end
    if ~isfield(cfg_ref,'delta_lo')        cfg_ref.delta_lo   =0.5; end
    if ~isfield(cfg_ref,'delta_hi')        cfg_ref.delta_hi   =3.5; end
    if ~isfield(cfg_ref,'theta_lo')        cfg_ref.theta_lo   =4; end
    if ~isfield(cfg_ref,'theta_hi')        cfg_ref.theta_hi   =7.5; end
    if ~isfield(cfg_ref,'alpha_lo')        cfg_ref.alpha_lo   =8; end
    if ~isfield(cfg_ref,'alpha_hi')        cfg_ref.alpha_hi   =12; end
    if ~isfield(cfg_ref,'beta_lo')         cfg_ref.beta_lo    =13; end
    if ~isfield(cfg_ref,'beta_hi')         cfg_ref.beta_hi    =35; end
    if ~isfield(cfg_ref,'trials')          cfg_ref.trials     =[1:size(EEG.data,3)]; end
    if ~isfield(cfg_ref,'channels')        cfg_ref.channels   =[1:size(EEG.data,1)]; end
    if ~isfield(cfg_ref,'do_smooth_time')  cfg_ref.do_smooth_time='no'; end
    if ~isfield(cfg_ref,'do_smooth_spat')  cfg_ref.do_smooth_spat='no'; end
    if ~isfield(cfg_ref,'smooth_time_val') cfg_ref.smooth_time_val=50; end
    if ~isfield(cfg_ref,'smooth_spat_fact') cfg_ref.smooth_spat_fact = 200;end
    if ~isfield(cfg_ref,'side')            cfg_ref.side = 'cEEGridLayout';end
    if ~isfield(cfg_ref,'layout')
        layoutFile=load([currentpath filesep 'Layouts' filesep cfg_ref.side]);
        fields=fieldnames(layoutFile);
        cfg_ref.layout= layoutFile.(fields{1});
    end
    if ~isfield(cfg_ref,'plot_filter')     cfg_ref.plot_filter    = 'no';end
    if ~isfield(cfg_ref,'xlabel')          cfg_ref.xlabel         = 'samples';end
    
    if ~isfield(cfg_ref,'use_raw')         cfg_ref.use_raw        = 'yes';end
    if ~isfield(cfg_ref,'use_delta')       cfg_ref.use_delta      = 'yes';end
    if ~isfield(cfg_ref,'use_theta')       cfg_ref.use_theta      = 'yes';end
    if ~isfield(cfg_ref,'use_alpha')       cfg_ref.use_alpha      = 'yes';end
    if ~isfield(cfg_ref,'use_beta')        cfg_ref.use_beta       = 'yes';end
    if ~isfield(cfg_ref,'conf_bound')      cfg_ref.conf_bound     =3;end
    if ~isfield(cfg_ref,'rem_outliers')    cfg_ref.rem_outliers   =0;end
    if ~isfield(cfg_ref,'time_range')      cfg_ref.time_range     =[EEG.xmin*1000 EEG.xmax*1000];end
    if ~isfield(cfg_ref,'refchannel')      cfg_ref.refchannel     =0;end
    
    
    % compare cfg_filt with cfg_ref and put parameters from cfg_filt that
    % differ from cfg_ref into a string, to pass them as 'key-value' pair
    % to the function and make 'com' readable
    fieldnames_cfg_filt= fieldnames(cfg_filt);
    arg_string='';
    for i=1:numel(fieldnames_cfg_filt)
        %check if field is string or number
        if strcmp(class(cfg_filt.(fieldnames_cfg_filt{i})),'char')
            if ~strcmp(cfg_filt.(fieldnames_cfg_filt{i}),cfg_ref.(fieldnames_cfg_filt{i}))
                %['found difference in cfg_filt.' fieldnames_cfg_filt{i}]
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg_filt{i} ''', ''' cfg_filt.(fieldnames_cfg_filt{i}) '''']);
            end
            
        elseif numel(cfg_ref.(fieldnames_cfg_filt{i}))>1
            if numel(cfg_filt.(fieldnames_cfg_filt{i}))== numel(cfg_ref.(fieldnames_cfg_filt{i}))
                if cfg_filt.(fieldnames_cfg_filt{i})~=cfg_ref.(fieldnames_cfg_filt{i})
                    arg_string=strcat(arg_string,[', ''' fieldnames_cfg_filt{i} ''', [' num2str(cfg_filt.(fieldnames_cfg_filt{i})) ']' ]);
                end
            else
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg_filt{i} ''', [' num2str(cfg_filt.(fieldnames_cfg_filt{i})) ']' ]);
            end
        elseif cfg_filt.(fieldnames_cfg_filt{i})~=cfg_ref.(fieldnames_cfg_filt{i})
                %['found difference in cfg_filt.' fieldnames_cfg_filt{i}]
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg_filt{i} ''', ' num2str(cfg_filt.(fieldnames_cfg_filt{i})) ]);
            
      
            
        end
    end
    
    
    
    
    %plot_call=['cEEGrid_filterplot(EEG,[]' arg_string ')'];
    plot_call=[',[]' arg_string];
else
    if nargin == 2
        %plot_call=['cEEGrid_filterplot(EEG,' vararg2str(cfg_filt) ')'];
        if isempty(cfg_filt)
            plot_call=',[]';
        else
            plot_call=[',' vararg2str(cfg_filt)];
        end
    else
        %plot_call=['cEEGrid_filterplot(EEG,[],' vararg2str(varargin) ')'];
        if isempty(cfg_filt)
            plot_call=[',[],' vararg2str(varargin)];
        else
        plot_call=[',' vararg2str(cfg_filt) ',' vararg2str(varargin)];
        end
    end
end


%% call plot function
%EEG_filter_dat=eval(plot_call);
[EEG,EEG_filter_dat, ~]=eval(['cEEGrid_filterplot(EEG' plot_call ');']);

%% return the string command
% -------------------------
%com = sprintf('pop_cEEGrid_filterplot(%s, %s);', inputname(1),vararg2str(cfg_filt));
com = sprintf(['pop_cEEGrid_filterplot(' inputname(1) plot_call ');']);
