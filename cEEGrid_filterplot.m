%cEEGrid_filterplot(EEG,cfg_filt,varargin); - plots filtered data over time.

% Usage:
% >>[OUTEEG,EEG_filter_dat] = cEEGrid_filterplot(INEEG,cfg_filt,varargin)
%
% Inputs:
%   INEEG         - input EEG dataset

%   cfg_filt       - contains the parameters for the filter boundaries and
%                   smoothing
%   cfg_filt parameters:
% Parameter:         Var-Type   possible values                default values           description
% 'raw_lo'           'real'     [0 Inf]                        0.1
% 'raw_hi'           'real'     [0 Inf]                        45
% 'delta_lo'         'real'     [0 Inf]                        0.5
% 'delta_hi'         'real'     [0 Inf]                        3.5
% 'theta_lo'         'real'     [0 Inf]                        4
% 'theta_hi'         'real'     [0 Inf]                        7.5
% 'alpha_lo'         'real'     [0 Inf]                        8
% 'alpha_hi'         'real'     [0 Inf]                        12
% 'beta_lo'          'real'     [0 Inf]                        13
% 'beta_hi'          'real'     [0 Inf]                        35
% 'trials'           'integer'  e.g. [1 2 3 4 5 6]             vector with all trials
% 'channels'         'integer'  e.g. [1 2 3 4 5 6]             vector with all channels
% 'do_smooth_time'   'string'   {'yes' 'no'}                   'no'
% 'do_smooth_spat'   'string'   {'yes' 'no'}                   'no'
% 'smooth_time_val'  'real'     []                             50 ;
% 'smooth_spat_fact' 'real'     []                             1 ;
% 'side'             'string'   []                             'cEEGridLayout'
% 'layout'           'struct'   []                             layout struct that is selected with 'side'
% 'plot_filter'      'string'   {'yes' 'no'}                   'no'                     show plots from filters
% 'xlabel'           'string'   {'samples','time'}             'samples'
% 'use_raw'          'string'   {'yes' 'no'}                   'yes'                    use filters
% 'use_delta'        'string'   {'yes' 'no'}                   'yes'
% 'use_theta'        'string'   {'yes' 'no'}                   'yes'
% 'use_alpha'        'string'   {'yes' 'no'}                   'yes'
% 'use_beta'         'string'   {'yes' 'no'}                   'yes'
% 'conf_bound'       'real'     [0 Inf]                        3
% 'rem_outliers'     'integer'  [0 1]                          0
% 'time_range'       'real'     []                             [EEG.xmin*1000 EEG.xmax*1000]
% 'refchannel'       'integer'  []                             0
%
% Parameters can be passed to this function via key-value pair. E.g.:
% cEEGrid_filterplot(EEG,[], 'raw_lo', 0.2, 'trials', [1  2  3  5  6], 'xlabel', 'time', 'use_delta', 'no', 'rem_outliers', 1, 'conf_bound', 2.5)
%
% Parameters can also be passed via cfg_filt like:
% cfg_filt.raw_lo = 0.2;
% cfg_filt.trials=[1  2  3  5  6];
% cfg_filt.xlabel='time'
% cEEGrid_filterplot(EEG,cfg_filt)
% parameters passed via cfg_filt are overwritten by the key value pairs (if
% matching parameters are entered as key value pair.)
%
% Outputs:
%  OUTEEG           - output EEG set (identical to INEEG)
%  EEG_filter_dat   - output datasets for different filters
%                       for example:
%                       EEG data from 'raw' filter can be found under:
%                       EEG_filter_dat.raw
%
% See also:
%    EEGLAB
%
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG,EEG_filter_dat, com] = cEEGrid_filterplot(EEG,cfg_filt,varargin)
%com = ''; % this initialization is not needed since there is no GUI that can be cancelled
% string of cfg from call
if isempty(cfg_filt)
    call_cfg='[]';
else
    call_cfg=vararg2str(cfg_filt);
end
if nargin > 2
    call_cfg= strcat(call_cfg, [',' vararg2str(varargin)]);
end
%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false);
end
%%

%cfg_filt=[];
%cfg_filt.do_smooth_time='yes';
%cfg_filt.do_smooth_spat='yes';
%cfg_filt.trials=size(EEG.data,3)
%cfg_filt.plot_filter = 'no';
%% Default parameters
%get path for layouts
% load the correct layout file
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
%load([currentpath '\cEEGridLayout.mat']);

if exist([currentpath filesep 'Layouts' filesep 'cEEGridLayout.mat'],'file')==0
    msg ='The cEEGrid LayoutFile cEEGridStandardLayout.mat needs to be in the script directory';
    error(msg);
end


%% default values (they need to be defined before inputcheck, bc otherwise cfg cant be passed to this function w/o beeing overwritten)
if ~isfield(cfg_filt,'raw_lo')          cfg_filt.raw_lo     =0.1; end
if ~isfield(cfg_filt,'raw_hi')          cfg_filt.raw_hi     =45; end
if ~isfield(cfg_filt,'delta_lo')        cfg_filt.delta_lo   =0.5; end
if ~isfield(cfg_filt,'delta_hi')        cfg_filt.delta_hi   =3.5; end
if ~isfield(cfg_filt,'theta_lo')        cfg_filt.theta_lo   =4; end
if ~isfield(cfg_filt,'theta_hi')        cfg_filt.theta_hi   =7.5; end
if ~isfield(cfg_filt,'alpha_lo')        cfg_filt.alpha_lo   =8; end
if ~isfield(cfg_filt,'alpha_hi')        cfg_filt.alpha_hi   =12; end
if ~isfield(cfg_filt,'beta_lo')         cfg_filt.beta_lo    =13; end
if ~isfield(cfg_filt,'beta_hi')         cfg_filt.beta_hi    =35; end
if ~isfield(cfg_filt,'trials')          cfg_filt.trials     =[1:size(EEG.data,3)]; end
if ~isfield(cfg_filt,'channels')        cfg_filt.channels   =[1:size(EEG.data,1)]; end
if ~isfield(cfg_filt,'do_smooth_time')  cfg_filt.do_smooth_time='no'; end
if ~isfield(cfg_filt,'do_smooth_spat')  cfg_filt.do_smooth_spat='no'; end
if ~isfield(cfg_filt,'smooth_time_val') cfg_filt.smooth_time_val=50; end
if ~isfield(cfg_filt,'smooth_spat_fact') cfg_filt.smooth_spat_fact = 200;end
if ~isfield(cfg_filt,'side')            cfg_filt.side = 'cEEGridLayout';end
if ~isfield(cfg_filt,'layout')
    layoutFile=load([currentpath filesep 'Layouts' filesep cfg_filt.side]);
    fields=fieldnames(layoutFile);
    cfg_filt.layout= layoutFile.(fields{1});
end
if ~isfield(cfg_filt,'plot_filter')     cfg_filt.plot_filter    = 'no';end
if ~isfield(cfg_filt,'xlabel')          cfg_filt.xlabel         = 'samples';end

if ~isfield(cfg_filt,'use_raw')         cfg_filt.use_raw        = 'yes';end
if ~isfield(cfg_filt,'use_delta')       cfg_filt.use_delta      = 'yes';end
if ~isfield(cfg_filt,'use_theta')       cfg_filt.use_theta      = 'yes';end
if ~isfield(cfg_filt,'use_alpha')       cfg_filt.use_alpha      = 'yes';end
if ~isfield(cfg_filt,'use_beta')        cfg_filt.use_beta       = 'yes';end
if ~isfield(cfg_filt,'conf_bound')      cfg_filt.conf_bound     =3;end
if ~isfield(cfg_filt,'rem_outliers')    cfg_filt.rem_outliers   =0;end
if ~isfield(cfg_filt,'time_range')      cfg_filt.time_range     =[EEG.xmin*1000 EEG.xmax*1000];end
if ~isfield(cfg_filt,'refchannel')      cfg_filt.refchannel     =0;end
%% check inputs

fieldlist = {   'raw_lo'           'real'     [0 Inf]                        cfg_filt.raw_lo  ;
    'raw_hi'           'real'     [0 Inf]                        cfg_filt.raw_hi  ;
    'delta_lo'         'real'     [0 Inf]                        cfg_filt.delta_lo ;
    'delta_hi'         'real'     [0 Inf]                        cfg_filt.delta_hi ;
    'theta_lo'         'real'     [0 Inf]                        cfg_filt.theta_lo ;
    'theta_hi'         'real'     [0 Inf]                        cfg_filt.theta_hi ;
    'alpha_lo'         'real'     [0 Inf]                        cfg_filt.alpha_lo ;
    'alpha_hi'         'real'     [0 Inf]                        cfg_filt.alpha_hi ;
    'beta_lo'          'real'     [0 Inf]                        cfg_filt.beta_lo ;
    'beta_hi'          'real'     [0 Inf]                        cfg_filt.beta_hi ;
    'trials'           'integer'  []                             cfg_filt.trials ;
    'channels'         'integer'  []                             cfg_filt.channels ;
    'do_smooth_time'   'string'   {'yes' 'no'}                   cfg_filt.do_smooth_time ;
    'do_smooth_spat'   'string'   {'yes' 'no'}                   cfg_filt.do_smooth_spat ;
    'smooth_time_val'  'real'     []                             cfg_filt.smooth_time_val ;
    'smooth_spat_fact' 'real'     []                             cfg_filt.smooth_spat_fact ;
    'side'             'string'   []                             cfg_filt.side ;
    'layout'           'struct'   []                             cfg_filt.layout ;
    'plot_filter'      'string'   []                             cfg_filt.plot_filter ;
    'xlabel'           'string'   []                             cfg_filt.xlabel ;
    'use_raw'          'string'   []                             cfg_filt.use_raw ;
    'use_delta'        'string'   []                             cfg_filt.use_delta ;
    'use_theta'        'string'   []                             cfg_filt.use_theta ;
    'use_alpha'        'string'   []                             cfg_filt.use_alpha ;
    'use_beta'         'string'   []                             cfg_filt.use_beta ;
    'conf_bound'       'real'     [0 Inf]                        cfg_filt.conf_bound ;
    'rem_outliers'     'integer'  [0 1]                          cfg_filt.rem_outliers ;
    'time_range'       'real'     []                             cfg_filt.time_range ;
    'refchannel'       'integer'  []                             cfg_filt.refchannel ;
    };
[cfg_filt varargin] = finputcheck( varargin, fieldlist, 'cEEGrid_filterplot', 'ignore');


INEEG=EEG;
%% smoothing data
%smooth data over time

if strcmp(cfg_filt.do_smooth_time,'yes')
    % create gauss window
    sizeX=cfg_filt.smooth_time_val;
    meanX = (sizeX+1)/2;
    sigmaX = sizeX/5;
    x_vec= linspace(1, sizeX, sizeX);
    gauss_vec=exp(-0.5*(  ((x_vec-meanX)/sigmaX).*((x_vec-meanX)/sigmaX)))...
        /((sigmaX)*pi);
    % convolute time signals with gauss window
    for ch_nr=1:size(INEEG.data,1)
        for tr_nr=1:size(INEEG.data,3)
            INEEG.data(ch_nr,:,tr_nr)=conv(INEEG.data(ch_nr,:,tr_nr),gauss_vec,'same');
        end
    end
    % done
end
% if strcmp(cfg_filt.do_smooth_time,'yes')
%     for count_channels= 1:size(INEEG.data,1)
%         for count_trials= 1:size(INEEG.data,3)
%             INEEG.data(count_channels,:,count_trials)=smooth(INEEG.data(count_channels,:,count_trials),cfg_filt.smooth_time_val)';
%         end
%     end
% end

%% smooth data spatially

if strcmp(cfg_filt.do_smooth_spat,'yes')
    
    % getting positions of channels
    [channel_num, channel_ind] = match_str({INEEG.chanlocs.labels}, cfg_filt.layout.label);
    if isempty(channel_ind)
        ft_error('labels in data and labels in layout do not match');
    end
    
    % Select x and y coordinates and labels of the channels in the data
    channel_x = cfg_filt.layout.pos(channel_ind,1);
    channel_y = cfg_filt.layout.pos(channel_ind,2);
    %     channel_x=channel_x-min(channel_x)+1;
    %     channel_y=channel_y-min(channel_y)+1;
    
    % Calculate distances between channel positions
    dist_list = pdist([channel_x,channel_y]);
    dist_mat=squareform(dist_list);
    sizeX=cfg_filt.smooth_spat_fact;
    meanX = 0;%(sizeX+1)/2;
    sigmaX = sizeX/5;
    %      use_point_mat=dist_mat;
    %      use_point_mat(dist_mat<=sizeX/2)=1;
    %      use_point_mat(dist_mat>sizeX/2)=0;
    
    % Calculate gauss 'window'
    gauss_mat=exp(-0.5*(  ((dist_mat-meanX)/sigmaX).*((dist_mat-meanX)/sigmaX)))...
        /((sigmaX)*pi);%.*use_point_mat;
    
    for ch_nr=1:size(INEEG.data,1)
        
        for tr_nr=1:size(INEEG.data,3)
            smoothedChannelData(ch_nr,:,tr_nr)=gauss_mat(ch_nr,1)*INEEG.data(1,:,tr_nr);
            for ch_nr_intern=2:size(INEEG.data,1)
                
                smoothedChannelData(ch_nr,:,tr_nr)=smoothedChannelData(ch_nr,:,tr_nr)+ gauss_mat(ch_nr,ch_nr_intern)*INEEG.data(ch_nr_intern,:,tr_nr);
            end
        end
        
    end
    INEEG.data=smoothedChannelData;
    
end


%% split signal up into different powerbands (provide default, allow to hand
%in the band cutoffs as parameter
subfig_count=0;
EEG_filter_dat=[];
figure
if strcmp(cfg_filt.use_raw, 'yes')
    EEGraw = pop_eegfiltnew(INEEG, 'locutoff',cfg_filt.raw_lo,'hicutoff',cfg_filt.raw_hi,'plotfreqz',1);
    if strcmp(cfg_filt.plot_filter, 'yes') figure; end
    subfig_count=subfig_count+1;
    raw_pos=subfig_count;
    EEG_filter_dat.raw=EEGraw;
end
if strcmp(cfg_filt.use_delta, 'yes')
    EEGdel = pop_eegfiltnew(INEEG, 'locutoff',cfg_filt.delta_lo,'hicutoff',cfg_filt.delta_hi,'plotfreqz',1);
    if strcmp(cfg_filt.plot_filter, 'yes') figure; end
    subfig_count=subfig_count+1;
    delta_pos=subfig_count;
    EEG_filter_dat.delta=EEGdel;
end
if strcmp(cfg_filt.use_theta, 'yes')
    EEGthe = pop_eegfiltnew(INEEG, 'locutoff',cfg_filt.theta_lo,'hicutoff',cfg_filt.theta_hi,'plotfreqz',1);
    if strcmp(cfg_filt.plot_filter, 'yes') figure; end
    subfig_count=subfig_count+1;
    theta_pos=subfig_count;
    EEG_filter_dat.theta=EEGthe;
end
if strcmp(cfg_filt.use_alpha, 'yes')
    EEGalp = pop_eegfiltnew(INEEG, 'locutoff',cfg_filt.alpha_lo,'hicutoff',cfg_filt.alpha_hi,'plotfreqz',1);
    if strcmp(cfg_filt.plot_filter, 'yes') figure; end
    subfig_count=subfig_count+1;
    alpha_pos=subfig_count;
    EEG_filter_dat.alpha=EEGalp;
end
if strcmp(cfg_filt.use_beta, 'yes')
    EEGbet = pop_eegfiltnew(INEEG, 'locutoff',cfg_filt.beta_lo,'hicutoff',cfg_filt.beta_hi,'plotfreqz',1);
    subfig_count=subfig_count+1;
    beta_pos=subfig_count;
    EEG_filter_dat.beta=EEGbet;
end





%% Plots

%time vectors:
times_ind=find(INEEG.times<=cfg_filt.time_range(2));
times_ind=find(INEEG.times(times_ind)>=cfg_filt.time_range(1));
times=INEEG.times(times_ind);


if cfg_filt.refchannel>0
    subfig_count=subfig_count+1;
end
if subfig_count>0
    if strcmp(cfg_filt.xlabel,'time')
        x_val=times;
        x_label='Time [ms]';
        
    else
        x_val=times_ind;
        x_label='Samples';
    end
    y_val=cfg_filt.channels;
    
    if strcmp(cfg_filt.plot_filter, 'yes') figure; end
    if strcmp(cfg_filt.use_raw, 'yes')
        subplot(subfig_count*100+10+raw_pos)
        if cfg_filt.rem_outliers==1
            data=mean(EEGraw.data(cfg_filt.channels,times_ind,cfg_filt.trials),3);
            ndev = cfg_filt.conf_bound;
            data_mean = mean(data(:));
            data_std = std(data(:));
            data_min = min(data(:));
            data_max = max(data(:));
            cmin = max( data_min, data_mean - ndev * data_std );
            cmax = min( data_max, data_mean + ndev * data_std );
            
            % plot either continous signal 2D EEG.data, or epoched signal 3D EEG.data (either averaged (allow to specific which trials should be average) or single trial)
            imagesc(x_val,y_val,mean(EEGraw.data(cfg_filt.channels,times_ind,cfg_filt.trials),3),[cmin, cmax])
        else
            imagesc(x_val,y_val,mean(EEGraw.data(cfg_filt.channels,times_ind,cfg_filt.trials),3))
        end
        title(['Raw signal ',num2str(cfg_filt.raw_lo),'-',num2str(cfg_filt.raw_hi),' Hz'])
        ylabel('Channels')
        xlabel(x_label)
        set(gca,'YTickLabel',[]);
        
    end
    if strcmp(cfg_filt.use_delta, 'yes')
        subplot(subfig_count*100+10+delta_pos)
        if cfg_filt.rem_outliers==1
            data=mean(EEGdel.data(cfg_filt.channels,times_ind,cfg_filt.trials),3);
            ndev = cfg_filt.conf_bound;
            data_mean = mean(data(:));
            data_std = std(data(:));
            data_min = min(data(:));
            data_max = max(data(:));
            cmin = max( data_min, data_mean - ndev * data_std );
            cmax = min( data_max, data_mean + ndev * data_std );
            
            % plot either continous signal 2D EEG.data, or epoched signal 3D EEG.data (either averaged (allow to specific which trials should be average) or single trial)
            imagesc(x_val,y_val,mean(EEGdel.data(cfg_filt.channels,times_ind,cfg_filt.trials),3),[cmin, cmax])
        else
            imagesc(x_val,y_val,mean(EEGdel.data(cfg_filt.channels,times_ind,cfg_filt.trials),3))
        end
        title(['Delta ',num2str(cfg_filt.delta_lo),'-',num2str(cfg_filt.delta_hi),' Hz'])
        ylabel('Channels')
        xlabel(x_label)
        set(gca,'YTickLabel',[]);
    end
    if strcmp(cfg_filt.use_theta, 'yes')
        subplot(subfig_count*100+10+theta_pos)
        if cfg_filt.rem_outliers==1
            data=mean(EEGthe.data(cfg_filt.channels,times_ind,cfg_filt.trials),3);
            ndev = cfg_filt.conf_bound;
            data_mean = mean(data(:));
            data_std = std(data(:));
            data_min = min(data(:));
            data_max = max(data(:));
            cmin = max( data_min, data_mean - ndev * data_std );
            cmax = min( data_max, data_mean + ndev * data_std );
            
            % plot either continous signal 2D EEG.data, or epoched signal 3D EEG.data (either averaged (allow to specific which trials should be average) or single trial)
            imagesc(x_val,y_val,mean(EEGthe.data(cfg_filt.channels,times_ind,cfg_filt.trials),3),[cmin, cmax])
        else
            imagesc(x_val,y_val,mean(EEGthe.data(cfg_filt.channels,times_ind,cfg_filt.trials),3))
        end
        title(['Theta ',num2str(cfg_filt.theta_lo),'-',num2str(cfg_filt.theta_hi),' Hz'])
        ylabel('Channels')
        xlabel(x_label)
        set(gca,'YTickLabel',[]);
    end
    if strcmp(cfg_filt.use_alpha, 'yes')
        subplot(subfig_count*100+10+alpha_pos)
        if cfg_filt.rem_outliers==1
            data=mean(EEGalp.data(cfg_filt.channels,times_ind,cfg_filt.trials),3);
            ndev = cfg_filt.conf_bound;
            data_mean = mean(data(:));
            data_std = std(data(:));
            data_min = min(data(:));
            data_max = max(data(:));
            cmin = max( data_min, data_mean - ndev * data_std );
            cmax = min( data_max, data_mean + ndev * data_std );
            
            % plot either continous signal 2D EEG.data, or epoched signal 3D EEG.data (either averaged (allow to specific which trials should be average) or single trial)
            imagesc(x_val,y_val,mean(EEGalp.data(cfg_filt.channels,times_ind,cfg_filt.trials),3),[cmin, cmax])
        else
            imagesc(x_val,y_val,mean(EEGalp.data(cfg_filt.channels,times_ind,cfg_filt.trials),3))
        end
        title(['Alpha ',num2str(cfg_filt.alpha_lo),'-',num2str(cfg_filt.alpha_hi),' Hz'])
        ylabel('Channels')
        xlabel(x_label)
        set(gca,'YTickLabel',[]);
    end
    if strcmp(cfg_filt.use_beta, 'yes')
        subplot(subfig_count*100+10+beta_pos)
        if cfg_filt.rem_outliers==1
            data=mean(EEGbet.data(cfg_filt.channels,times_ind,cfg_filt.trials),3);
            ndev = cfg_filt.conf_bound;
            data_mean = mean(data(:));
            data_std = std(data(:));
            data_min = min(data(:));
            data_max = max(data(:));
            cmin = max( data_min, data_mean - ndev * data_std );
            cmax = min( data_max, data_mean + ndev * data_std );
            
            % plot either continous signal 2D EEG.data, or epoched signal 3D EEG.data (either averaged (allow to specific which trials should be average) or single trial)
            imagesc(x_val,y_val,mean(EEGbet.data(cfg_filt.channels,times_ind,cfg_filt.trials),3),[cmin, cmax])
        else
            imagesc(x_val,y_val,mean(EEGbet.data(cfg_filt.channels,times_ind,cfg_filt.trials),3))
        end
        title(['Beta ',num2str(cfg_filt.beta_lo),'-',num2str(cfg_filt.beta_hi),' Hz'])
        ylabel('Channels')
        xlabel(x_label)
        set(gca,'YTickLabel',[]);
    end
    if cfg_filt.refchannel>0
        subplot(subfig_count*100+10+subfig_count)
        plot(mean(EEG.data(cfg_filt.refchannel,times_ind,cfg_filt.trials),3))
        title(['REF channel: ' EEG.chanlocs(cfg_filt.refchannel).labels])
        %ylabel('Channels')
        xlabel(x_label)
    end
    
end
% return the string command
% -------------------------
com = sprintf('cEEGrid_filterplot(%s,%s);', inputname(1),call_cfg);

