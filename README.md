# EEGLAB plugin to visualize cEEGrid data using fieldtrip functions
The plugin facilitates analyzing and plotting  ear-EEG data (cEEGrid). 

### What are cEEGrids? 
The cEEGrid is an electrode array for measuring EEG scalp potentials around the ear. It was developed in the Neuropsychology Lab in the Psychology Department of the Carl von Ossietzky University Oldenburg, Germany.

More details on how to use it can be found [here](https://uol.de/psychologie/abteilungen/ceegrid).

A video tutorial on how to set up and record with cEEGrids can be found [here](https://uol.de/psychologie/neurophysiologie-des-alltags-emmy-noether-gruppe/ressourcen/ceegrid-video-tutorial).


### What does this plugin do? 
The plugin uses [fieldtrip](http://www.fieldtriptoolbox.org/) functions to plot cEEGrid data in the correct 
channel arrangement or as topoplots in [EEGlab](https://sccn.ucsd.edu/eeglab/index.php). 
Furthermore, it extends the corrmap plugin (http://www.debener.de/corrmap/corrmapplugin1.html) to work with cEEGrid data. 

### Requirements
In addition to the requirements of EEGLAB, cEEGrid plug-in needs some fieldtrip functions to work. Using the EEGLAB extension manager you can add Fieldtrip-lite to have all necessary functions in your path.  
For plotting the ICA topographies and components it is necessary that you have run ICA. 

### Download and Installation
1) Download the compressed cEEGrid plug-in file (*.zip) into your 'plugins' directory of your  EEGLAB distribution.

2) Uncompress the downloaded file using utility software. Inside your 'plugins' directory, you should now have a directory called 'cEEGrid_EEGLAB_plugin' 
containing all the necessary m-files . Starting EEGLAB should now automatically recognise and add the plug-in. 
You should see the following line appear in your matlab environment window:

EEGLAB: adding "cEEGridplugin" v0.9 (see >> help eegplugin_ceegrid)

That's it.

### Channel Labeling Convention
The plugin assumes the following channel labeling convention. 
R4a and R4b are generally used as ground and reference electrode, respectively. This will depend on your amplifier and connector. 

<img src="/images/cEEGridLabel.png" width="80%">

# Plotting

## Time domain (cEEGrid Channel ERP)
The time domain signal for each channel can be plotted with "cEEGrid channel ERP".

In the pop up you can specify the channels to plot using the channel indices. Otherwise all channels will be shown. 
Also Grid and additional plot options can be selected.
![cEEGrid Layout](/images/multiplotER.png)

The resulting figure could look as follows.

![cEEGrid Layout](/images/multiplotER_Result.PNG)

The channels that are not in your .set file (here R04a and R04b) won't show in your plot. 

As we are using fieldtrip functions for plotting we have additional functionality that is normaly not part of EEGLAB

For example, you can select a couple of electrodes with the mouse, and left click on the selection. 

![channel Selection](/images/selectChannels.PNG)

A new figure will open up, showing you the mean signal of all selected channels.

![mean Channel](/images/meanChannel.png)

In the resulting figure you can select a specific time window with the mouse. 

![mean Channel selection](/images/meanChannelSelection.png)

A new figure will open up, showing the topography for the selected time window. 

![selected Topography](/images/selectedTopography.PNG)

Note that the topography looks slightly different from the topography that results from 'cEEGrid topography' due to differences in the default settings. 


## Topoplots (cEEGrid Topography)
Topographies for specific time point (if one value is given) or time window (if two values are given). 

In the pop up you can specify the channels to plot using the channel indices. Otherwise all channels will be shown. 
You have to indicate the time point or time window for which the topoplot should be made. 

You can choose to either show both grids, or only the left or the right grid. 

![cEEGrid Layout](/images/topoplotER.png)

The resulting figure.

![cEEGrid Layout](/images/topoplotER_Result.png)

## ICA topographies (cEEGrid ICA plot)

After you have computed ICA components you can plot them using "cEEGrid ICA plot".

In the pop up you can specify the components to plot using the component indices. Otherwise all components will be shown. 
Also Grid and additional plot options can be selected.
You can specify, if you want to plot the time courses additional to the topographies of the components.
It is also possible to select, if the plots should be shown in one figure or in seperated figures containing a specified number of plots and how the plots should be arranged inside the figures.
The 'Automatic row/columns' option -if selected- arranges the plots as close as possible to a squared grid. Otherwise you can deselect the option and set a fixed number of plots per row.

![cEEGrid Layout](/images/ICAplot.png)

The resulting figures could look as follows.

Topographies with time courses, seperated plots and 2 plots per figure.

![cEEGrid Layout](/images/ICAplot_components_timecourses.png)

All topographies without time courses in 1 figure.

![cEEGrid Layout](/images/ICAplot_components.png)


## Plot spectra and topographies

Spectra and topographies for different frequencies can be plotted with "cEEGrid spectopo".

The pop up of the function allows to adjust the time range that is to be analyzed, the percent of trials that should be used and the frequenzies for which the topographies should be plotted.
Also the frequency range and grid can be adjusted.

![cEEGrid spectra and topographies](/images/spectopo.png)

The resulting figures could look as follows:

![cEEGrid spectra and topographies Results](/images/spectopo_result.png)

## Filter EEG data and plot results

Using the plotting function "cEEGrid plot filtered data" the EEG data can be filtered into different filterbands and plotted.
The pop up menu allows to adjust the frequencies and if a band should be plotted or not. Also channels and trials can be specified.
It is also possible to smooth the data in time and spatial dimensions with a gaussian window. It is also possible to ignore outliers in the resulting plot to enhance the visibility.
Also time range and X-axis labels can be adjusted.
It is also possible to choose one of the EEG channels as reference channel, which will be shown as 2D plot below the other plots.
The pop up function looks as follows:
![cEEGrid plot filtered data](/images/filterplot.png)

The resulting figure could look as follows:
![cEEGrid plot filtered data Results](/images/filterplot_result.png)

## Different Electrode layouts (fEEGrid) 
Currently, you can choose (for 'cEEGrid channel' ERP and 'cEEGrid topography') between cEEGrids and fEEGrids.

![fEEGrid Result](/images/topoplotER_Result_fEEGrid.png)

# Tools

## Rename channels
After loading a dataset, it is possible to rename the channels using the tool 'cEEGrid rename'. First the function will ask to select a Layout:
![cEEGrid rename](/images/rename1.png)

After selecting the layout, the layout and a form to change the channel labels will show up:
![cEEGrid rename](/images/rename3.png)
![cEEGrid rename](/images/rename2.png)

In this form, you can select an existing setup with channel names, or create one yourself and save it. Afterwards the channelnames will be renamed.

## Difference channels
Using the tool 'cEEGrid diff. channels' you can compute the difference channels:
![cEEGrid diff. channels](/images/diffchan.png)
minuends and subtrahends can be entered like:
    Indicate minuend:    R02 [R01 R02]
    Indicate subtrahend: [R06 R07][R07 R08]
This would calculate R02 - (R06+R07)/2 and (R01+R02)/2 - (R07+R08)/2

# Study displays
This plugin allows to display your data within the cEEGrid and fEEGrid layouts at the study level (with different groups and conditions to compare). 

The first step that you have to do is to create a usual EEGLAB STUDY with your datasets, including a usual study design with the different conditions labeled. 
Then you should precompute the ERP and spectral activities to be plotted. After that, you can click wihin the STUDY tool EEGLAB menu, and you can select the "Plotting cEEGrid" button.

A pop-up window appears when you can select to plot either time-domain data ("ERP") or frequency-domain data ("Spec"). Likewise, you can either plot the time-course/frequency variations ("Plot channels"), or the topographies ("Plot topo").

The ERP and spectral data are computed the same way as previously explained. For channel plots all the conditions are displayed on single plots. For topographies, there is one pop-up window  per condition (with the name of the condition displayed on the figure header).

You can plot channels and topographies consecultively, alike you can plot time and frequency-domain data consecutively.

The resulting figure.

![Study plotting](/images/Study_timecourse.PNG)

## Corrmap

Using the study function 'Cluster components by correlation (cEEGrid-CORRMAP)' you can compute correlations between IC template from a single dataset and all ICs from current STUDY, cluster ICs above correlation threshold, and display summary plot containing topographical maps for clustered ICs, average map, correlation plot and similarity index plot.

GUI and resulting figure:

![cEEGrid rename](/images/corrmap.png)

## How to cite this work
If you want to refer to this work: 10.5281/zenodo.5946875. 

## Publications 
Please refer to the following publications for more information on cEEGrids:

1. Debener, S., Emkes, R., De Vos, M. & Bleichner, M. (2015). Unobtrusive ambulatory EEG using a smartphone and flexible printed electrodes around the ear. Sci. Rep. 5, 16743. doi: [10.1038/srep16743](https://doi.org/10.1038/srep16743)
2. Mirkovic, B., Bleichner, M. G., De Vos, M. & Debener, S. (2016). Target speaker detection with concealed EEG around the ear. Front. Neurosci. 10. doi: [10.3389/fnins.2016.00349](https://doi.org/10.3389/fnins.2016.00349)
3. Bleichner, M. G. et al. (2015). Exploring miniaturized EEG electrodes for brain-computer interfaces. Physiol. Rep. 3, e12362. doi: [10.14814/phy2.12362](https://doi.org/10.14814/phy2.12362)
4. Bleichner, M.G., Mirkovic, B. & Debener, S. (2016). Identifying auditory attention with ear-EEG: cEEGrid versus high-density cap-EEG comparison. J. Neural Eng. 13.
5. Bleichner, M.G. & Debener, S. (2017). Concealed, unobtrusive ear-centered EEG acquisition: cEEGrids for transparent EEG. Front Hum Neurosci. 11.
6. Pacharra, N., Debener, S. & Wascher, E. (2017). Concealed Around-the-Ear EEG Captures Cognitive Processing in a Visual Simon Task. Front. Hum. Neurosci. 11.
7. Blum, S., Debener, S., Emkes, R., Volkening, N., Fudickar, S., & Bleichner, M. G. (2017). EEG Recording and Online Signal Processing on Android: A Multiapp Framework for Brain-Computer Interfaces on Smartphone. BioMed Research International. 2017. 1-22. doi: [10.1155/2017/3072870](https://doi.org/10.1155/2017/3072870)
8. Denk, F., Grzybowski, M., Ernst, S. M. A., Kollmeier B., Debener, S., Bleichner, M. G. (2018). Event-related potentials measured from in and around the ear electrodes integrated in a live hearing device for monitoring sound perception, Trends in Hearing 22.
9. Hölle, D., Meekes, J. & Bleichner, M.G. (2021). Mobile ear-EEG to study auditory attention in everyday life. Behav Res 53, 2025–2036. doi: [10.3758/s13428-021-01538-0](https://doi.org/10.3758/s13428-021-01538-0)
10. Holtze, B., Rosenkranz, M., Jaeger, M., Debener, S., & Mirkovic, B. (2022). Ear-EEG Measures of Auditory Attention to Continuous Speech. Front. Neurosci. 16:869426. doi: [10.3389/fnins.2022.869426](https://doi.org/10.3389/fnins.2022.869426)
11. Hölle, D., & Bleichner, M.G. (2023). Smartphone-based ear-EEG to study sound processing in everyday life. Eur. J. Neurosci. 58(7), 3671-3685. doi: [10.1111/ejn.16124](https://doi.org/10.1111/ejn.16124)
12. Hölle, D., & Bleichner, M.G. (2023). Recording brain activity with ear-electroencephalography. JoVE (Journal of Visualized Experiments), e64897. doi: [10.3791/64897](https://doi.org/10.3791/64897)
