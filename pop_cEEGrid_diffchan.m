% pop_cEEGrid_diffchan(EEG,ch_minuend, ch_subtrahend); -  Computes difference channels.

%Usage:
% >> [OUTEEG, diff_chan] =pop_cEEGrid_diffchan(INEEG, ch_minuend, ch_subtrahend);
%
% Inputs:
%   INEEG           - input EEG dataset
%   ch_minuend      - minuends (have to be given as cell array, e.g.:
%                      {{'R02'} {'R01' 'R02'}})
%   ch_subtrahend   - subtrahends (have to be given as cell array, e.g.:
%                      {{'R06' 'R07'} {'R07' 'R08'}})
%
%             -Minuends and subtrahends can only contain channelnames, that
%             exist in the given EEG data set. 
%             -The number of minuends and subtrahends must match.
%             -If only INEEG is given, a GUI will show up, where minuends
%             and subtrahends can be entered like:
%               Indicate minuend:    R02 [R01 R02]
%               Indicate subtrahend: [R06 R07][R07 R08]
%
% Outputs:
%   OUTEEG          - output EEG set (identical to INEEG)
%   diff_chan       - difference channels
%
% Example:
% pop_cEEGrid_diffchan( EEG,{{'R02'},{'R01','R02'}}, {{'R06','R07'},{'R07','R08'}});
% will calculate R02 - (R06+R07)/2 AND (R01+R02)/2 - (R07+R08)/2 and return
% them in diff_chan
%
% See also:
%    EEGLAB


% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, diff_chan, com] = pop_cEEGrid_diffchan(EEG,ch_minuend, ch_subtrahend)

com='';
if nargin < 1
    help pop_cEEGrid_diffchan;
    return;
end

if nargin < 2
    %% GUI
    
    guititle='cEEGrid_diffchan -- pop_cEEGrid_diffchan';
    
    geom={[1 2] [1 2] [1] [1 2] [1 2]};
    
    minuend='';
    subtrahend='';
    correct_inputs=false;
    while correct_inputs==false % recreate GUI, until all GUI inputs are correct, or function is returned
        % Indicate minuend: [R02 R03] [R01 R02]
        % Indicate subtrahend: [R06 R07] [R07 R08]
        correct_inputs=true;
        uilist={
            { 'style', 'text', 'string', 'Indicate minuend:', 'fontweight', 'bold' },...
            { 'style', 'edit', 'string', minuend 'tag' 'minuend'}...
            { 'style', 'text', 'string', 'e.g.:[R02 R03] [R01 R02]' },...
            {}...
            {}...
            { 'style', 'text', 'string', 'Indicate subtrahend:', 'fontweight', 'bold' },...
            { 'style', 'edit', 'string', subtrahend 'tag' 'subtrahend'}...
            { 'style', 'text', 'string', 'e.g.:[R06 R07] [R07 R08]' },...
            {}...
            };
        
        
        [result userdat strhalt outstruct]=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_diffchan'')', guititle, [], 'normal');
        
        % catch cancelling-event
        if isempty(result), return; end
        
        %% process inputs to get minuend and subtrahend as cell arrays
        input1=outstruct.minuend;
        input1=strrep(input1,',',' ');
        input1=strrep(input1,';',' ');
        input1=strrep(input1,'{','[');
        input1=strrep(input1,'}',']');
        input1=strrep(input1,'(','[');
        input1=strrep(input1,')',']');
        input2=outstruct.subtrahend;
        input2=strrep(input2,',',' ');
        input2=strrep(input2,';',' ');
        input2=strrep(input2,'{','[');
        input2=strrep(input2,'}',']');
        input2=strrep(input2,'(','[');
        input2=strrep(input2,')',']');
        
        %% create cell array for minuend
        a=strfind(input1,'['); % indices of '[' inside inputstring
        b=strfind(input1,']'); % indices of ']' inside inputstring
        if numel(a)~=numel(b)
            fprintf(['Wrong input for minuend! Number of ''['' and '']'' does not match!\n']);
            correct_inputs=false;
        end
        
        
        
        minu=[]; %cell array containing different minuends e.g.: R01[R02 R03] -> minu={{'R01'}{'R02 R03'}}
        if numel(a)~=0 % if brackets are found, use them as delimiter and find everything inside and outside of brackets
            k=1; % counter for different minuends
            
            %get everything befor first bracket and store it seperated by ' ' to minu
            in1=split(input1(1:a(1)-1));
            for h=k:k+numel(split(input1(1:a(1)-1)))-1
                minu{h}=in1{h};
            end
            k=k+numel(split(input1(1:a(1)-1)));
            
            %now do the same for everything inside brackets and
            %between/behind them
            for i=1:numel(a)
                
                minu{k}=input1(a(i)+1:b(i)-1);
                k=k+1;
                if i<numel(a)
                    in1=split(input1(b(i)+1:a(i+1)-1));
                    for h=k:k+numel(split(input1(b(i)+1:a(i+1)-1)))-1
                        minu{h}=in1{h-k+1};
                    end
                    k=k+numel(split(input1(b(i)+1:a(i+1)-1)));
                elseif b(i)+1<numel(input1)
                    minu{k}=input1(b(i)+1:end);
                    in1=split(input1(b(i)+1:end));
                    for h=k:k+numel(split(input1(b(i)+1:end)))-1
                        minu{h}=in1{h-k+1};
                    end
                    k=k+numel(split(input1(b(i)+1:end)));
                end
                
            end
        else
            minu{1}=input1;
        end
        
        % create final cell array with minuends e.g.:
        % minu={{'R01'},{'R02 R03'}} -> minu_final={{'R01'},{{'R02'}, {'R03'}}}
        minu_final=[];
        k=1;
        for i=1:numel(minu)
            if ~isempty(erase(minu{i}," "))
                minu_final{k}=split(minu{i});
                for j=1:numel(minu_final{k})
                    minu_final{k}{j}=erase(minu_final{k}{j}," ");
                    
                end
                minu_final{k}(cellfun('isempty',minu_final{k}))=[];
                k=k+1;
            end
        end
        %% create cell array for subtrahend 
        %(for comments see analogical process for minuends above)
        a=strfind(input2,'[');
        b=strfind(input2,']');
        
        if numel(a)~=numel(b)
            fprintf(['Wrong input for subtrahend! Number of ''['' and '']'' does not match!\n']);
            correct_inputs=false;
        end
        
        
        
        subt=[];
        if numel(a)~=0
            k=1;
            in1=split(input2(1:a(1)-1));
            for h=k:k+numel(split(input2(1:a(1)-1)))-1
                subt{h}=in1{h};
            end
            k=k+numel(split(input2(1:a(1)-1)));
            for i=1:numel(a)
                
                subt{k}=input2(a(i)+1:b(i)-1);
                k=k+1;
                if i<numel(a)
                    in1=split(input2(b(i)+1:a(i+1)-1));
                    for h=k:k+numel(split(input2(b(i)+1:a(i+1)-1)))-1
                        subt{h}=in1{h-k+1};
                    end
                    k=k+numel(split(input2(b(i)+1:a(i+1)-1)));
                elseif b(i)+1<numel(input2)
                    subt{k}=input2(b(i)+1:end);
                    in1=split(input2(b(i)+1:end));
                    for h=k:k+numel(split(input2(b(i)+1:end)))-1
                        subt{h}=in1{h-k+1};
                    end
                    k=k+numel(split(input2(b(i)+1:end)));
                end
                
            end
        else
            subt{1}=input2;
        end
        subt_final=[];
        k=1;
        for i=1:numel(subt)
            if ~isempty(erase(subt{i}," "))
                subt_final{k}=split(subt{i});
                for j=1:numel(subt_final{k})
                    subt_final{k}{j}=erase(subt_final{k}{j}," ");
                    
                end
                subt_final{k}(cellfun('isempty',subt_final{k}))=[];
                k=k+1;
            end
        end
        ch_minuend=minu_final;
        ch_subtrahend=subt_final;
        
        if numel(ch_minuend)~=numel(ch_subtrahend)
            fprintf(['Wrong inputs! Number of minuends and subtrahends must match!\n']);
            correct_inputs=false;
        end
        minuend=outstruct.minuend;
        subtrahend=outstruct.subtrahend;
    end
end



[EEG, diff_chan, ~] = cEEGrid_diffchan(EEG,ch_minuend, ch_subtrahend);


com = sprintf('[EEG, diff_chan]=pop_cEEGrid_diffchan( %s,{%s}, {%s});', inputname(1), vararg2str(ch_minuend), vararg2str(ch_subtrahend));
end