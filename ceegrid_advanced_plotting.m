function com = ceegrid_advanced_plotting(Data,dimVec,Labels,cfg,varargin)

% Plotting function for the cEEGrid electrode array. It allows to use 
% fieltrip plotting functions outside of fieldtrip (http://www.ru.nl/neuroimaging/fieldtrip) e.g. use it from within eeglab.
% This functions requires the fieldtrip toolbox to be in your matlab path. 
% If you are working with fieldtrip you can simply use the
% layout(cEEGridLayout.mat)
% and use your standard filedtrip workflow. 
% 
% plots event-related potentials, frequency spectra for the, aswell as topographies
% cEEGrid. Multiple datasets can be overlayed. The plots are arranged
% accourding to their location specified in the layout file (cEEGridLayout.mat need to be in the script folder).
%  
%
% Data: channel x samples (x conditions)
% dimVec:  vector describing the samples (e.g. time points, frequencies)
% Labels: cell array of electrode names (e.g. {EEG.chanlocs.labels}) 
% The configuration cfg can have the following parameters 
%     cfg.plotSort      = field that contains the plotting function 
%                        'ft_multiplotER', 'ft_multiplotTFR' or 'ft_topoplotER' (default ft_multiplotER)
%     cfg.newfig       = create new figure for plot 'yes' or 'no' (default='yes')
%     cfg.zoom         = make the individual plots larger 'yes' or 'no' (default ='no')                 
%     all other configuration depend on the selected plotting function. 
%    
%
%     cfg.parameter     = field to be plotted on y-axis (default depends on data.dimord)
%                         'avg', 'powspctrm' or 'cohspctrm'
%     cfg.newfig = controls the creation of a new figure(default true)
%     cfg.maskparameter = field in the first dataset to be used for marking significant data
%     cfg.maskstyle     = style used for masking of data, 'box', 'thickness' or 'saturation' (default = 'box')
%     cfg.hlim          = 'maxmin' or [xmin xmax] (default = 'maxmin')
%     cfg.vlim          = 'maxmin', 'maxabs', 'zeromax', 'minzero', or [ymin ymax] (default = 'maxmin')
%     cfg.channel       = Nx1 cell-array with selection of channels (default = 'all'), see FT_CHANNELSELECTION for details
%     cfg.refchannel    = name of reference channel for visualising connectivity, can be 'gui'
%     cfg.baseline      = 'yes', 'no' or [time1 time2] (default = 'no'), see FT_TIMELOCKBASELINE or FT_FREQBASELINE
%     cfg.baselinetype  = 'absolute' or 'relative' (default = 'absolute')
%     cfg.trials        = 'all' or a selection given as a 1xN vector (default = 'all')
%     cfg.axes          = 'yes', 'no' (default = 'yes')
%                         Draw x- and y-axes for each graph
%     cfg.box           = 'yes', 'no' (default = 'no')
%                         Draw a box around each graph
%     cfg.comment       = string of text (default = date + colors)
%                         Add 'comment' to graph (according to COMNT in the layout)
%     cfg.showlabels    = 'yes', 'no' (default = 'no')
%     cfg.showoutline   = 'yes', 'no' (default = 'no')
%     cfg.fontsize      = font size of comment and labels (if present) (default = 8)
%     cfg.interactive   = Interactive plot 'yes' or 'no' (default = 'yes')
%                         In a interactive plot you can select areas and produce a new
%                         interactive plot when a selected area is clicked. Multiple areas
%                         can be selected by holding down the SHIFT key.
%     cfg.renderer      = 'painters', 'zbuffer', ' opengl' or 'none' (default = [])
%     cfg.linestyle     = linestyle/marker type, see options of the PLOT function (default = '-')
%                         can be a single style for all datasets, or a cell-array containing one style for each dataset
%     cfg.linewidth     = linewidth in points (default = 0.5)
%     cfg.graphcolor    = color(s) used for plotting the dataset(s) (default = 'brgkywrgbkywrgbkywrgbkyw')
%                         alternatively, colors can be specified as Nx3 matrix of RGB values
%     cfg.directionality = '', 'inflow' or 'outflow' specifies for
%                         connectivity measures whether the inflow into a
%                         node, or the outflow from a node is plotted. The
%                         (default) behavior of this option depends on the dimor
%                         of the input data (see below).
%     cfg.layout        = specify the channel layout for plotting using one of
%                         the supported ways (see below).
%  
%  example 
%        cfg=[];
%        cfg.channel='all'; % alternative:  cfg.channel={'Pin 5' 'L2'};
%        dimVec=EEG.times;
%        Data=EEG.data; %(channels*samples) alternative: cat(3,mean(EEG1.data,3),mean(EEG2.data,3)); 
%        Labels={EEG.chanlocs.labels}
%        ceegrid_advanced_plotting(Data,dimVec,Labels,cfg)
%  alternatively, cfg-parameters can be entered as additional input
%  variables (for supported variables see below)
%  example
%        ceegrid_advanced_plotting(Data,dimVec,Labels,[],'side','cEEGridLayout','newfig','no','plotSort','ft_multiplotER')

% 

%%
if nargin < 4
    help ceegrid_advanced_plotting;
    return;
end;
%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false); 
end

%% 
com = ''; % this initialization ensure that the function will return something
% string of cfg from call
if isempty(cfg)
    call_cfg='[]';
else
    call_cfg=vararg2str(cfg);
end

% load the correct layout file
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
%load([currentpath filesep 'cEEGridLayout.mat']);

if exist([currentpath filesep 'Layouts' filesep 'cEEGridLayout.mat'],'file')==0
    msg ='The cEEGrid LayoutFile cEEGridStandardLayout.mat needs to be in the script directory';
    error(msg);
end

%default values
if ~isfield(cfg,'interactive') cfg.interactive='yes'; end
if ~isfield(cfg,'linewidth') cfg.linewidth=2;end
if ~isfield(cfg,'parameter') cfg.parameter ='amp';end
if ~isfield(cfg,'showoutline') cfg.showoutline   = 'yes';end
%if ~isfield(cfg,'showlabels') cfg.showlabels= 'yes'; end
if ~isfield(cfg,'channel') cfg.channel='all';end
if ~isfield(cfg,'side') cfg.side = 'cEEGridLayout';end
if ~isfield(cfg,'plotSort') cfg.plotSort='ft_multiplotER';end
if ~isfield(cfg,'layout') 
            layoutFile=load([currentpath filesep 'Layouts' filesep cfg.side]);
            fields=fieldnames(layoutFile);
            cfg.layout= layoutFile.(fields{1});         
end

if ~isfield(cfg,'newfig') cfg.newfig='yes';end
if ~isfield(cfg,'zoom') cfg.zoom='no';end

% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions={};
for k=1:numel(layouts)
    layoutoptions{k}=layouts(k).name(1:end-4);
end

% parse input arguments for extra cfg options

    p = inputParser;
    validScalarPosNum = @(x) isnumeric(x) && isscalar(x) && (x > 0);
    
    addParameter(p,'interactive',cfg.interactive,@(x) any(validatestring(x,{'yes','no'})));
    addParameter(p,'linewidth',cfg.linewidth,validScalarPosNum);
    addParameter(p,'parameter',cfg.parameter,@(x) any(validatestring(x,{'avg', 'powspctrm', 'cohspctrm'})));
    addParameter(p,'showoutline',cfg.showoutline,@(x) any(validatestring(x,{'yes','no'})));
    addParameter(p,'channel',cfg.channel);
    addParameter(p,'side',cfg.side,@(x) any(validatestring(x,layoutoptions)));
    addParameter(p,'plotSort', cfg.plotSort,@(x) any(validatestring(x,{'ft_multiplotER', 'ft_multiplotTFR','ft_topoplotER'})));
    %addParameter(p,'layout', cfg.layout);
    addParameter(p,'newfig', cfg.newfig,@(x) any(validatestring(x,{'yes', 'no'})));
    addParameter(p,'zoom', cfg.zoom,@(x) any(validatestring(x,{'yes', 'no'})));

    parse(p,varargin{:});
    cfg.interactive=p.Results.interactive;
    cfg.linewidth=p.Results.linewidth;
    cfg.parameter=p.Results.parameter;
    cfg.showoutline=p.Results.showoutline;
    cfg.channel=p.Results.channel;
    cfg.side=p.Results.side;
    layoutFile=load([currentpath filesep 'Layouts' filesep cfg.side]);
    fields=fieldnames(layoutFile);
    cfg.layout= layoutFile.(fields{1}); 
    cfg.plotSort=p.Results.plotSort;
    %cfg.layout=p.Results.layout;
    cfg.newfig=p.Results.newfig;
    cfg.zoom=p.Results.zoom;




% increases the size of each electrode plot by a factor of 1.5
 if strcmp(cfg.zoom,'yes')
     cfg.layout.width=cfg.layout.width*1.5;
     cfg.layout.height=cfg.layout.height*1.5;
 end
 
% create new figure if necessary 
if strcmp(cfg.newfig,'yes')
    figure
end

%Checks whether the data come from a study (cell array) or a single dataset
%(matrix)
if iscell(Data)
	conditions = Data{2};
	Data = Data{1};
	%If conditions are numbers, changes them  to D1, D2, D3, etc.
	if sum(cell2mat(cellfun(@(x)~isempty(str2num(x)), conditions, 'UniformOutput', false)))>0
		conditions = cellfun(@(x)['D', x], conditions, 'UniformOutput', false);
	end
else
	for d=1:size(Data,3)
		conditions{d} = ['D' num2str(d)];
	end
end
%Creates the structures with the data to be analyzed
%The name of the structure is the name of the conditions if it comes from a
%study, and it is D1 if it comes from a dataset
nConditions = [];
for d=1:size(Data,3)
	du=num2str(d);
	eval([conditions{d}(~isspace(conditions{d})) '.amp=Data(:,:,' du ');']);
	eval([conditions{d}(~isspace(conditions{d})) '.dimord=''chan_time'';']);
	eval([conditions{d}(~isspace(conditions{d})) '.time=dimVec;']);
	eval([conditions{d}(~isspace(conditions{d})) '.label=Labels;']);
	nConditions=[nConditions ',' conditions{d}(~isspace(conditions{d}))];
end

eval([cfg.plotSort '(cfg' nConditions ');']);

% white figure background
set(gcf,'color','w');


% return the string command
% -------------------------
com = sprintf(['ceegrid_advanced_plotting(' vararg2str(Data) ',[' vararg2str(dimVec) '],{' vararg2str(Labels) '},' call_cfg fastif(strcmp(vararg2str(varargin),''), '' ,',') vararg2str(varargin) ');']);

return;
