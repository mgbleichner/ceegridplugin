% pop_cEEGrid_rename() - adds the correct labels for your cEEGrid recording
%		         depending on your connector layout.
%
% Usage:
% >> OUTEEG =pop_cEEGrid_rename(EEG, setup_cfg, addChannelLocations,AMP);

%  
% Inputs:
%   INEEG     	  - input EEG dataset
%   setup_cfg     - contains struct e.g.:struct('ch_18',{struct('channelnames',{{{'R08'},{'R07'},{'R06'},{'R05'},{'R04'},{'R03'},{'R02'},{'R01'},{'L08'},{'L07'},{'L06'},{'L05'},{'L04b'},{'L04a'},{'L04'},{'L03'},{'L02'},{'L01'}}},...
%                                                               'types',{{'EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG'}})})
%                   
%   addChannelLocations - 0 or 1, depending on if you want to add channel Locations
%   AMP           - your recording setup file name (without '.mat' ending).
%                   e.g.: 'smarting_v35' or 'smartphone_razor'
%                   setup file should be placed in cEEGrid/Setups folder
%
%    channel data can be entered as setup_cfg or as AMP. If setup_cfg is
%    not empty, data will be taken from setup_cfg. For example:
%
%   EEG = pop_cEEGrid_rename( EEG,[], 0 ,'smartphone_razor');
%
%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_rename(EEG, setup_cfg, addChannelLocations,AMP)

% the command output is a hidden output that does not have to
% be described in the header

com = ''; % this initialization ensure that the function will return something
% if the user press the cancel button

% display help if not enough arguments
% ------------------------------------
if nargin < 1
    help pop_cEEGrid_rename;
    return;
end
% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, ~, ~]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
labelarray=['']; % create string containing all layouts for popupmenu inside GUI I
for k=1:numel(layouts)
    labelarray=strcat(labelarray,layouts(k).name(1:end-4));
    if k<numel(layouts)
        labelarray=strcat(labelarray,'|');
    end
end
% get all saved setups from the setups directory
setups=dir([currentpath filesep 'Setups',filesep '*.mat']);
setupoptions=['|'];
if numel(setups)==0
    setupoptions=['No setup found'];
else
    for k=1:numel(setups)
        setupoptions=strcat(setupoptions,setups(k).name(1:end-4));
        if k<numel(setups)
            setupoptions=strcat(setupoptions,'|');
        end
    end
end


% pop up window
% -------------

if nargin < 2
    %     use_setup=false;
    %     while use_setup==false
    %% GUI I for layout selection and recording setup selection
    guititle='cEEGrid_rename -- pop_cEEGrid_rename -- setup selection';
    geom={[1 2] };%[1] [1] [1] };
    uilist={
        { 'style', 'text', 'string', 'Select Grid:', 'fontweight', 'bold' },...
        { 'style', 'popupmenu', 'string', labelarray}...
        };
    %{}...
    %             {'Style', 'checkbox', 'string' 'Use saved setup' 'value' 0 'tag' 'saved_setup'}...
    %             { 'style', 'popupmenu', 'string', setupoptions ,'tag' 'setup' } ...
    %             };
    [result, ~, ~, outstruct]=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_rename'')', guititle, [], 'normal');
    % catch cancelling-event
    if isempty(result), return; end
    
    % get the selected layout file
    side = layouts((result{1})).name(1:end-4);
    layoutFile=load([currentpath filesep 'Layouts' filesep side]);
    fields=fieldnames(layoutFile);
    layout= layoutFile.(fields{1});
    
    
    %% setup for next GUI (GUI II or Rename GUI)
    
    % Default values for edit fields of GUI II
    for i=1:EEG.nbchan
        label_input{i}= EEG.chanlocs(i).labels;
        label_edit{i}={''};
        type_edit{i}=EEG.chanlocs(i).type;
    end
    %     end
    
    AMP='';
    %     if strcmp(AMP,'')
    %         AMP='default_setting';
    %     end
    addChannelLocations=0;
    setup_val =1;
    do_save=0;
    do_save_channeltypes=0;
    use_savename=false;
    while use_savename==false
        %% Plot of layout and channel labels
        grid=figure;
        set(gcf,'position',[5 300 400 300]) %  position and size of Plot, so its next to GUI II
        hold on
        for i=1:size(layout.outline,2) % plot outlines of selected grid
            plot(layout.outline{1,i}(:,1),layout.outline{1,i}(:,2),'k')
        end
        for i=1:size(layout.label,1)-2 % add labels
            text(layout.pos(i,1),layout.pos(i,2),layout.label(i,1),'HorizontalAlignment','center');
        end
        title(side);
        axis off
        hold off
        %     figure;
        
        
        
        %% GUI II: to rename existing channels to channelnames of selected layout
        guititle_2='cEEGrid_rename -- pop_cEEGrid_rename -- rename channels';
        geom_2={[1 4 ] [2 3 3 3] };
        geomv = [1.5 1]  ;
        
        %GUI II headline
        uilist_2={{'style', 'text', 'string', 'Setups: ', 'fontweight', 'bold'}...
            { 'style', 'popupmenu', 'string', setupoptions ,'value' setup_val 'tag' 'setup' 'callback', 'set(findobj( ''tag'', ''ok''), ''userdata'', ''reload'');'}...
            { 'style', 'text', 'string', 'Channel index', 'fontweight', 'bold' }...
            { 'style', 'text', 'string', 'Channel names', 'fontweight', 'bold' }...
            { 'style', 'text', 'string', 'New names', 'fontweight', 'bold' }...
            { 'style', 'text', 'string', 'Channel type', 'fontweight', 'bold' }...
            };
        
        % Create edit fields dynamically
        for i=1:EEG.nbchan
            uilist_2{1,numel(uilist_2)+1}={ }; % empty field, so the distance between channel index and edit field is smaller
            uilist_2{1,numel(uilist_2)+1}={ 'style', 'text', 'string', num2str(i), 'fontweight', 'bold'  }; % channel index
            uilist_2{1,numel(uilist_2)+1}={ 'Style', 'text', 'string' label_input{i}   }; % edit field for channel name
            uilist_2{1,numel(uilist_2)+1}={ 'Style', 'edit', 'string' label_edit{i}  'tag' ['label_' num2str(i)] }; % edit field for channel name
            uilist_2{1,numel(uilist_2)+1}={ 'Style', 'edit', 'string' type_edit{i}  'tag' ['type_' num2str(i)] }; % edit field for channel type
            geom_2{1,numel(geom_2)+1}=[1 1 3 3 3]; % geometry for new fields
            
            geomv(1,numel(geomv)+1)=1.1; % vertical geometry for new fields (sets distance between rows)
        end
        
        %End of GUI II
        uilist_2{1,numel(uilist_2)+1}={};
        uilist_2{1,numel(uilist_2)+1}={ 'Style', 'checkbox', 'string' 'Add channel locations' 'value' addChannelLocations 'tag' 'addChannelLocations'};
        uilist_2{1,numel(uilist_2)+1}={ 'Style', 'checkbox', 'string' 'Save setup as:' 'value' do_save , 'tag' 'do_save'};
        uilist_2{1,numel(uilist_2)+1}={ 'Style',  'edit',    'string', AMP 'tag' 'save_name'  };
        uilist_2{1,numel(uilist_2)+1}={ 'Style', 'checkbox', 'string' 'Also save channel types' 'value' do_save_channeltypes , 'tag' 'do_save_channeltypes'};
        geom_2{1,numel(geom_2)+1}=[1];
        geom_2{1,numel(geom_2)+1}=[1];
        geom_2{1,numel(geom_2)+1}=[1 2];
        geom_2{1,numel(geom_2)+1}=[1];
        geomv(1,numel(geomv)+1)=1;
        geomv(1,numel(geomv)+1)=1;
        geomv(1,numel(geomv)+1)=1;
        geomv(1,numel(geomv)+1)=1;
        GUI_II_pos = [408 50];
        [result, ~, userdata, outstruct]=inputgui('geometry', geom_2,'geomvert',geomv,'uilist', uilist_2,'helpcom', 'pophelp(''pop_cEEGrid_rename'')','title', guititle_2, 'mode', 'normal','screenpos',GUI_II_pos);
        
        delete(grid); % close figure containing the layout
        
        % catch cancelling-event
        if isempty(result), return; end
        for i=1:EEG.nbchan
            type_edit{i}=outstruct.(['type_' num2str(i)]);
        end
        if ~strcmp(userdata,'reload')
            %% Get input data from GUI II
            do_save_channeltypes=outstruct.do_save_channeltypes;
            for i=1:EEG.nbchan
                if ~strcmp(outstruct.(['label_' num2str(i)]),'')
                    setup_cfg.(['ch_' num2str(EEG.nbchan)]).channelnames{i}=outstruct.(['label_' num2str(i)]);
                else
                    setup_cfg.(['ch_' num2str(EEG.nbchan)]).channelnames{i}={EEG.chanlocs(i).labels};
                end
                if do_save_channeltypes==1
                    setup_cfg.(['ch_' num2str(EEG.nbchan)]).types{i}=type_edit{i};
                end
                    
                label_edit{i}=outstruct.(['label_' num2str(i)]);
                
            end
            addChannelLocations=outstruct.addChannelLocations;
            do_save=outstruct.do_save;
            
            AMP=outstruct.save_name;
            if strcmp(AMP,'')
                AMP='default_setting';
            end
            
            
            
            
            %% save setup data in 'Setups' folder
            if do_save==1
                if ~isfolder([currentpath filesep 'Setups'])
                    mkdir([currentpath filesep 'Setups']);
                end
                savename=[currentpath filesep 'Setups',filesep strrep(AMP,' ','_') '.mat'];
                
                if exist(savename, 'file') == 2
                    %% Warning-Dialog: File exists already.
                    answer=questdlg2(['Filename already exists. Press ''Ignore'' to continue without saving.'],...
                        'File exists already.','Rename', 'Overwrite', 'Ignore', 'Ignore');
                    % Handle response
                    switch answer
                        case 'Rename'
                            
                        case 'Overwrite'
                            use_savename=true;
                            save(savename,'setup_cfg');
                        case 'Ignore'
                            use_savename=true;
                    end
                else
                    % File does not exist and can be saved without further actions
                    use_savename=true;
                    save(savename,'setup_cfg');
                end
            else
                use_savename=true;
            end
        else
            %Get setup name
            
            if numel(setups)==0
                AMP='';
            else
                if outstruct.setup==1
                    AMP='';
                    for i=1:EEG.nbchan
                        label_edit{i}={''};
                    end
                    setup_val=outstruct.setup;
                else
                AMP=setups(outstruct.setup-1).name(1:end-4);
                setup_val=outstruct.setup;
                end
            end
            %% Load setup file
            if ~strcmp(AMP,'')
                setupFile=load([currentpath filesep 'Setups' filesep AMP '.mat']);
                fields=fieldnames(setupFile);
                setup_cfg=setupFile.(fields{1});
                
                
                % Select correct channel set (correct channel number)
                cfg_fields = fieldnames(setup_cfg);
                cfg_channelnumbers=[];
                sub_setup_nr=[];
                data_format=true;
                if strcmp(cfg_fields{1}(1:3),'ch_')
                    for i = 1:numel(cfg_fields)
                        cfg_channelnumbers(i)= str2num(cfg_fields{i}(4:end));
                    end
                    
                    sub_setup_nr=find(cfg_channelnumbers==EEG.nbchan);
                    
                else
                    %warning dialog -> wrong setup file format
                    warndlg2('Data format doesn''t match, maybe an old Setup file was selected.', 'Wrong Data Format');
                    setup_val=1;
                    data_format=false;
                end
                if ~isempty(sub_setup_nr)
                    sub_setup=cfg_fields{sub_setup_nr};
                    for i=1:EEG.nbchan
                        
                        label_edit{i}=setup_cfg.(sub_setup).channelnames{i};
                        
                    end
                elseif isempty(sub_setup_nr)&&data_format==true
                    %warning dialog -> channelnumbers do not match
                    %    provide option, to select one of the existing sub-setups
                    % load this setup
                    % maybe set setup_val=1;
                    subsetupoptions=[''];
                    
                    
                    for k=1:numel(cfg_fields)
                        subsetupoptions=strcat(subsetupoptions,cfg_fields{k});
                        if k<numel(cfg_fields)
                            subsetupoptions=strcat(subsetupoptions,'|');
                        end
                    end
                   
                    %% GUI III warning channelmissmatch
                    guititle='Warning Channel-missmatch';
                    geom={[1] [1 2] };%[1] [1] [1] };
                    warning_msg = ['Channel-numbers do not match! Your data has ' num2str(EEG.nbchan) ' channels. ' AMP ' has not.'];
                        
                    uilist={{'style', 'text', 'string', warning_msg, 'fontweight', 'bold'}...
                    { 'style', 'checkbox', 'string', ['Use following subset of ' AMP ':'], 'tag' 'use_subset' }...
                    { 'style', 'popupmenu', 'string', subsetupoptions, 'tag' 'subset'}
                    };
                   
                    [~, ~, ~, outstruct_w]=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_rename'')', guititle, [], 'normal');
                    if outstruct_w.use_subset==1
                        sub_setup=cfg_fields{outstruct_w.subset};
                        for i=1:EEG.nbchan
                            if i<=numel(setup_cfg.(sub_setup).channelnames)
                                label_edit{i}=setup_cfg.(sub_setup).channelnames{i};
                            else
                                label_edit{i}={''};
                            end
                        end
                    else
                        setup_val=1;
                        for i=1:EEG.nbchan
                            label_edit{i}={''};
                        end
                        
                    end
                end
            end
            
        end
    end
    channelnames=setup_cfg.(['ch_' num2str(EEG.nbchan)]).channelnames; % Only pass channelnames
    setup_cfg=[];
    setup_cfg.(['ch_' num2str(EEG.nbchan)]).channelnames=channelnames;
    for i=1:EEG.nbchan
        setup_cfg.(['ch_' num2str(EEG.nbchan)]).types{i}=type_edit{i};
    end
   
    %check if setupname exists
    savename=[currentpath filesep 'Setups',filesep strrep(AMP,' ','_') '.mat'];
                
    if exist(savename, 'file') == 2
        %load setup
            setupFile=load(savename);
        %compare setups
        if isfield(setupFile.setup_cfg, ['ch_' num2str(EEG.nbchan)])
            if isequaln(setup_cfg.(['ch_' num2str(EEG.nbchan)]),setupFile.setup_cfg.(['ch_' num2str(EEG.nbchan)]))
                setup_cfg=[];
            end
        end
    else
        AMP=[];
    end
    
    
    %
    
else
    if nargin == 2 % if input value is not given, set to 0 by default.
        addChannelLocations=0;
        AMP=[];
    end
    
end




EEG = cEEGrid_rename(EEG, setup_cfg, addChannelLocations,AMP);





% return the string command
% -------------------------
com = sprintf(['EEG = pop_cEEGrid_rename( %s, ' fastif(isempty(setup_cfg),'[]',vararg2str(setup_cfg)) ', %d, ''%s'' );'], inputname(1), addChannelLocations,AMP);

return;
