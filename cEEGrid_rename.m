% cEEGrid_rename() - adds the correct labels for your cEEGrid recording
%		         depending on your connector layout.
%
% Usage:
% >> OUTEEG =cEEGrid_rename(EEG, setup_cfg, addChannelLocations,AMP);

%
% Inputs:
%   INEEG     	  - input EEG dataset
%   setup_cfg     - contains struct with channelnames e.g.:struct('ch_18',{struct('channelnames',{{{'R08'},{'R07'},{'R06'},{'R05'},{'R04'},{'R03'},{'R02'},{'R01'},{'L08'},{'L07'},{'L06'},{'L05'},{'L04b'},{'L04a'},{'L04'},{'L03'},{'L02'},{'L01'}}},...
%                                                                                 'types',{{'EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG','EEG'}})})
%   addChannelLocations - 0 or 1, depending on if you want to add channel Locations
%   AMP           - your recording setup file name (without '.mat' ending).
%                   e.g.: 'smarting_v35' or 'smartphone_razor'
%                   setup file should be placed in cEEGrid/Setups folder
%
%    channel data can be entered as setup_cfg or as AMP. If setup_cfg is
%    not empty, data will be taken from setup_cfg. For example:
%
%    EEG = cEEGrid_rename( EEG,[], 0 ,'smartphone_razor');
%
%
%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA



function [EEG, com] = cEEGrid_rename(EEG, setup_cfg, addChannelLocations,AMP)
%% return the string command
% -------------------------
com = sprintf(['EEG = cEEGrid_rename( %s, ' fastif(isempty(setup_cfg),'[]',vararg2str(setup_cfg)) ', %d, ''%s'' );'], inputname(1), addChannelLocations,AMP);
%com = ''; % this initialization ensure that the function will return something

if nargin < 2
    help cEEGrid_rename;
    return;
end
%% Load Setup file
if isempty(setup_cfg) % if setup_cfg is empty, get channelnames and types from setup file
    if isempty(AMP)||strcmp(AMP,'')
        help cEEGrid_rename;
        return;
    end
    scriptName = mfilename('fullpath');
    [currentpath, ~, ~]= fileparts(scriptName);
    setupFile=load([currentpath filesep 'Setups' filesep AMP '.mat']);
    setup_cfg=setupFile.setup_cfg;
    
    % fields=fieldnames(setupFile);
    % setup_cfg=setupFile.(fields{1});
end


% check if setup_cfg has a subset with correct amount of channels
if isfield(setup_cfg, ['ch_' num2str(EEG.nbchan)])
    setup_cfg_sel=setup_cfg.(['ch_' num2str(EEG.nbchan)]);
    
else %option to choose subset via console
    found_setup=0;
    while found_setup==0
        fieldnames_cfg= fieldnames(setup_cfg);
        prompt = ['No subset with ' num2str(EEG.nbchan) ' channels found. Choose a subset from ' vararg2str(fieldnames_cfg) ', or type ''exit'':\n'];
        x = input(prompt);
        if strcmp(x,'exit')
            return
        end
        if isfield(setup_cfg, x)
            setup_cfg_sel=setup_cfg.(x);
            found_setup=1;
        else
            prompt ='This subset does not exist, try again? ''Y''/''N''\n';
            x  = input(prompt);
            if strcmp(x,'N')
                return;
            end
        end
    end
end

%% Rename channels and change type
for ch=1:EEG.nbchan
    
    EEG.chanlocs(ch).labels=setup_cfg_sel.channelnames{ch}{1};
    if isfield(setup_cfg_sel, 'types')
        EEG.types(ch).type=setup_cfg_sel.types{ch};
    end
end




%% add channels if requested
if nargin == 2 % if input value is not given, set to 0 by default.
    addChannelLocations=0;
end
if addChannelLocations
    display('Adding channel locations');
    scriptName = mfilename('fullpath');
    [currentpath, filename, fileextension]= fileparts(scriptName);
    channelLocationFile=[currentpath filesep 'elec_cEEGrid.elp'];
    EEG=pop_chanedit(EEG, 'lookup',channelLocationFile);
end

