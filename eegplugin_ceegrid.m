% eegplugin_ceegrid() - A plugin for plotting cEEGrid data using fieltrip functions. 
% 
% Usage:
%   >> eegplugin_ceegrid(fig,try_strings, catch_strings); 
%
%   
% Author: Martin Georg Bleichner, Neurophysiology of everyday life lab , University of Oldenburg 2019
%		   
% History:
% 03/06/2021 ver 0.90 by Lukas
% 08/06/2020 ver 0.60 by Bertille adding the Study functions
% 17/10/2018 ver 0.40 by Martin 
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function vers= eegplugin_ceegrid(fig,try_strings, catch_strings)
    
vers = 'ceegrid_0.9';

% Check for fieldtrip dependencies
% if ~plugin_status('fieldtrip-lite')
%     display('The fieldtrip-lite plugin is not installed. Please install the fieldtrip-lite plugin manually.'); 
%     
%     plugin_askinstall('fieldtrip-lite', [], false); 
%     this does not work here. It is therefore implemented inside the
%     functions that need it, like 'ceegrid_advanced_plotting.m'
% end
    

mu= findobj(fig, 'tag', 'tools');

cmd =[try_strings.no_check '[EEG LASTCOM] = pop_cEEGrid_rename(EEG);'];
cmd=[cmd '[ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG, CURRENTSET);' catch_strings.add_to_hist];

finalcmd=cmd;
uimenu( mu, 'label', 'cEEGrid rename', 'callback', finalcmd);
%[EEG, diff_chan, com] = pop_cEEGrid_diffchan(EEG,ch_minuend, ch_subtrahend)
uimenu(mu, 'label', 'cEEGrid diff. channels', 'callback', [try_strings.no_check '[EEG diff_chan LASTCOM] = pop_cEEGrid_diffchan(EEG); [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);' catch_strings.add_to_hist], 'userdata','chanloc:on');

mu= findobj(fig, 'tag', 'plot');

uimenu(mu, 'label', 'cEEGrid channel ERP', 'callback', [try_strings.no_check '[EEG LASTCOM] = pop_cEEGrid_multiplot(EEG); [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);' catch_strings.add_to_hist], 'userdata','chanloc:on');
uimenu(mu, 'label', 'cEEGrid topography', 'callback', [try_strings.no_check  '[EEG LASTCOM] = pop_cEEGrid_topoplot(EEG); [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);'  catch_strings.add_to_hist], 'userdata','chanloc:on');
uimenu(mu, 'label', 'cEEGrid plot filtered data', 'callback', [try_strings.no_check  '[EEG EEG_filter_dat LASTCOM] = pop_cEEGrid_filterplot(EEG); [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);'  catch_strings.add_to_hist], 'userdata','chanloc:on');
uimenu(mu, 'label', 'cEEGrid spectopo', 'callback', [try_strings.no_check  '[ LASTCOM] = pop_cEEGrid_spectopo(EEG, 1); [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);'  catch_strings.add_to_hist], 'userdata','chanloc:on');
uimenu(mu, 'label', 'cEEGrid ICA plot', 'callback', [try_strings.check_ica '[EEG LASTCOM] = pop_cEEGrid_icaplot(EEG);[ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);'  catch_strings.store_and_hist ], 'userdata','chanloc:on');
%uimenu(mu, 'label', 'cEEGrid ICA topography', 'callback', [try_strings.check_ica '[EEG LASTCOM] = pop_cEEGrid_icatopoplot(EEG);[ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);'  catch_strings.store_and_hist ], 'userdata','chanloc:on');
%uimenu(mu, 'label', 'cEEGrid ICA components', 'callback', [try_strings.check_ica '[EEG LASTCOM] = pop_cEEGrid_icacomponents(EEG);[ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG,EEG, CURRENTSET);' catch_strings.store_and_hist  ], 'userdata','chanloc:on');

mu = findobj(fig, 'tag', 'study');
uimenu(mu, 'label', 'Plotting cEEGrid', 'callback', [try_strings.no_check '[STUDY, ALLEEG, LASTCOM] = pop_ceegrid_advanced_plotting_std(STUDY, ALLEEG); STUDY = std_checkset(STUDY, ALLEEG);' catch_strings.add_to_hist], 'userdata','study:on');
uimenu(mu, 'label', 'Cluster components by correlation (cEEGrid-CORRMAP)', 'callback', ...
    [try_strings.check_ica '[CORRMAP STUDY ALLEEG LASTCOM]= pop_cEEGrid_corrmap(STUDY,ALLEEG);' catch_strings.add_to_hist ], 'userdata', 'startup:off;study:on');
