%This  is to build the GUI for pop_cEEGrid_topoplot.m
% Inputs:
% -Labels       Array of all Labels from Dataset
% -num_chan     Number of channels
% -x_min        minimum x value
% -x_max        maximum x value
% 
% Outputs:
% -plot_call_cfg        string containing the config structure
% -plot_call_vararg     string containing the key value pairs for the plot
% -timepoints           Can be a single or a set of two values
% -DataLabels           Array with selected channel names
%
% Usage:
% Labels={EEG.chanlocs.labels};
% [plot_call_cfg,plot_call_vararg,timepoints, DataLabels]= cEEGrid_topoplot_GUI(Labels,EEG.nbchan,EEG.xmin,EEG.xmax);
%
% For more information check pop_cEEGrid_topoplot.m
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
function [plot_call_cfg,plot_call_vararg,timepoints, DataLabels]= cEEGrid_topoplot_GUI(Labels,num_chan,x_min,x_max)

plot_call_cfg=[];
plot_call_vararg=[];
timepoints=[];
DataLabels=[];
%%
    % get all layouts from the layouts directory
    scriptName = mfilename('fullpath');
    [currentpath, filename, fileextension]= fileparts(scriptName);
    layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
    layoutoptions=[''];
    for k=1:numel(layouts)
        layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
        if k<numel(layouts)
            layoutoptions=strcat(layoutoptions,'|');
        end
    end
    %% GUI setup
    geom={[1 1]  [1 1] 1 [1 1] [1 1 ] [1] [1] [1 1 2 2] [1]  [5 1] [2 1 2 1] [2 1 2 1] [2 1 2 1] [2 1 2 1] [2 1 2 1] [2 1 2 1] [2 1 1 2] [2 1 2 1]};
    guititle='cEEGrid topoplotER -- pop_cEEGrid_topoplot';
    txtwhat2plot1 = 'Plotting ERP scalp maps at this latency';
    txtwhat2plot2 = sprintf('(range: %d to %d ms, two values -> mean over time range):', ...
        round(x_min*1000), round(x_max*1000));
    editwhat2plot = '';
    editwhichchannels = '';
    editplottitle = '';%fastif(~isempty(EEG.setname), [EEG.setname], '');
    popup_layoutoptions_value=1;
    checkbox_newfig_value=1;
    checkbox_interactive=1;
    edit_zlim='maxmin';
    popup_marker_value=1;
    popup_markersymbol_value=1;
    edit_markercolor='[0 0 0]';
    edit_markersize = '2';
    edit_markerfontsize = '8';
    popup_highlight_value= 4;
    edit_highlightchannel='';
    popup_highlightsymbol_value=1;
    edit_highlightcolor = '[0 0 0]';
    edit_highlightsize='6';
    edit_highlightfontsize='8';
    popup_colorbar_value=2;
    edit_colorbartext='';
    popup_comment_value=2;
    edit_fontsize='8';
    correct_inputs=false;
    while correct_inputs==false % recreate GUI, until all GUI inputs are correct, or function is returned
        correct_inputs=true; %so far, all inputs are correct. This will be set to false later, if a wrong input value is detected
        
        uilist={{ 'style', 'text', 'string', 'Channels subset (Vector containing channel numbers, []=all)', 'fontweight', 'bold' },...
            { 'style', 'edit', 'string', editwhichchannels 'tag' 'channels' } ,...
            { 'style', 'text', 'string', txtwhat2plot1, 'fontweight', 'bold'},...
            { 'style', 'edit', 'string', editwhat2plot 'tag' 'latency'},...
            { 'style', 'text', 'string', txtwhat2plot2 },...
            { 'style'   'text'     'string'    'Plot title', 'fontweight', 'bold' } ...
            { 'style'   'edit'     'string'  editplottitle  'tag' 'plottitle' } ...
            { 'style', 'text', 'string', 'Select Grid:', 'fontweight', 'bold' },...
            { 'style', 'popupmenu', 'string', layoutoptions 'value' popup_layoutoptions_value 'tag' 'layoutoptions'},...
            {}...
            { 'style'   'text'     'string'    'Additional plot options: ', 'fontweight', 'bold'  } ...
            {'Style', 'checkbox', 'string' 'New figure for the plot' 'value' checkbox_newfig_value 'tag' 'newfig'}...
            { 'Style', 'checkbox', 'string' 'Create interactive plot' 'value' checkbox_interactive 'tag' 'interactive' }, ...
            {}...
            {}...
            {'Style'   'text'     'string' 'Axis limits: ', 'fontweight', 'bold'}...
            {'Style'   'text'     'string' 'Limits for color dimension, ''maxmin'', ''maxabs'', ''zeromax'', ''minzero'', or [zmin zmax] (default = ''maxmin'')'}...
            {'Style', 'edit', 'string', edit_zlim 'tag' 'zlim'}...
            {'style'   'text'     'string'    'Marker: ', 'fontweight', 'bold'}...
            {'style', 'popupmenu', 'string', 'on|labels|numbers|off' 'value' popup_marker_value 'tag' 'marker'}...
            {}...
            {}...
            {'Style'   'text'     'string'    'Channel marker symbol (default = ''o''): '}...
            {'Style', 'popupmenu', 'string', 'o|+|*|.|x|Square|Diamond|Pentagram|Hexagram|Upward-pointing triangle|Downward-pointing triangle|Right-pointing triangle|Left-pointing triangle' 'value' popup_markersymbol_value  'tag' 'markersymbol'}...
            {'Style'   'text'     'string'    'Channel marker color (default = [0 0 0] (black)): '}...
            {'Style', 'edit', 'string', edit_markercolor 'tag' 'markercolor'}...
            {'style'   'text'     'string'    'Channel marker size (default = 2): '}...
            {'Style', 'edit', 'string', edit_markersize 'tag' 'markersize'}...
            {'style'   'text'     'string'    'Font size of channel labels (default = 8 pt): '}...
            {'Style', 'edit', 'string', edit_markerfontsize 'tag' 'markerfontsize'}...
            {'style'   'text'     'string'    'Highlight: ', 'fontweight', 'bold'}...
            {'style', 'popupmenu', 'string', 'on|labels|numbers|off' 'value' popup_highlight_value 'tag' 'highlight'}...
            {'style'   'text'     'string'    'Vector with indices of channels to be highlighted: '}...
            {'Style', 'edit', 'string', edit_highlightchannel 'tag' 'highlightchannel'}...
            {'Style'   'text'     'string'    'Highlight marker symbol (default = ''o''): '}...
            {'Style', 'popupmenu', 'string', 'o|+|*|.|x|Square|Diamond|Pentagram|Hexagram|Upward-pointing triangle|Downward-pointing triangle|Right-pointing triangle|Left-pointing triangle' 'value' popup_highlightsymbol_value 'tag' 'highlightsymbol'}...
            {'Style'   'text'     'string'    'Highlight marker color (default = [0 0 0] (black)): '}...
            {'Style', 'edit', 'string', edit_highlightcolor 'tag' 'highlightcolor'}...
            {'style'   'text'     'string'    'Highlight marker size (default = 6): '}...
            {'Style', 'edit', 'string', edit_highlightsize 'tag' 'highlightsize'}...
            {'style'   'text'     'string'    'Font size of highlight labels (default = 8 pt): '}...
            {'Style', 'edit', 'string', edit_highlightfontsize 'tag' 'highlightfontsize'}...
            {'style'   'text'     'string'    'Colorbar: ', 'fontweight', 'bold'}...
            {'Style', 'popupmenu', 'string', 'yes|no|North|South|East|West|NorthOutside|SouthOutside|EastOutside|WestOutside' 'value' popup_colorbar_value 'tag' 'colorbar'}...
            {'style'   'text'     'string'    'Colorbar text: '}...
            {'Style', 'edit', 'string', edit_colorbartext 'tag' 'colorbartext'}...
            {'style'   'text'     'string'    'Comment (''no'', ''auto'' or ''xlim'' (default = ''auto'')): ', 'fontweight', 'bold'}...
            {'Style', 'popupmenu', 'string', 'no|auto|xlim' 'value' popup_comment_value 'tag' 'comment'}...
            {'Style'   'text'     'string' 'Font size of comment and labels (if present) (default = 8): '}...
            {'Style', 'edit', 'string',edit_fontsize 'tag' 'fontsize'}...
            };
        
        
        
        
        
        
        [result, ~, ~, outstruct]=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_topoplot'')', guititle, [], 'normal');
        
        % catch cancelling-event
        if isempty(result), return; end
        
        
        
        
        
        
        
        cfg.side = layouts((result{4})).name(1:end-4); % get the layout file
        timepoints=str2num(result{2});
        
        
        
        if isempty(result{1})
            %DataLabels={EEG.chanlocs.labels};
            DataLabels=[];
        else
            %DataLabels=strsplit(result{1});
            
            founderror=false;
            if ~isempty(str2num(result{1}))
                values=str2num(result{1});
                if min(values)<1
                    founderror=true;
                end
            end
            if ((isempty(str2num(result{1})))||(numel(str2num(result{1}))<3)||founderror||(numel(str2num(result{1}))>num_chan)||(max(str2num(result{1}))>num_chan)||max(histcounts(str2num(result{1})))>1)
                
                correct_inputs=false;
                %error('To select channels you have to enter an array of channel indices, e.g.: ''1 2 3 4 5 9''. The array must contain at least 3 values');
                errordlg2(sprintf('To select channels you have to enter an array of unique channel indices, e.g.: ''1 2 3 4 5 9''. \nThe array must contain minimum 3 and maximum %d integer values between 1 and %d.',num_chan,num_chan), 'Error in channel selection');
            else
               
                DataLabels=Labels(str2num(result{1}));
            end
        end
        
        %% get additional fieldtrip cfg options from gui:
        yes_no = { 'no','yes'};
        marker_opt = {'on','labels','numbers','off'};
        markersymbol_opt = {'o','+','*','.','x','s','d','p','h','^','v','>','<'};
        colorbar_opt = {'yes','no','North','South','East','West','NorthOutside','SouthOutside','EastOutside','WestOutside'};
        comment_opt={'no','auto','xlim'};
        zlim_opt={'maxmin', 'maxabs', 'zeromax', 'minzero'};
        cfg.newfig = yes_no{outstruct.newfig+1};
        cfg.interactive = yes_no{outstruct.interactive+1};
        zlim=outstruct.zlim;
        if max(strcmp(zlim_opt,zlim))==0
            zlim=str2num(zlim);
        end
        cfg.zlim=zlim;
        %marker options
        cfg.marker = marker_opt{outstruct.marker};
        cfg.markersize = str2double(outstruct.markersize);
        cfg.markersymbol = markersymbol_opt{outstruct.markersymbol};
        markercolor=outstruct.markercolor;
        if strcmp(markercolor(1),'[')
            markercolor=str2num(markercolor);
        end
        cfg.markercolor=markercolor;
        cfg.markerfontsize = str2double(outstruct.markerfontsize);
        
        %highlight options
        cfg.highlight = marker_opt{outstruct.highlight};
        cfg.highlightchannel = str2num(outstruct.highlightchannel);
        if isempty(cfg.highlightchannel)
            cfg.highlightchannel='all';
        end
        cfg.highlightsymbol = markersymbol_opt{outstruct.highlightsymbol};
        highlightcolor=outstruct.highlightcolor;
        if strcmp(highlightcolor(1),'[')
            highlightcolor=str2num(highlightcolor);
        end
        cfg.highlightcolor = highlightcolor;
        cfg.highlightsize = str2double(outstruct.highlightsize);
        cfg.highlightfontsize = str2double(outstruct.highlightfontsize);
        %colorbar
        cfg.colorbar = colorbar_opt{outstruct.colorbar};
        cfg.colorbartext = outstruct.colorbartext;
        %comment
        cfg.comment=comment_opt{outstruct.comment};
        cfg.fontsize = str2num(outstruct.fontsize);
        %% Check edit inputs
        %latency
        if isempty(str2num(outstruct.latency))||min(str2num(outstruct.latency))<round(x_min*1000)||max(str2num(outstruct.latency))>round(x_max*1000)||numel(str2num(outstruct.latency))>2
            correct_inputs=false;
            errortitle='';
            
            if isempty(str2num(outstruct.latency))
                errortitle='No number entered!';
            elseif min(str2num(outstruct.latency))<round(x_min*1000)||max(str2num(outstruct.latency))>round(x_max*1000)
                errortitle='Input out of range!';
            elseif numel(str2num(outstruct.latency))>2
                errortitle='To many numbers entered!';
            end
            errordlg2(sprintf([errortitle '\nYou have to enter 1 or 2 numbers between %d and %d for latency.'],round(x_min*1000),round(x_max*1000)), ['Error: Wrong latency input: ' errortitle]);
            
        elseif ~isempty(outstruct.highlightchannel)
            founderror=false;
            if ~isempty(str2num(outstruct.highlightchannel))
                values=str2num(outstruct.highlightchannel);
                if min(values)<1
                    founderror=true;
                end
            end
            if ((isempty(str2num(outstruct.highlightchannel)))||founderror||(numel(str2num(outstruct.highlightchannel))>num_chan)||(max(str2num(outstruct.highlightchannel))>num_chan)||max(histcounts(str2num(outstruct.highlightchannel)))>1)
                
                correct_inputs=false;
                %error('To select channels you have to enter an array of channel indices, e.g.: ''1 2 3 4 5 9''.);
                errordlg2(sprintf('To select channels to be highlighted you have to enter an array of unique channel indices, e.g.: ''1 2 3 4 5 9''. \nThe array can only contain up to %d values between 1 and %d.',num_chan,num_chan), 'Error in channel highlight selection');
            end
        elseif min([cfg.markersize cfg.highlightsize cfg.markerfontsize cfg.highlightfontsize cfg.fontsize])<=0||max(isnan([cfg.markersize cfg.highlightsize cfg.markerfontsize cfg.highlightfontsize cfg.fontsize]))==1
            correct_inputs=false;
            errordlg2(sprintf('Marker size and font size values should be finite numbers larger than 1.'),'Error in marker size or font size');
        elseif max(strcmp(zlim_opt,outstruct.zlim))==0&&(isempty(str2num(outstruct.zlim))||numel(str2num(outstruct.zlim))~=2||(numel(str2num(outstruct.zlim))==2&&zlim(1)>=zlim(2)))
            correct_inputs=false;
            errordlg2(sprintf('Limits for color dimension has to be out of [''maxmin'', ''maxabs'', ''zeromax'', ''minzero''], \nor [zmin zmax], where zmax > zmin.'),'Error in color dimension Limits input');
        end
        
        %% In case of wrong inputs, get inputs from GUI to rebuild GUI with entered values
        if ~correct_inputs
            editwhichchannels = result{1};
            editwhat2plot =outstruct.latency;
            editplottitle = result{3};
            popup_layoutoptions_value=result{4};
            checkbox_newfig_value=outstruct.newfig;
            checkbox_interactive=outstruct.interactive;
            edit_zlim=outstruct.zlim;
            popup_marker_value=outstruct.marker;
            popup_markersymbol_value=outstruct.markersymbol;
            edit_markercolor=outstruct.markercolor;
            edit_markersize = outstruct.markersize;
            edit_markerfontsize = outstruct.markerfontsize;
            popup_highlight_value= outstruct.highlight;
            edit_highlightchannel=outstruct.highlightchannel;
            popup_highlightsymbol_value=outstruct.highlightsymbol;
            edit_highlightcolor = outstruct.highlightcolor;
            edit_highlightsize=outstruct.highlightsize;
            edit_highlightfontsize=outstruct.highlightfontsize;
            popup_colorbar_value=outstruct.colorbar;
            edit_colorbartext=outstruct.colorbartext;
            popup_comment_value=outstruct.comment;
            edit_fontsize=outstruct.fontsize;
        end
    end
    
    %% Prepare function call
    % default reference values (taken from cEEGrid_topoplot.m)
    cfg_ref=[];
    if ~isfield(cfg_ref,'interactive') cfg_ref.interactive='yes'; end
    if ~isfield(cfg_ref,'parameter') cfg_ref.parameter ='amp';end
    
    if ~isfield(cfg_ref,'channel') cfg_ref.channel='all';end
    if ~isfield(cfg_ref,'side') cfg_ref.side = 'cEEGridLayout';end
    if ~isfield(cfg_ref,'plotSort') cfg_ref.plotSort='ft_topoplotER';end
    if ~isfield(cfg_ref,'layout')
        layoutFile=load([currentpath filesep 'Layouts' filesep cfg_ref.side]);
        fields=fieldnames(layoutFile);
        cfg_ref.layout= layoutFile.(fields{1});
    end
    if ~isfield(cfg_ref,'newfig') cfg_ref.newfig='yes';end
    if ~isfield(cfg_ref,'interplimits') cfg_ref.interplimits ='head';end
    if ~isfield(cfg_ref,'interpolation') cfg_ref.interpolation='nearest';end
    if ~isfield(cfg_ref,'style') cfg_ref.style = 'straight';end
    if ~isfield(cfg_ref,'shading') cfg_ref.shading='flat';end
    if ~isfield(cfg_ref,'xlim') cfg_ref.xlim='maxmin';end
    if ~isfield(cfg_ref,'zlim') cfg_ref.zlim='maxmin';end
    
    if ~isfield(cfg_ref,'marker') cfg_ref.marker='on';end
    if ~isfield(cfg_ref,'markersymbol') cfg_ref.markersymbol='o';end
    if ~isfield(cfg_ref,'markercolor') cfg_ref.markercolor=[0 0 0];end
    if ~isfield(cfg_ref,'markersize') cfg_ref.markersize=2';end
    if ~isfield(cfg_ref,'markerfontsize') cfg_ref.markerfontsize=8;end
    
    if ~isfield(cfg_ref,'highlight') cfg_ref.highlight='off';end
    if ~isfield(cfg_ref,'highlightchannel') cfg_ref.highlightchannel='all';end
    if ~isfield(cfg_ref,'highlightsymbol') cfg_ref.highlightsymbol='o';end
    if ~isfield(cfg_ref,'highlightcolor') cfg_ref.highlightcolor=[0 0 0];end
    if ~isfield(cfg_ref,'highlightsize') cfg_ref.highlightsize=6;end
    if ~isfield(cfg_ref,'highlightfontsize') cfg_ref.highlightfontsize=8;end
    
    if ~isfield(cfg_ref,'colorbar') cfg_ref.colorbar='no';end
    if ~isfield(cfg_ref,'colorbartext') cfg_ref.colorbartext='';end
    
    if ~isfield(cfg_ref,'comment') cfg_ref.comment='auto';end
    if ~isfield(cfg_ref,'fontsize') cfg_ref.fontsize=8;end
    
     fieldnames_cfg= fieldnames(cfg);
    arg_string='';
    %compare inputs from gui to default values
    for i=1:numel(fieldnames_cfg)
        %check if field is string or number
        if ischar(cfg.(fieldnames_cfg{i}))
            if ~strcmp(cfg.(fieldnames_cfg{i}),cfg_ref.(fieldnames_cfg{i}))
                %['found difference in cfg.' fieldnames_cfg{i}]
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', ''' cfg.(fieldnames_cfg{i}) '''']);
            end
            
        elseif numel(cfg_ref.(fieldnames_cfg{i}))>1
            if numel(cfg.(fieldnames_cfg{i}))== numel(cfg_ref.(fieldnames_cfg{i}))
                if cfg.(fieldnames_cfg{i})~=cfg_ref.(fieldnames_cfg{i})
                    arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', [' num2str(cfg.(fieldnames_cfg{i})) ']' ]);
                end
            else
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', [' num2str(cfg.(fieldnames_cfg{i})) ']' ]);
            end
        elseif cfg.(fieldnames_cfg{i})~=cfg_ref.(fieldnames_cfg{i})
                %['found difference in cfg.' fieldnames_cfg{i}]
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', ' num2str(cfg.(fieldnames_cfg{i})) ]);
            
        end
    end
    plot_call_cfg=',[]';%[',[], timepoints' arg_string]; 
    plot_call_vararg=arg_string;% Plot call containing the parameters that differ from default values