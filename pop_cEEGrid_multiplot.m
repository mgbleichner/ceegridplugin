% pop_cEEGrid_multiplot(EEG,DataLabels,cfg,varargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =cEEGrid_multiplot(EEG,DataLabels,cfg,varargin)
%
% Inputs:
%   EEG         - input EEG dataset
%   DataLabels  - names of the channels that are to be plotted 
%   cfg         - contains all parameters for plotting
%   cfg paratmeters:
% Parameter:         Var-Type   possible values                                         default values           description
% 'showlabels'	 	   'string'	 {'yes','no'}                                           'no';
% 'showscale'          'string'	 {'yes','no'}                                           'yes';
% 'showcomment'	 	   'string'	 {'yes','no'}                                           'yes';
% 'showoutline'	 	   'string'	 {'yes','no'}                                           'yes';
% 'newfig'             'string'	 {'yes','no'}                                           'yes';
% 'axes'               'string'	 {'yes','no'}                                           'yes';
% 'box'                'string'	 {'yes','no'}                                           'no';
% 'interactive'	 	   'string'	 {'yes','no'}                                           'yes';
% 'zoom'               'string'	 {'yes','no'}                                           'no';
% 'comment'            'string'	 []                                                     '';
% 'xlim'      {'string' 'real'}	 'maxmin' or [xmin xmax]                                    'maxmin';
% 'ylim'      {'string' 'real'}	 {'maxmin', 'maxabs', 'zeromax', 'minzero'} or [ymin ymax]    'maxmin';
% 'fontsize'           'real'    []                                                     8;
% 'linewidth'          'real'    []                                                     2;
% 'side'               'string'	 []                                                     'cEEGridLayout';
% 'layout'             'struct'	 []                                                     layout struct that is selected with 'side'
%
% Parameters can be passed to this function via key-value pair. E.g.:
% pop_cEEGrid_multiplot(EEG,[],[], 'showcomment','no','box','yes')
%
% Parameters can also be passed via cfg_filt like:
% cfg.showcomment='no';
% cfg.box='yes';
% 
% pop_cEEGrid_multiplot(EEG,[],cfg)
% parameters passed via cfg_filt are overwritten by the key value pairs (if
% matching parameters are entered as key value pair.)
%
%  If only the INEEG is entered, the function will show a GUI to adjust the
%  input parameters for this function
%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_multiplot(EEG,DataLabels,cfg,varargin)

com = ''; % this initialization ensure that the function will return something

if nargin < 1
    help pop_cEEGrid_multiplot;
    return;
end
%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    
    plugin_askinstall('fieldtrip-lite', [], false);
end



if nargin < 2
    %%
    
    Labels={EEG.chanlocs.labels};
    [plot_call, DataLabels]= cEEGrid_multiplot_GUI(Labels,EEG.nbchan); %% Calls GUI function
    if isempty(plot_call) % handles cancel event
        return
    end
else
    %cfg.side = layoutname;
    %DataLabels={EEG.chanlocs.labels};
    if nargin == 3
        %plot_call=['cEEGrid_filterplot(EEG,' vararg2str(cfg_filt) ')'];
        if isempty(cfg)
            plot_call=',[]';
        else
            plot_call=[',' vararg2str(cfg)];
        end
    else
        %plot_call=['cEEGrid_filterplot(EEG,[],' vararg2str(varargin) ')'];
        if isempty(cfg)
            plot_call=[',[],' vararg2str(varargin)];
        else
        plot_call=[',' vararg2str(cfg) ',' vararg2str(varargin)];
        end
    end
end
%% call plot function
%cEEGrid_multiplot(EEG,DataLabels,cfg,varargin)

eval(['cEEGrid_multiplot(EEG,DataLabels' plot_call ');']);
% dimVec = EEG.times;
% Data = mean(EEG.data,3);
% use either all channels or the ones specified
% if isempty(varargin)
%     ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);
% else
%     ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg,varargin);
% end

% return the string command
% -------------------------
%com = sprintf('pop_cEEGrid_multiplot( %s, %s );', inputname(1), vararg2str(cfg.side));
%com = sprintf(['pop_cEEGrid_multiplot(' inputname(1) ',[' vararg2str(dimVec) '],{' vararg2str(DataLabels) '},' vararg2str(cfg) fastif(strcmp(vararg2str(varargin),''), '' ,',') vararg2str(varargin) ');']);
com = sprintf(['pop_cEEGrid_multiplot(' inputname(1) fastif(isempty(DataLabels),',[]',[',{' vararg2str(DataLabels) '}']) plot_call ');']);
return;