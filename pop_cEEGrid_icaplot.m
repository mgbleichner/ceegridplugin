% pop_cEEGrid_icaplot(INEEG, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =cEEGrid_icaplot(INEEG, DataLabels,cfg);
%
% Inputs:
%   INEEG         - input EEG dataset
%   DataLabels    - cell array of electrode names (e.g. {EEG.chanlocs.labels})
%   cfg           - contains the parameters for the fieldtrip function (see
%                   below)
% The configuration can have the following parameters
%   cfg.parameter          = field that contains the data to be plotted as color, for example 'avg', 'powspctrm' or 'cohspctrm' (default is automatic)
%   cfg.maskparameter      = field in the data to be used for masking of data. It should have alues between 0 and 1, where 0 corresponds to transparent.
%   cfg.xlim               = limit for 1st dimension in data (e.g., time),
%                            can be 'maxmin' or [xmin xmax] (default = 'maxmin')
%                            (actually not variable, since this function
%                            will only use one value in the 1st dimension
%   cfg.zlim               = limits for color dimension, 'maxmin', 'maxabs', 'zeromax', 'minzero', or [zmin zmax] (default = 'maxmin')
%   cfg.channel            = Nx1 cell-array with selection of channels (default = 'all'), see FT_CHANNELSELECTION for details
%   cfg.refchannel         = name of reference channel for visualising connectivity, can be 'gui'
%   cfg.baseline           = 'yes','no' or [time1 time2] (default = 'no'), see FT_TIMELOCKBASELINE or FT_FREQBASELINE
%   cfg.baselinetype       = 'absolute' or 'relative' (default = 'absolute')
%   cfg.trials             = 'all' or a selection given as a 1xN vector (default = 'all')
%   cfg.colormap           = any sized colormap, see COLORMAP
%   cfg.marker             = 'on', 'labels', 'numbers', 'off'
%   cfg.markersymbol       = channel marker symbol (default = 'o')
%   cfg.markercolor        = channel marker color (default = [0 0 0] (black))
%   cfg.markersize         = channel marker size (default = 2)
%   cfg.markerfontsize     = font size of channel labels (default = 8 pt)
%   cfg.highlight          = 'off', 'on', 'labels', 'numbers'
%   cfg.highlightchannel   =  Nx1 cell-array with selection of channels, or vector containing channel indices see FT_CHANNELSELECTION
%   cfg.highlightsymbol    = highlight marker symbol (default = 'o')
%   cfg.highlightcolor     = highlight marker color (default = [0 0 0] (black))
%   cfg.highlightsize      = highlight marker size (default = 6)
%   cfg.highlightfontsize  = highlight marker size (default = 8)
%   cfg.hotkeys            = enables hotkeys (pageup/pagedown/m) for dynamic zoom and translation (ctrl+) of the color limits
%   cfg.colorbar           = 'yes'
%                            'no' (default)
%                            'North'              inside plot box near top
%                            'South'              inside bottom
%                            'East'               inside right
%                            'West'               inside left
%                            'NorthOutside'       outside plot box near top
%                            'SouthOutside'       outside bottom
%                            'EastOutside'        outside right
%                            'WestOutside'        outside left
%   cfg.colorbartext       =  string indicating the text next to colorbar
%   cfg.interplimits       = limits for interpolation (default = 'head')
%                            'electrodes' to furthest electrode
%                            'head' to edge of head
%   cfg.interpolation      = 'linear','cubic','nearest','v4' (default = 'nearest') see GRIDDATA
%   cfg.style              = plot style (default = 'straight')
%                            'straight' colormap only
%                            'contour' contour lines only
%                            'both' (default) both colormap and contour lines
%                            'fill' constant color between lines
%                            'blank' only the head shape
%   cfg.gridscale          = scaling grid size (default = 67)
%                            determines resolution of figure
%   cfg.shading            = 'flat' or 'interp' (default = 'flat')
%   cfg.comment            = 'no', 'auto' or 'xlim' (default = 'auto')
%                            'auto': date, xparam and zparam limits are printed
%                            'xlim': only xparam limits are printed
%   cfg.commentpos         = string or two numbers, position of the comment (default = 'leftbottom')
%                            'lefttop' 'leftbottom' 'middletop' 'middlebottom' 'righttop' 'rightbottom'
%                            'title' to place comment as title
%                            'layout' to place comment as specified for COMNT in layout
%                            [x y] coordinates
%   cfg.interactive        = Interactive plot 'yes' or 'no' (default = 'yes')
%                            In a interactive plot you can select areas and produce a new
%                            interactive plot when a selected area is clicked. Multiple areas
%                            can be selected by holding down the SHIFT key.
%   cfg.directionality     = '', 'inflow' or 'outflow' specifies for
%                            connectivity measures whether the inflow into a
%                            node, or the outflow from a node is plotted. The
%                            (default) behavior of this option depends on the dimor
%                            of the input data (see below).
%   cfg.interpolatenan     = string 'yes', 'no' (default = 'yes')
%                            interpolate over channels containing NaNs
%   cfg.side               = string containing the name of the layout(e.g.
%                            'cEEGridLayout')
%   cfg.component          = Vector containing indizes of the components
%                            that are to be plotted
%   cfg.timecourses        = plot time courses above topographies, 'yes' or 'no'
%   cfg.sepplots           = Create seperate plots 'yes' or 'no'
%   cfg.subcount           = Number of subplots per figure
%   cfg.autogrid           = Calculate subplot grid dimensions close to a square number, 'yes' or 'no'
%   cfg.column_count       = Amount of subplot columns
%   cfg.showtitle          = Show component number in title, 'yes' or 'no'
%
% Parameters that are editable from the GUI can also be entered via console
% as key-value pairs. These parameters are:
% Parameter:         Var-Type   possible values     default values           description
%      'side'           'string'	 []             'cEEGridLayout'
%      'layout'         'struct'	 []             layout struct that is selected with 'side'
%      'component'	 	'real'       []             [1:18]
%      'timecourses'	'string'	 []             'no'
%      'timepoints'     'real'       []             [min(EEG.times) max(EEG.times)]
%      'sepplots'	 	'string'	 []             'no'
%      'subcount'	 	'real'       []             1
%      'autogrid'	 	'string'	 []             'yes'
%      'column_count'	'real'       []             3
%      'showtitle'	 	'string'	 []             'yes'
%      'channel'	 	'string'	 []             'all'
%      'interplimits'	'string'	 []             'head'
%      'interpolation'	'string'	 []             'nearest'
%      'plotSort'	 	'string'	 []             'ft_topoplotER'
%      'newfig'         'string'	 []             'no'
%      'style'          'string'	 []             'straight'
%      'fontsize'	 	'real'       []             8

%
% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_cEEGrid_icaplot(EEG, DataLabels,cfg,varargin)

com = ''; % this initialization ensure that the function will return something

if nargin < 1
    help pop_cEEGrid_icaplot;
    return;
end;
%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    plugin_askinstall('fieldtrip-lite', [], false);
end
%% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=[''];
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,'|');
    end
end
%% GUI
if nargin < 2
    
    guititle='cEEGrid ICA plot options';
    geom={[3 1] [3 1] [1] [1 1] [3 1] [3 3 1] [3 3 1]  [2 1] [1 2] [2 1] [2 1]};
    uilist={{ 'style', 'text', 'string', 'Components (Vector containing component numbers, []=all)', 'fontweight', 'bold' }...
        { 'style', 'edit', 'string', '' 'tag' 'component'} ...
        { 'style', 'text', 'string', 'Select Grid:', 'fontweight', 'bold' }...
        { 'style', 'popupmenu', 'string', layoutoptions 'tag' 'side'}...
        {}...
        {'Style', 'checkbox', 'string' 'Plot time courses' 'value' 0 'tag' 'timecourses'}...
        {'Style', 'checkbox', 'string' 'Show component number in title' 'value' 1 'tag' 'showtitle'}...
        { 'style', 'text', 'string', ['Time range for time courses (two values between ' num2str(min(EEG.times)) ' and ' num2str(max(EEG.times)) ' ms)' ] },...
        { 'style', 'edit', 'string', '' 'tag' 'timepoints'},...
        { 'Style', 'checkbox', 'string' 'Seperated plots' 'value' 0 'tag' 'sepplots' }...
        { 'style', 'text', 'string', 'Number of subplots per plot:'}...
        {'Style', 'edit', 'string', '3' 'tag' 'subcount'}...
        { 'Style', 'checkbox', 'string' 'Automatic row/columns' 'value' 1 'tag' 'autogrid' }...
        { 'style', 'text', 'string', 'Number of plots per row:'}...
        {'Style', 'edit', 'string', '3' 'tag' 'column_count'}...
        {'style'   'text'     'string'    'Colorbar: ', 'fontweight', 'bold'}...
        {'Style', 'popupmenu', 'string', 'yes|no|North|South|East|West|NorthOutside|SouthOutside|EastOutside|WestOutside' 'value' 2 'tag' 'colorbar'}...
        {'style'   'text'     'string'    'Colorbar text: '}...
        {'Style', 'edit', 'string', '' 'tag' 'colorbartext'}...
        {'style'   'text'     'string'    'Comment (''no'', ''auto'' or ''xlim'' (default = ''auto'')): ', 'fontweight', 'bold'}...
        {'Style', 'popupmenu', 'string', 'no|auto|xlim' 'value' 2 'tag' 'comment'}...
        {'Style'   'text'     'string' 'Font size of comment and labels (if present) (default = 8): '}...
        {'Style', 'edit', 'string', '8' 'tag' 'fontsize'}...
        };
    
    %result=inputgui('geometry', geom , 'uilist', uilist);
    [result userdat strhalt outstruct]=inputgui( geom ,  uilist, 'pophelp('' pop_cEEGrid_icaplot'')', guititle, [], 'normal');
    % catch cancelling-event
    if isempty(result), return; end
    
    %% Get GUI inputs
    cfg.side = layouts(outstruct.side).name(1:end-4); % get the layout file
    if ~strcmp(outstruct.component,'') %gets component indices from input
        cfg.component=str2num(outstruct.component);
    else
        cfg.component=1:EEG.nbchan;
    end
    yes_no = { 'no','yes'};
    %marker_opt = {'on','labels','numbers','off'};
    %markersymbol_opt = {'o','+','*','.','x','s','d','p','h','^','v','>','<'};
    colorbar_opt = {'yes','no','North','South','East','West','NorthOutside','SouthOutside','EastOutside','WestOutside'};
    comment_opt={'no','auto','xlim'};
    
    %plot behaviour
    cfg.timecourses=yes_no{outstruct.timecourses+1};
    if strcmp(outstruct.timepoints,'')
        cfg.timepoints=[min(EEG.times) max(EEG.times)];
    else
        cfg.timepoints=str2num(outstruct.timepoints);
    end
    cfg.sepplots=yes_no{outstruct.sepplots+1};
    cfg.subcount=str2num(outstruct.subcount);
    cfg.autogrid =yes_no{outstruct.autogrid+1};
    cfg.column_count=str2num(outstruct.column_count);
    cfg.showtitle = yes_no{outstruct.showtitle+1};
    %colorbar
    cfg.colorbar = colorbar_opt{outstruct.colorbar};
    cfg.colorbartext = outstruct.colorbartext;
    %comment
    cfg.comment=comment_opt{outstruct.comment};
    cfg.fontsize = str2num(outstruct.fontsize);
    
    %DataLabels={EEG.chanlocs.labels}; % all channels by default (should be
    %defined in cEEGrid_icaplot.m)
    DataLabels=[];
    
    %% Prepare function call
    % default reference values (taken from cEEGrid_icaplot.m)
    cfg_ref=[];
    if ~isfield(cfg_ref,'side') cfg_ref.side = 'cEEGridLayout';end
    if ~isfield(cfg_ref,'layout')
        layoutFile=load([currentpath filesep 'Layouts' filesep cfg_ref.side]);
        fields=fieldnames(layoutFile);
        cfg_ref.layout= layoutFile.(fields{1});
    end
    if ~isfield(cfg_ref,'colorbar') cfg_ref.colorbar='no';end
    if ~isfield(cfg_ref,'colorbartext')  cfg_ref.colorbartext=''; end
    if ~isfield(cfg_ref, 'comment') cfg_ref.comment='auto'; end
    if ~isfield(cfg_ref,'component') cfg_ref.component=1:EEG.nbchan; end
    if ~isfield(cfg_ref,'timecourses') cfg_ref.timecourses='no'; end
    if ~isfield(cfg_ref,'timepoints') cfg_ref.timepoints=[min(EEG.times) max(EEG.times)]; end
    if ~isfield(cfg_ref,'sepplots') cfg_ref.sepplots='no'; end
    if ~isfield(cfg_ref,'subcount') cfg_ref.subcount=3; end
    if ~isfield(cfg_ref,'autogrid') cfg_ref.autogrid='yes'; end
    if ~isfield(cfg_ref,'column_count') cfg_ref.column_count=3; end
    if ~isfield(cfg_ref,'showtitle') cfg_ref.showtitle='yes'; end
    if ~isfield(cfg_ref,'channel') cfg_ref.channel='all'; end
    if ~isfield(cfg_ref,'interplimits') cfg_ref.interplimits='head'; end
    if ~isfield(cfg_ref,'interpolation') cfg_ref.interpolation='nearest'; end
    if ~isfield(cfg_ref,'plotSort') cfg_ref.plotSort='ft_topoplotER'; end
    if ~isfield(cfg_ref,'newfig') cfg_ref.newfig='no'; end
    if ~isfield(cfg_ref,'style') cfg_ref.style= 'straight'; end
    if ~isfield(cfg_ref,'fontsize') cfg_ref.fontsize=8;end
    fieldnames_cfg= fieldnames(cfg);
    arg_string='';
    %compare inputs from gui to default values
    for i=1:numel(fieldnames_cfg)
        %check if field is string or number
        if ischar(cfg.(fieldnames_cfg{i}))
            if ~strcmp(cfg.(fieldnames_cfg{i}),cfg_ref.(fieldnames_cfg{i}))
                %['found difference in cfg.' fieldnames_cfg{i}]
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', ''' cfg.(fieldnames_cfg{i}) '''']);
            end
            
        elseif numel(cfg_ref.(fieldnames_cfg{i}))>1
            if numel(cfg.(fieldnames_cfg{i}))== numel(cfg_ref.(fieldnames_cfg{i}))
                if cfg.(fieldnames_cfg{i})~=cfg_ref.(fieldnames_cfg{i})
                    arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', [' num2str(cfg.(fieldnames_cfg{i})) ']' ]);
                end
            else
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', [' num2str(cfg.(fieldnames_cfg{i})) ']' ]);
            end
        elseif cfg.(fieldnames_cfg{i})~=cfg_ref.(fieldnames_cfg{i})
            %['found difference in cfg.' fieldnames_cfg{i}]
            arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', ' num2str(cfg.(fieldnames_cfg{i})) ]);
            
        end
    end
    plot_call=[',[]' arg_string]; % Plot call containing the parameters that differ from default values
else
    %% No GUI
%     if isempty(DataLabels)
%         DataLabels={EEG.chanlocs.labels};% all channels by default
%     end
    if isempty(varargin)
        %plot_call=['cEEGrid_filterplot(EEG,' vararg2str(cfg_filt) ')'];
        if isempty(cfg)
            plot_call=',[]';
        else
            plot_call=[',' vararg2str(cfg)];
        end
    else
        %plot_call=['cEEGrid_filterplot(EEG,[],' vararg2str(varargin) ')'];
        if isempty(cfg)
            plot_call=[',[],' vararg2str(varargin)];
        else
            plot_call=[',' vararg2str(cfg) ',' vararg2str(varargin)];
        end
    end

end
%% call plot function
eval(['cEEGrid_icaplot(EEG,DataLabels' plot_call ');']);
%[EEG, com] = cEEGrid_icaplot(EEG, DataLabels,cfg); %call plot function

%% return the string command
% -------------------------
% com = sprintf(['pop_cEEGrid_icaplot(' inputname(1) ',{' vararg2str(DataLabels) '},' vararg2str(cfg) ');']);
com = sprintf(['pop_cEEGrid_icaplot(' inputname(1) fastif(isempty(DataLabels),',[]',[',{' vararg2str(DataLabels) '}']) plot_call ');']);

end