% pop_gedfun() - Compute spatial filters maximizing spatial difference
%                   between conditions or temporal windows
% Usage for GUI:
%   >> pop_gedfun( EEG, cls, 'gui',1); % pops-up interactive window
%       > necessary entries
%           EEG or ALLEEG     - needs to be in the eeglab data sturcture form
%           mrk               - does not support multiple event markers to be epoched
%                             over
%
% Usage for GED over a single condition, for different baselines
%   >> [eigvecs,eigvals, S,R] = pop_gedfun( EEG, cls 'key', 'val',...);
%       > necessary entries
%         'win'   = 1
%         's_win' = [t1 t2] in seconds
%         'r_win' = [t3 t4] in seconds
%         'epo'   = [-t5 t6] in seconds
%
% Usage for GED if spatial filters are to be computed over different conditions                                     
%   >> [eigvecs,eigvals, S,R] = pop_gedfun( ALLEEG, cls 'key', 'val',...);
%       > necessary entries if 'win' = 1
%         'epo' = time duration of existing epoch structure
%           only 's_win' -> same window for both conditions
%           's_win' and 'r_win' -> s_win for first data set, r_win for second
%           data set
%
%
%returns pop_gedfun() outputs
%
% Inputs:
%   Data_Struct        - EEG data (GED for 1 condition, i.e. basline vs. peak)  
%                      or ALLEEG{1,2} (GED for 2 conditions i.e. narrowband vs. broadband) containing 2 EEG structures.
%                      Data always in the same form chan x sample or, chan x
%                      sample x trial.
%
% Optional inputs:
%   gui                - 1 to open gui (default = 0)
%   filt               - [hp lp], enables narrow vs. broadband spatial
%                      computation. Works for 1 or 2 data sets (in
%                      case of 2, the ALLEEG{1,1} will be filtered (default
%                      = []);
%   mrk                - cell, containing event.type over which data (if
%                      2D) is epoched (default = []). Can contain multiple
%                      event.types if 2 datasets are to be compared or if 
%                      multiple events for a single data set are to be computed
%                      i.e. {'left','right'}                        
%   win                - 1, if GED is to be computed over specified
%                      windows (s_win, r_win) (default 0). If specified,
%                      epoch information have to be entered.
%   epo                - in seconds, if data is matrix i.e. chan x sample, enter
%                      desired epoch interval (default []). Epoched around 
%                      chosen 'mrk'. If data is already in tensor form i.e.
%                      chan x sample x trial specifiy your sample duration in seconds i.e. [-0.1 1]
%   s_win              - in seconds, if win is selected, specify target
%                      window duration here (default = [0 0.5])
%   r_win              - in seconds, if win is selected, specify reference
%                      window duration here (default = [-0.1 -0.01])
%   norm               - 1, determine if the trial covariance matrices are
%                      to be normalized (default = 1)
%   reg                - 0, determine if the matrices should be regularized
%                      (default = 0) (shrinkage operation)
%   lambda             - specifies the lambda parameter for regularization
%                      (default = 0.01)
%   clean              - 1, determine if outlier z-scored - trail covariance matrices
%                      should be removed (default = 1)
%   std_param          - scalar, determine threshold for z-scored trial
%                      covariance removal (default = 3)
%   cmp                - integer, computes the component and their corresponding
%                      topography (default = [1]. 
%   cEEGrid            - enables the option to plot the toporgaphy of the
%                      cEEGGrids. Does not impact GED computation (default = 0)
%   plt                - 1, in conjunction with cmp, plots the results
%                      (default = 0)
%
%
%
% Outputs: 
%   eigvecs            - matrix of column eigenvectors (sorted)
%   eigvals            - matrix of eigenvalues (sorted)
%   S                  - target covariance matrix
%   R                  - reference covariance matrix
%   compts             - components' time series
%   compmaps           - topography of components
%
% example1 (baseline GED for condition FR_pre): 
%        [eigvecs,eigvals,S,R] =  pop_gedfun(EEG,'mrk',{'FR_pre'},'win',1,'epo',[-1 2]);
% example 2 (motor imagery GED for spatial filter max left, min right):
%        [eigvecs,eigvals,S,R] =  pop_gedfun(ALLEEG,'mrk',{'left','right'},'win',1,'epo',[-1 5],'s_win',[0.25 3.5]);
%
%
% Author: Thorge Haupt 2022
% This file contains two functions from the BBCI toolbox: 
% for futher citation please see: 
%Blankertz B, Acqualagna L, Dähne S, Haufe S, Schultze-Kraft M, Sturm I, Ušćumlic M, Wenzel MA, Curio G, Müller KR. 
%The Berlin Brain-Computer Interface: Progress Beyond Communication and Control.
%Frontiers in Neuroscience. 2016;10. http://journal.frontiersin.org/article/10.3389/fnins.2016.00530


function [eigvecs,eigvals,S,R, compts, compmap] = pop_cEEGrid_gedfun(Data_Struct,varargin)

if nargin < 1
    help gedfun
    return
end

%check if the input data structures are correct
cls = size(Data_Struct,2);
if cls ~= 1 && cls ~= 2
    error('enter valid number of data sets')
end

%get the options
opt = property2list(varargin{:});

%% Start the ultimate GUI experience
if opt.gui
    %necessary for the event type options to show up in the selection menu
    if size(Data_Struct,2) == 2
        EEG = Data_Struct{1,1};
    else
        EEG = Data_Struct;
    end
    
    %determine the first entry: size of the box and type of interactive
    %item
    g = [1 0.3];
    geometry = {[g]};
    uilist = {  { 'Style', 'text', 'string', 'Epoch [min max] (sec)', 'fontweight', 'bold' } ...
        { 'Style', 'edit', 'string', ' ' 'tag' 'epo' } };
    %this option is only present if also events are present
    if ~isempty(EEG.event)
        % add functonality that unique event markers are displayed that can be
        % selected (copied this straight out of a pop_selectevent script)
        allfields = fieldnames(EEG.event);
        indexmatch = strmatch('urevent', allfields);
        if ~isempty(indexmatch)
            allfields = { allfields{1:indexmatch-1} allfields{indexmatch+1:end} };
        end
        % add all fields to graphic interface
        % -----------------------------------
        ind1 = strmatch('type', allfields, 'exact');
        ind2 = strmatch('latency' , allfields, 'exact');
        ind3 = strmatch('duration', allfields, 'exact');
        neworder = [ ind2 ind3 ind1 setdiff(1:length(allfields), [ind1 ind2 ind3]) ];
        allfields = { allfields{neworder} };
        
        for index = 3%1:length(allfields)
            % format the description to fit a help box
            % ----------------------------------------
            if index <= length( EEG.eventdescription )
                tmptext = EEG.eventdescription{ neworder(index) };
                if ~isempty(tmptext)
                    if size(tmptext,1) > 15,    stringtext = [ tmptext(1,1:15) '...' ];
                    else                        stringtext = tmptext(1,:);
                    end
                else stringtext = 'no description'; tmptext = 'no description (use menu Edit > Event Field)';
                end
            else stringtext = 'no description'; tmptext = 'no description (use menu Edit > Event Field)';
            end
            
            descrip = { 'string', stringtext, 'callback', ['questdlg2(' vararg2str(tmptext) ...
                ',''Description of field ''''' allfields{index} ''''''', ''OK'', ''OK'');' ] };
            
            % create the gui for this field
            % -----------------------------
            textfield = allfields{index};
            if strcmp(textfield, 'latency') || strcmp(textfield, 'duration')
                if EEG.trials > 1, textfield = [ textfield ' (ms)' ];
                else textfield = [ textfield ' (s)' ];
                end
                middletxt  = { { 'Style', 'text', 'string', 'min' } { 'Style', 'edit', 'string', '' 'tag' [ 'min' allfields{index} ] } ...
                    { 'Style', 'text', 'string', 'max' } { 'Style', 'edit', 'string', '' 'tag' [ 'max' allfields{index} ] } };
                middlegeom = [ 0.3 0.35 0.3 0.35 ];
            elseif strcmp(textfield, 'type')
                commandtype = [ 'tmpEEG = get(gcbf, ''userdata'');' ...
                    'if ~isfield(tmpEEG.event, ''type'')' ...
                    '   errordlg2(''No type field'');' ...
                    'else' ...
                    '   tmpevent = tmpEEG.event;' ...
                    '   if isnumeric(tmpEEG.event(1).type),' ...
                    '        [tmps,tmpstr] = pop_chansel(unique([ tmpevent.type ]));' ...
                    '   else,' ...
                    '        [tmps,tmpstr] = pop_chansel(unique({ tmpevent.type }));' ...
                    '   end;' ...
                    '   if ~isempty(tmps)' ...
                    '       set(findobj(''parent'', gcbf, ''tag'', ''type''), ''string'', tmpstr);' ...
                    '   end;' ...
                    'end;' ...
                    'clear tmps tmpv tmpEEG tmpevent tmpstr tmpfieldnames;' ];
                middletxt  = { { 'Style', 'edit', 'string', '' 'tag' 'type' } { 'Style', 'pushbutton', 'string', '...' 'callback' commandtype } };
                middlegeom = [ 0.95 0.35 ];
            else
                middletxt  = { { 'Style', 'edit', 'string', '' 'tag' textfield } };
                middlegeom = 1.3;
            end
            geometry = { geometry{:} [0.55 middlegeom] };
            uilist   = { uilist{:} ...
                { 'Style', 'text', 'string', textfield, 'fontweight', 'bold' }, ...
                middletxt{:} };
        end
    end
    %add all the remaining interactive items
    geometry = {geometry{:} [1] [g] [g] [1] [[g 0.3]] [[g 0.3]] [[g 0.3]] [g] [g 0.3] };
    %if i write fucking comments into the uilist this entire piece of shit
    %stops working
    uilist = { uilist{:} ...
        { 'Style', 'checkbox', 'string', 'Window', 'fontweight', 'bold', 'value' 0 'tag' 'win' } ...
        ...
        { 'Style', 'text', 'string', 'Target Window duration [min max] (sec)', 'fontweight', 'bold' } ...
        { 'Style', 'edit', 'string', ' ' 'tag' 's_win' } ...
        ...
        { 'Style', 'text', 'string', 'Reference Window duration [min max] (sec)', 'fontweight', 'bold' } ...
        { 'Style', 'edit', 'string', ' ' 'tag' 'r_win' } ...
        ...
        { 'Style', 'checkbox', 'string', 'Normalization of trial covariance matrices', 'value' 0 'tag' 'norm'} ...
        ...
        { 'Style', 'checkbox', 'string' 'Regularization' 'value' 0 'tag' 'reg' } ...
        { 'Style', 'text', 'string', 'lambda'} ...
        { 'Style', 'edit', 'string', '0.01' 'tag' 'lambda'} ...
        ...
        { 'Style', 'checkbox', 'string' 'Clean' 'value' 0 'tag' 'clean'} ...
        { 'Style', 'text', 'string', 'Threshold' } ...
        { 'Style', 'edit', 'string', '3' 'tag' 'std_param' } ...
        ...
        { 'Style', 'text', 'string', 'Number of Components (sorted)' } ...
        { 'Style', 'edit', 'string', '1' 'tag' 'cmp_nr' } ...
        { 'Style', 'checkbox', 'string' 'Plot' 'value' 0 'tag' 'plt'} ...
        ...
        { 'Style', 'text', 'string', 'Filter [lower upper] (Hz)', 'fontweight', 'bold' } ...
        { 'Style', 'edit', 'string', ' ' 'tag' 'filt' } ...
        ...
        { 'Style', 'checkbox', 'string', 'cEEGrids', 'value' 0 'tag' 'cgrid'} ...
        { 'Style', 'text', 'string', 'Win (ms)'} ...
        { 'Style', 'edit', 'string', ' ' 'tag' 'cwin' } };
    
    %generate the GUI
    [outparam, userdat, strhalt, outstruct, instruct] = inputgui( 'geometry', geometry, 'uilist', uilist, ...
        'helpcom','pophelp(''pop_cEEGrid_gedfun'');', 'title','Derive Spatial filters -- pop_cEEGrid_gedfun()','userdata',EEG); %change to data strct%
    %decode input into opt structure
    opt.epo = str2num(getfield(outstruct,'epo'));
    if isfield(outstruct,'type'); opt.mrk = {getfield(outstruct,'type')}; else opt.mrk = []; end
    opt.win = getfield(outstruct,'win');
    opt.s_win = str2num(getfield(outstruct,'s_win'));
    opt.r_win = str2num(getfield(outstruct,'r_win'));
    opt.norm = getfield(outstruct,'norm');
    opt.reg = getfield(outstruct,'reg');
    opt.lambda = str2num(getfield(outstruct,'lambda'));
    opt.clean = getfield(outstruct,'clean');
    opt.std_param = str2num(getfield(outstruct,'std_param'));
    %opt.cmp = str2num(getfield(outstruct,'cmp'));
    opt.cmp_nr = str2num(getfield(outstruct,'cmp_nr'));
    opt.plt = getfield(outstruct,'plt');
    opt.filt = str2num(getfield(outstruct,'filt'));
    opt.cgrid = getfield(outstruct,'cgrid');
    opt.cwin = str2num(getfield(outstruct,'cwin'));

end

%sets the defaults of the option structure
opt = set_defaults(opt,...
    'gui',0,...
    'mrk',[],...
    'win',0,...
    's_win',[0 0.5],...
    'r_win',[-0.1 -0.01],...
    'epo',[],...
    'norm',0,...
    'reg',0,...
    'clean',0,...
    'std_param', 3,...
    'lambda', 0.01, ...
    'cmp', 0,...
    'cmp_nr',[],...
    'plt',0, ...
    'filt',[]);

%add the filter option 
if ~isempty(opt.filt)
    if cls == 1 %makes sure that also with a single data set, without windowing, a GED can be computed
        cls2 = Data_Struct;
        cls1 = pop_firws(Data_Struct, 'fcutoff', opt.filt(1,2), 'ftype', 'lowpass', 'wtype', 'hann' , 'forder',100);
        cls1 = pop_firws(cls1, 'fcutoff', opt.filt(1,1), 'ftype', 'highpass', 'wtype', 'hann' , 'forder',100);
        cls = 2;
    else
        cls2 = Data_Struct{1,2};
        cls1 = pop_firws(Data_Struct{1,1}, 'fcutoff', opt.filt(1,2), 'ftype', 'lowpass', 'wtype', 'hann' , 'forder',100);
        cls1 = pop_firws(cls1, 'fcutoff', opt.filt(1,1), 'ftype', 'highpass', 'wtype', 'hann' , 'forder',100);
    end
end
%case single data set, but two different epoch markers
if cls == 1 && size(opt.mrk,2)
    cls1 = pop_epoch(Data_struct,opt.mrk{1,1},opt.epo);
    cls2 = pop_epoch(Data_struct,opt.mrk{2,1},opt.epo);
    cls = 2;
elseif cls == 1 && size(opt.mrk,2) && ~isempty(opt.filt)
    error('How about you do some of the pre-processing yourself')
end
    

%computation if 2 Datasets are present
if cls == 2
    %checks if data structure is empty or if the input is even a structure
    if isempty(Data_Struct) || ~isstruct(cls1)
        error('please enter a data Structure, or there is nothing that can be computed')
    end
    %checks if the data is correctly ordered or if there is event a second
    %entry (kind of obsolete, as it is already tested beforehand. Otherwise
    %we wouldnt be in this loop)
    if isempty(opt.filt) && size(Data_Struct,2) < 2
        error('could not find two separate data sets')
        
    elseif opt.win && isempty(opt.epo) %necessary conditions for later determination of the time vector
        error('please specify the interval around which the data has to be epoched, or is already epoched')
    end

    %necessary condition for epochs to be computed
    if ismatrix(cls1.data) && isempty(opt.mrk)
        error('enter an evnet value')
    elseif ismatrix(cls1.data) && ~isempty(opt.mrk) %checks if input data is matrix and if markers for epoching are given
        
        %Checks if the event markers are existent
        if sum(strncmp(string(opt.mrk{1,1}),{cls1.event.type},strlength(opt.mrk{1,1}))) > 0 && sum(strncmp(string(opt.mrk{1,2}),{cls2.event.type},strlength(opt.mrk{1,2}))) > 0
            %checks if only 1 event was given
            if size(opt.mrk,2) == 1
                cls1_epo = pop_epoch(cls1,opt.mrk,opt.epo);
                cls2_epo = pop_epoch(cls2,opt.mrk,opt.epo);
            else
                cls1_epo = pop_epoch(cls1,opt.mrk(1,1),opt.epo);
                cls2_epo = pop_epoch(cls2,opt.mrk(1,2),opt.epo);
            end
            %determine the time vector of the epochs -> needed for
            %potential windowing later
            time_vec = round(linspace(opt.epo(1)*cls1_epo.srate,opt.epo(2)*cls1_epo.srate,size(cls1_epo.data,2)));
        else
            error('the marker that you entered is non-existent')
        end
    else %if data is already in tensor form
        cls1_epo = cls1;
        cls2_epo = cls2;
    end
    
    %if windowing is desired
    if opt.win
        s_wins = opt.s_win*cls1_epo.srate;  
        for w = 1:length(s_wins)
            [~, s_win(1,w)] = min(abs(time_vec - s_wins(1,w)));
        end
        
        if ~isempty(opt.r_win)
            r_wins = opt.r_win*cls1_epo.srate;
            for w = 1:length(r_wins)
                [~, r_win(1,w)] = min(abs(time_vec - r_wins(1,w)));
            end
        end
    else %whole epoch is used
        s_win = [1 size(cls1_epo.data,2)];
        
         if ~isempty(opt.r_win)
            r_win = [1 size(cls2_epo.data,2)];
         end
    end
    
    %set up trial covariance matrix containers, is sensitive to different
    %trial structures in the data
    S_a = zeros(cls1_epo.nbchan,cls1_epo.nbchan,size(cls1_epo.data,3));
    R_a = zeros(cls1_epo.nbchan,cls1_epo.nbchan,size(cls2_epo.data,3));
    %start the covariance computation
    for t=1:size(cls1_epo.data,3)
        %compute the S covariance matrix
        seg = cls1_epo.data(:,s_win(1):s_win(2),t);
        seg = seg - mean(seg,2); %mean center 
        if opt.norm %normalization 
            S_a(:,:,t) = seg*seg'/(size(seg,2)-1);
        else
            S_a(:,:,t) = seg*seg';
        end
    end
    S = mean(S_a,3);
    for t = 1:size(cls2_epo.data,3)
        %compute the R covariance matrix
        if ~isempty(opt.r_win)
            segfa = cls2_epo.data(:,r_win(1):r_win(2),t);
        else
            segfa = cls2_epo.data(:,s_win(1):s_win(2),t);
        end
        segfa = segfa - mean(segfa,2);
        if opt.norm
            R_a(:,:,t) = segfa*segfa'/(size(segfa,2)-1);
        else
            R_a(:,:,t) = segfa*segfa';
        end
    end
    R = mean(R_a,3);
    
% other option -> 1 data set    
elseif cls == 1
    
    %check if input contains content and is of type struct
    if isempty(Data_Struct) || ~isstruct(Data_Struct)
        error('please enter a data Structure, or there is nothing that can be computed')
    elseif ~opt.win %single data set requires windowing 
        error('no window, not different covariance matrices')
    elseif size(Data_Struct,2) > 1 %also kind of obsolete
        error('you have entered two datasets, but set cls to 1')
    elseif isempty(opt.epo) %necessary condition for 2D data, trial covariance computation sadly requires trials,
                            %and tensor data requires information about the
                            %length of the epochs
        error('please specify the interval around which the data has to be epoched, or an existing interval if your data is already in tensor form')
    end
    
    %get the data
    data = Data_Struct.data;
    %start epoching flow
    if ismatrix(data)
        if ~iscell(opt.mrk) || isempty(opt.mrk)
            error('either there is no marker specified, or the marker is not of type cell')
        else %epoch the data
                        
            Data_Struct = pop_epoch(Data_Struct,opt.mrk,opt.epo);
            data = Data_Struct.data;
        end
        %need this for the windowing later
        time_vec = linspace(opt.epo(1)*Data_Struct.srate,opt.epo(2)*Data_Struct.srate,size(data,2));
    % checks if data is already in tensor form and in the correct order 
    elseif ndims(data) == 3
        if size(data,1) ~= Data_Struct.nbchan
            data = permute(data,[2 1 3]);
            if size(data,1)~= Data_Struct.nbchan
                error(' please enter your data correct you instruction-inconsiderate person ')
            end
        end
        %still very important for windowing
        time_vec = linspace(opt.epo(1)*Data_Struct.srate,opt.epo(2)*Data_Struct.srate,size(data,2));
        
        %target window is too larger
        if opt.s_win(2)*Data_Struct.srate > time_vec(end)
            error('please reconsider using a suitable target window')
        end
    else
        error('please leave your 1 dimensional space')
    end
    
    %last adjustment of the data -> get it into the correct order
    [d1,d2,d3] = size(data);
    if d1 ~= Data_Struct.nbchan
        data = permute(data,[2 1 3]);
        if size(data,1)~= Data_Struct.nbchan
            help gedfun
        end
    end
    
    
    %get the windows over which the weights have to be computed
    s_wins = opt.s_win*Data_Struct.srate;
    r_wins = opt.r_win*Data_Struct.srate;
    for w = 1:length(s_wins)
        [~, s_win(1,w)] = min(abs(time_vec - s_wins(1,w)));
        [~, r_win(1,w)] = min(abs(time_vec - r_wins(1,w)));
    end
    
    
    %start the trial covariance matrix computation
    S_a = zeros(Data_Struct.nbchan,Data_Struct.nbchan,size(data,3));
    R_a = zeros(Data_Struct.nbchan,Data_Struct.nbchan,size(data,3));
    for t=1:size(data,3)
        %compute the S covariance matrix
        seg = data(:,s_win(1):s_win(2),t);
        seg = seg - mean(seg,2);
        
        %compute the R covariance matrix
        segfa = data(:,r_win(1):r_win(2),t);
        segfa = segfa - mean(segfa,2);
        if opt.norm
            S_a(:,:,t) = (seg*seg')/(size(seg,2)-1);
            R_a(:,:,t) = (segfa*segfa')/(size(segfa,2)-1);
        else
            S_a(:,:,t) = seg*seg';
            R_a(:,:,t) =segfa*segfa';
        end
    end
    S = mean(S_a,3);
    R = mean(R_a,3);
end




if opt.clean
    % clean R
    dist_R = zeros(size(S_a,3),1);     % vector of distances to mean
    for t = 1:size(R_a,3)
        r =  R_a(:,:,t) ;
        % Euclidean distance
        dist_R(t) = sqrt( sum((r(:)-R(:)).^2) );
    end
    
    % finally, average trial-covariances together, excluding outliers
    R = squeeze(mean( R_a(:,:,zscore(dist_R)<opt.std_param) ,3));
    
    
    % clean S
    dist_S = zeros(size(S_a,3),1);     % vector of distances to mean
    for t = 1:size(S_a,3)
        s =  S_a(:,:,t) ;
        % Euclidean distance
        dist_S(t) = sqrt( sum((s(:)-S(:)).^2) );
    end
    
    % finally, average trial-covariances together, excluding outliers
    S = squeeze(mean( S_a(:,:,zscore(dist_S)<opt.std_param) ,3));
    
end

%optional regularization 
if opt.reg
    R = R * (1-opt.lambda) + opt.lambda*eye(size(R));
    S = S * (1-opt.lambda) + opt.lambda*eye(size(S));
end
%can you believe it, this three lines are the GED (only the first actually
%...) the lengths we go at to make people's life easy
[W,L] = eig(S,R);
[eigvals,sidx]=sort(diag(L),'descend');
eigvecs = W(:,sidx);

% add component map, component time series and plotting options 
if isempty(opt.cmp_nr) && opt.plt
    error('there can be no plotting if you did not select any components')
elseif ~isempty(opt.cmp_nr)
    compts = [];
    compmap = [];
    for cm = 1:length(opt.cmp_nr)
        
        %transform the epoched data into a matrix
        if cls == 1
            data_mtx = reshape(data,Data_Struct.nbchan,[]);
        elseif cls == 2
            if opt.filt
                %broadband data
                data_mtx = reshape(cls2_epo.data,cls2_epo.nbchan,[]);
            else
                %normal data with the desired class
                data_mtx = reshape(cls1_epo.data,cls1_epo.nbchan,[]);
            end
            Data_Struct = cls1_epo;
        else
            error('some grave mistake has happened and it wasnt your birth')
        end
        compt = eigvecs(:,opt.cmp_nr(cm))' * data_mtx;
        % and then reshaped back into trials
        compts(opt.cmp_nr(cm),:,:) = reshape(compt,[],size(Data_Struct.data,3));
        
        
        
        map = eigvecs(:,opt.cmp_nr(cm))' * S;
        %flip the sign of the compmap according to the max value
        [~,se] = max(abs( map ));
        compmap(:,opt.cmp_nr(cm)) = map * sign(map(se));
    end
    
    %update the Datastructures data
    Data_Struct.data = compts;
    
end
%plot in the case of normal data
if opt.plt && isempty(opt.filt)
    for cm = 1:length(opt.cmp_nr)
        figure(cm),clf
        
        %check if input is cEEGrid data
        if opt.cgrid
            subplot(2,1,1)
            ceegrid_advanced_plotting(compmap(:,cm),1,{Data_Struct.chanlocs.labels},[]);
            title(sprintf('%d Component Map ',opt.cmp_nr(cm)))
        else
            
            subplot(2,1,1)
            topoplot(compmap(:,cm),Data_Struct.chanlocs);
            title(sprintf('%d Component Map ',opt.cmp_nr(cm)))
        end
        subplot(2,1,2)
        plot(timevec,mean(compts(cm,:,:),3));
        set(gca,'XTick', linspace(0,length(time_vec),10),'XTickLabel',linspce(time_vec(1,1),time_vec(1,end),10))
        xlabel('Time in seconds')
        ylabel('a.u.')
    end

elseif opt.plt && ~isempty(opt.filt) %plots also the power spectra
    for cm = 1:length(opt.cmp_nr)
        %compute the power spectra
        cmppw = abs( fft(squeeze(compts(cm,:,:)),[],1) ).^2;
        cmppw_av = squeeze(mean(cmppw,2));
        hz = linspace(0,Data_Struct.srate/2,size(compts,2)/2);
        figure(cm),clf
        
         %check if input is cEEGrid data
        if opt.cgrid
            subplot(2,1,1)
            ceegrid_advanced_plotting(compmap(:,cm),1,{Data_Struct.chanlocs.labels},[]);
            title(sprintf('%d Component Map for filter range [%d %d]',opt.cmp_nr(cm),opt.filt(1,1), opt.filt(1,2)))
        else
            
            subplot(2,1,1)
            topoplot(compmap(:,cm),Data_Struct.chanlocs);
            title(sprintf('%d Component Map for filter range [%d %d]',opt.cmp_nr(cm),opt.filt(1,1), opt.filt(1,2)))
        end
        
        subplot(2,1,2)
        plot(hz,cmppw_av(1:length(hz))/max(cmppw_av(1:length(hz))), 'linew',2)
        xlabel('Frequency (Hz)')
        ylabel('Power (normalized by max avg. power)')
        set(gca,'Xlim',[0 40]);
    end
end
 
         
%% stuff to add
% - can only add one marker at the moment in the GUI
%% 
%for the following functions cite the BBCI toolbox
%make the varagin input a bit more managable
function opt = property2list(varargin)

if nargin==0
  % Return an empty struct without identification tag
  opt= [];
  return;
end

if isstruct(varargin{1}) || isempty(varargin{1})
  % First input argument is already a structure: Start with that, write
  % the additional fields
  opt= varargin{1};
  iListOffset= 1;
else
  % First argument is not a structure: Assume this is the start of the
  % parameter/value list
  opt = [];
  iListOffset = 0;
end
% Write the identification field. ID field contains a 'version number' of
% how parameters are passed.
opt.isPropertyStruct = 1;

nFields= (nargin-iListOffset)/2;
if nFields~=round(nFields)
  error('Invalid parameter/value list');
end

for ff= 1:nFields
  fld = varargin{iListOffset+2*ff-1};
  if ~ischar(fld)
    error(sprintf('String required on position %i of the parameter/value list', ...
                  iListOffset+2*ff-1));
  end
  prp= varargin{iListOffset+2*ff};
  opt= setfield(opt, fld, prp);
end

function opt = set_default(opt,varargin)
if length(opt)>1
  error('first argument must be a 1x1 struct');
end

% Set 'isdefault' to ones for the field already present in 'opt'
isdefault= [];
if ~isempty(opt)
  for Fld=fieldnames(opt)'
    isdefault= setfield(isdefault, Fld{1}, 0);
  end
end

% Check if we have a  field/value list
if length(varargin) > 1
  
  % If the target is a propertylist structure use propertylist2struct to
  % convert the property list to a defopt structure.
  if (ispropertystruct(opt))
    defopt = propertylist2struct(varargin{:});
      
  else  % otherwise construct defopt from scratch
    
    
    % Create a dummy defopt structure: a terrible Matlab hack to overcome
    % impossibility of incremental update of an empty structure.
    defopt = struct('matlabsucks','foo');
  
    % Check consistency of a field/value list: even number of arguments
    nArgs= length(varargin)/2;
    if nArgs~=round(nArgs) && length(varargin~=1)
      error('inconsistent field/value list');
    end
    
    % Write a temporary defopt structure
    for ii= 1:nArgs
      defopt= setfield(defopt, varargin{ii*2-1}, varargin{ii*2});
    end
    
    % Remove the dummy field from defopt
    defopt = rmfield(defopt,'matlabsucks');
  end
  
else  
  
  % If varargin has only one element, it must be a defopt structure.
  defopt = varargin{1};
  
end
  
% Replace the missing fields in 'opt' from their 'defopt' counterparts. 
for Fld=fieldnames(defopt)'
  fld= Fld{1};
  if ~isfield(opt, fld)
    opt= setfield(opt, fld, getfield(defopt, fld));
    isdefault= setfield(isdefault, fld, 1);
  end
end

% Check if some fields in 'opt' are missing in 'defopt': possibly wrong
% options.
for Fld=fieldnames(opt)'
  fld= Fld{1};
  if ~isfield(defopt,fld)
%    warning('set_defaults:DEFAULT_FLD',['field ''' fld ''' does not have a valid default option']);
  end
end



