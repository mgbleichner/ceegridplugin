% cEEGrid_multiplot(EEG, nargin); - uses fieldtrip functions to plot data in the cEEGrid shape.
%
% Usage:
% >> OUTEEG =cEEGrid_multiplot(EEG,DataLabels,cfg,varargin)
%
% Inputs:
%   EEG         - input EEG dataset
%   DataLabels  - names of the channels that are to be plotted 
%   cfg         - contains all parameters for plotting
%   cfg paratmeters:
% Parameter:         Var-Type   possible values                                         default values           description
% 'showlabels'	 	   'string'	 {'yes','no'}                                           'no';
% 'showscale'          'string'	 {'yes','no'}                                           'yes';
% 'showcomment'	 	   'string'	 {'yes','no'}                                           'yes';
% 'showoutline'	 	   'string'	 {'yes','no'}                                           'yes';
% 'newfig'             'string'	 {'yes','no'}                                           'yes';
% 'axes'               'string'	 {'yes','no'}                                           'yes';
% 'box'                'string'	 {'yes','no'}                                           'no';
% 'interactive'	 	   'string'	 {'yes','no'}                                           'yes';
% 'zoom'               'string'	 {'yes','no'}                                           'no';
% 'comment'            'string'	 []                                                     '';
% 'xlim'      {'string' 'real'}	 'maxmin' or [xmin xmax]                                    'maxmin';
% 'ylim'      {'string' 'real'}	 {'maxmin', 'maxabs', 'zeromax', 'minzero'} or [ymin ymax]    'maxmin';
% 'fontsize'           'real'    []                                                     8;
% 'linewidth'          'real'    []                                                     2;
% 'side'               'string'	 []                                                     'cEEGridLayout';
% 'layout'             'struct'	 []                                                     layout struct that is selected with 'side'
%
% Parameters can be passed to this function via key-value pair. E.g.:
% cEEGrid_multiplot(EEG,[],[], 'showcomment','no','box','yes')
%
% Parameters can also be passed via cfg_filt like:
% cfg.showcomment='no';
% cfg.box='yes';
% 
% cEEGrid_multiplot(EEG,[],cfg)
% parameters passed via cfg_filt are overwritten by the key value pairs (if
% matching parameters are entered as key value pair.)


% Outputs:
%   OUTEEG        - output dataset
%
% See also:
%    EEGLAB
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = cEEGrid_multiplot(EEG,DataLabels,cfg,varargin)

com = ''; % this initialization ensure that the function will return something

if nargin < 1
    help cEEGrid_multiplot;
    return;
end

%% Check for fieldtrip dependencies
if ~plugin_status('fieldtrip-lite')
    fprintf('The fieldtrip-lite plugin is not installed. It will be installed automatically now.\n');
    
    plugin_askinstall('fieldtrip-lite', [], false);
end
if isempty(DataLabels)
    call_cfg='[]';
else
    call_cfg=['{' vararg2str(DataLabels) '}'];
end
if isempty(cfg)
    call_cfg=[call_cfg ',[]'];
else
    call_cfg=[call_cfg ',' vararg2str(cfg_filt)];
end
if nargin > 3 
    call_cfg= strcat(call_cfg, [',' vararg2str(varargin)]);
end
%%
% get all layouts from the layouts directory
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);
layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
layoutoptions=[''];
for k=1:numel(layouts)
    layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
    if k<numel(layouts)
        layoutoptions=strcat(layoutoptions,'|');
    end
end
%% Default values (they need to be defined before inputcheck, bc otherwise cfg cant be passed to this function w/o beeing overwritten)
if ~isfield(cfg,'showlabels');          cfg.showlabels          ='no'; end
if ~isfield(cfg,'showscale');           cfg.showscale           ='yes'; end
if ~isfield(cfg,'showcomment');         cfg.showcomment         ='yes'; end
if ~isfield(cfg,'showoutline');         cfg.showoutline         ='yes'; end
if ~isfield(cfg,'newfig');              cfg.newfig              ='no'; end
if ~isfield(cfg,'axes');                cfg.axes                ='yes'; end
if ~isfield(cfg,'box');                 cfg.box                 ='no'; end
if ~isfield(cfg,'interactive');         cfg.interactive         ='yes'; end
if ~isfield(cfg,'zoom');                cfg.zoom                ='no'; end
if ~isfield(cfg,'comment');             cfg.comment             =''; end
if ~isfield(cfg,'xlim');                cfg.xlim                ='maxmin'; end
if ~isfield(cfg,'ylim');                cfg.ylim                ='maxmin'; end
if ~isfield(cfg,'fontsize');            cfg.fontsize            =8; end
if ~isfield(cfg,'linewidth');           cfg.linewidth           =2; end
if ~isfield(cfg,'side')                 cfg.side = 'cEEGridLayout';end
if ~isfield(cfg,'layout')
    layoutFile=load([currentpath filesep 'Layouts' filesep cfg.side]);
    fields=fieldnames(layoutFile);
    cfg.layout= layoutFile.(fields{1});
end
if ~isfield(cfg,'study');               cfg.study = 'no';end
if ~isfield(cfg,'dimVec');              cfg.dimVec = [];end
%% check inputs 

fieldlist = {   'showlabels'	 	'string'	 [] 	 cfg.showlabels;
     'showscale'	 	'string'	 [] 	 cfg.showscale;
     'showcomment'	 	'string'	 [] 	 cfg.showcomment;
     'showoutline'	 	'string'	 [] 	 cfg.showoutline;
     'newfig'           'string'	 [] 	 cfg.newfig;
     'axes'             'string'	 [] 	 cfg.axes;
     'box'              'string'	 [] 	 cfg.box;
     'interactive'	 	'string'	 [] 	 cfg.interactive;
     'zoom'             'string'	 [] 	 cfg.zoom;
     'comment'          'string'	 [] 	 cfg.comment;
     'xlim'             {'string' 'real'}	 [] 	 cfg.xlim;
     'ylim'             {'string' 'real'}	 [] 	 cfg.ylim;
     'fontsize'         'real'       [0 Inf] 	 cfg.fontsize;
     'linewidth'	 	'real'       [0 Inf] 	 cfg.linewidth;
     'side'             'string'	 [] 	 cfg.side;
     'layout'           'struct'	 [] 	 cfg.layout;
     'study'            'string'     []      cfg.study;
     'dimVec'           'real'       []      cfg.dimVec};
[cfg varargin] = finputcheck( varargin, fieldlist, 'cEEGrid_multiplot', 'ignore');







    %cfg.side = layoutname;
    %DataLabels={EEG.chanlocs.labels};
    

    %% Plot
    if strcmp(cfg.study,'yes')%check if function is called from pop_ceegrid_advanced_plotting_std
       dimVec=cfg.dimVec;
       Data=EEG;
    else
        if isempty(DataLabels)
            DataLabels={EEG.chanlocs.labels};
        end
        dimVec = EEG.times;
        Data = mean(EEG.data,3);
    end
% use either all channels or the ones specified
if isempty(varargin)
    ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg);
else
    ceegrid_advanced_plotting(Data,dimVec,DataLabels,cfg,varargin);
end

% return the string command
% -------------------------
%com = sprintf('pop_cEEGrid_multiplot( %s, %s );', inputname(1), vararg2str(cfg.side));
%com = sprintf(['cEEGrid_multiplot(' inputname(1) ',[' vararg2str(dimVec) '],{' vararg2str(DataLabels) '},' vararg2str(cfg) fastif(strcmp(vararg2str(varargin),''), '' ,',') vararg2str(varargin) ');']);
com = sprintf('cEEGrid_multiplot(%s,%s);', inputname(1),call_cfg);
return;