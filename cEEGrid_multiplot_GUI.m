%this function is to build the GUI for pop_cEEGrid_multiplot.m
% Inputs:
% -Labels       Array of all Labels from Dataset
% -num_chan     Number of channels
% 
% Outputs:
% -plot_call            string containing the config structure and the key
%                           value pairs for the plot options
% -DataLabels           Array with selected channel names
%
% Usage:
% Labels={EEG.chanlocs.labels};
% [plot_call, DataLabels]= cEEGrid_multiplot_GUI(Labels,EEG.nbchan); 
%
% For more information check pop_cEEGrid_multiplot.m
%
% Copyright (C) 2019 Martin G. Bleichner
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
function [plot_call, DataLabels]= cEEGrid_multiplot_GUI(Labels,num_chan)
plot_call=[];
DataLabels=[];
%%
    % get all layouts from the layouts directory
    scriptName = mfilename('fullpath');
    [currentpath, filename, fileextension]= fileparts(scriptName);
    layouts=dir([currentpath filesep 'Layouts',filesep '*.mat']);
    layoutoptions=[''];
    for k=1:numel(layouts)
        layoutoptions=strcat(layoutoptions,layouts(k).name(1:end-4));
        if k<numel(layouts)
            layoutoptions=strcat(layoutoptions,'|');
        end
    end
    
    %% GUI setup
    
    
    guititle='cEEGrid multiplot-- pop_cEEGrid_multiplot';
    %     geom={[1 1] [1 1] [1 1]  [1] [1]};
    geom={[1 1] [1 1] [1 1]  [1] [1] [1 1 1 1 1]  [1 1 1 1 1] [1 1 1 1 1] [1 1 2 1] [2 1 1 1 ] [2 1 1 1] [1] [3 1 1] [3 1 1]};
    
    %default values for GUI inputs:
    editwhichchannels = '';
%     editplottitle = ~fastif(~isempty(EEG.setname), [EEG.setname], '');
    editplottitle='';
    popup_layoutoptions_value=1;
    checkbox_showlabels_value=0;
    checkbox_showoutline_value=1;
    checkbox_showscale_value=1;
    checkbox_newfig_value=0;
    checkbox_axes_value=1;
    checkbox_box_value=0;
    checkbox_zoom_value=0;
    checkbox_interactive_value=1;
    checkbox_showcomment_value=1;
    edit_comment='';
    edit_fontsize='8';
    edit_linewidth='2';
    edit_hlim='maxmin';
    edit_vlim='maxmin';
    correct_inputs=false;
    while correct_inputs==false % recreate GUI, until all GUI inputs are correct, or function is returned
        correct_inputs=true; %so far, all inputs are correct. This will be set to false later, if a wrong input value is detected
        uilist={
            { 'style', 'text', 'string', 'Channels subset (Vector containing channel numbers, []=all) ' , 'fontweight', 'bold'},...
            { 'style', 'edit', 'string', editwhichchannels , 'tag' 'channels' }...
            { 'style'   'text'     'string'    'Plot title', 'fontweight', 'bold' }, ...
            { 'style'   'edit'     'string'   editplottitle 'tag' 'plottitle' } ...
            { 'style', 'text', 'string', 'Select Grid:', 'fontweight', 'bold' },...
            { 'style', 'popupmenu', 'string', layoutoptions 'value' popup_layoutoptions_value 'tag' 'layoutoptions'}...
            {}...
            { 'style'   'text'     'string'    'Additional plot options: ', 'fontweight', 'bold'  } ...
            {'Style'   'text'     'string' 'Plot appearence: ', 'fontweight', 'bold' }...
            {}...
            {}...
            {}...
            {'Style'   'text'     'string' 'Options for plot creation: ', 'fontweight', 'bold'}...
            { 'Style', 'checkbox', 'string' 'Labels on' 'value' checkbox_showlabels_value 'tag' 'showlabels' }, ...
            { 'Style', 'checkbox', 'string' 'Show outline' 'value' checkbox_showoutline_value 'tag' 'showoutline' }, ...
            { 'Style', 'checkbox', 'string' 'Show scale' 'value' checkbox_showscale_value 'tag' 'showscale' }, ...
            {}...
            { 'Style', 'checkbox', 'string' 'New figure for the plot' 'value' checkbox_newfig_value 'tag' 'newfig' } ...
            { 'Style', 'checkbox', 'string' 'Axes on' 'value' checkbox_axes_value 'tag' 'axes' }, ...
            { 'Style', 'checkbox', 'string' 'Box around plots' 'value' checkbox_box_value 'tag' 'box' }, ...
            { 'Style', 'checkbox', 'string' 'Larger plots' 'value' checkbox_zoom_value 'tag' 'zoom' } ...
            {},...
            { 'Style', 'checkbox', 'string' 'Create interactive plot' 'value' checkbox_interactive_value 'tag' 'interactive' }, ...
            { 'Style', 'checkbox', 'string' 'Show comment' 'value' checkbox_showcomment_value 'tag' 'showcomment' }, ...
            { 'Style'   'text'     'string' 'Comment ([]=date+time): '  }, ...
            { 'Style', 'edit', 'string', edit_comment 'tag' 'comment'  } ...
            {}...
            {'Style'   'text'     'string' 'Font size of comment and labels (if present) (default = 8): '}...
            {'Style', 'edit', 'string', edit_fontsize 'tag' 'fontsize'}...
            {}...
            {}...
            {'Style'   'text'     'string' 'Linewidth in points (default = 2): '}...
            {'Style', 'edit', 'string', edit_linewidth 'tag' 'linewidth'}...
            {}...
            {}...
            {'Style'   'text'     'string' 'Axis limits: ', 'fontweight', 'bold'}...
            {'Style'   'text'     'string' 'X-Limits: ''maxmin'' or [xmin xmax] (default = ''maxmin''): '}...
            {'Style', 'edit', 'string', edit_hlim 'tag' 'hlim'}...
            {}...
            {'Style'   'text'     'string' 'Y-Limits: ''maxmin'', ''maxabs'', ''zeromax'', ''minzero'', or [ymin ymax] (default = ''maxmin'') '}...
            {'Style', 'edit', 'string', edit_vlim 'tag' 'vlim'}...
            {}...
            };
        
        [result userdat strhalt outstruct]=inputgui( geom ,  uilist, 'pophelp(''pop_cEEGrid_multiplot'')', guititle, [], 'normal');
        
        % catch cancelling-event
        if isempty(result), return; end
        
        % if no config file is given make empty config file
        %if nargin < 3
        %    cfg = [];
        %end
        
        
        if result{3}
            % set layout file
            cfg.side = layouts((result{3})).name(1:end-4); % get the layout file
        end
        
        if isempty(result{1})
%             DataLabels={EEG.chanlocs.labels};
            DataLabels=[];
        else
            %DataLabels=strsplit(result{1});
            % First check if channel vector is in correct format
            founderror=false;
            if ~isempty(str2num(result{1}))
                values=str2num(result{1});
                if min(values)<1
                    founderror=true;
                end
            end
            if ((isempty(str2num(result{1})))||(numel(str2num(result{1}))<1)||founderror||(numel(str2num(result{1}))>num_chan)||(max(str2num(result{1}))>num_chan)||max(histcounts(str2num(result{1})))>1)
                
                correct_inputs=false;
                %error('To select channels you have to enter an array of channel indices, e.g.: ''1 2 3 4 5 9''. The array must contain at least 3 values');
                errordlg2(sprintf('To select channels you have to enter an array of unique channel indices, e.g.: ''1 2 3 4 5 9''. \nThe array must contain minimum 1 and maximum %d integer values between 1 and %d.',num_chan,num_chan), 'Error in channel selection');
            else %channel vector seems to be right.
                
                DataLabels=Labels(str2num(result{1}));
            end
        end
        
        %% get additional fieldtrip cfg options from gui:
        yes_no = { 'no','yes'};
        cfg.showlabels = yes_no{outstruct.showlabels+1};
        cfg.showoutline = yes_no{outstruct.showoutline+1};
        cfg.showscale = yes_no{outstruct.showscale+1};
        cfg.showcomment = yes_no{outstruct.showcomment+1};
        cfg.newfig = yes_no{outstruct.newfig+1};
        cfg.axes = yes_no{outstruct.axes+1};
        cfg.box = yes_no{outstruct.box+1};
        cfg.interactive = yes_no{outstruct.interactive+1};
        cfg.zoom = yes_no{outstruct.zoom+1};
        cfg.comment = outstruct.comment;
        hlim=outstruct.hlim;
        if ~strcmp(outstruct.hlim,'maxmin')
            hlim=str2num(outstruct.hlim);
        end
        vlim=outstruct.vlim;
        if max(strcmp(outstruct.vlim,{'maxmin', 'maxabs', 'zeromax', 'minzero'}))==0
            vlim=str2num(outstruct.vlim);
        end
        
        cfg.xlim = hlim;
        cfg.ylim = vlim;
        cfg.fontsize = str2num(outstruct.fontsize);
        cfg.linewidth = str2num(outstruct.linewidth);
        %% check for wrong inputs
        if min([cfg.fontsize cfg.linewidth])<=0||max(isnan([cfg.fontsize cfg.linewidth]))==1
            correct_inputs=false;
            errordlg2(sprintf('Font size and linewidth values should be finite numbers larger than 1.'),'Error in font size and/or linewidth');
        elseif max(strcmp({'maxmin', 'maxabs', 'zeromax', 'minzero'},outstruct.vlim))==0&&(isempty(str2num(outstruct.vlim))||numel(str2num(outstruct.vlim))~=2||(numel(str2num(outstruct.vlim))==2&&vlim(1)>=vlim(2)))
            correct_inputs=false;
            errordlg2(sprintf('Y-Limits has to be out of [''maxmin'', ''maxabs'', ''zeromax'', ''minzero''], \nor [ymin ymax], where ymax > ymin.'),'Error in Y-Limits input');
        elseif max(strcmp({'maxmin'},outstruct.hlim))==0&&(isempty(str2num(outstruct.hlim))||numel(str2num(outstruct.hlim))~=2||(numel(str2num(outstruct.hlim))==2&&hlim(1)>=hlim(2)))
            correct_inputs=false;
            errordlg2(sprintf('X-Limits has to be ''maxmin'' or [ymin ymax], where ymax > ymin.'),'Error in X-Limits input');
        end
        %% In case of wrong inputs, get inputs from GUI to rebuild GUI with entered values
        if ~correct_inputs
            editwhichchannels = outstruct.channels;
            editplottitle = outstruct.plottitle;
            popup_layoutoptions_value=outstruct.layoutoptions;
            checkbox_showlabels_value=outstruct.showlabels;
            checkbox_showoutline_value=outstruct.showoutline;
            checkbox_showscale_value=outstruct.showscale;
            checkbox_newfig_value=outstruct.newfig;
            checkbox_axes_value=outstruct.axes;
            checkbox_box_value=outstruct.box;
            checkbox_zoom_value=outstruct.zoom;
            checkbox_interactive_value=outstruct.interactive;
            checkbox_showcomment_value=outstruct.showcomment;
            edit_comment=outstruct.comment;
            edit_fontsize=outstruct.fontsize;
            edit_linewidth=outstruct.linewidth;
            edit_hlim=outstruct.hlim;
            edit_vlim=outstruct.vlim;
        end
        
    end
    %% Prepare function call
    % default reference values (taken from cEEGrid_multiplot.m)
    cfg_ref=[];
    if ~isfield(cfg_ref,'showlabels');          cfg_ref.showlabels          ='no'; end
    if ~isfield(cfg_ref,'showscale');           cfg_ref.showscale           ='yes'; end
    if ~isfield(cfg_ref,'showcomment');         cfg_ref.showcomment         ='yes'; end
    if ~isfield(cfg_ref,'showoutline');         cfg_ref.showoutline         ='yes'; end
    if ~isfield(cfg_ref,'newfig');              cfg_ref.newfig              ='no'; end
    if ~isfield(cfg_ref,'axes');                cfg_ref.axes                ='yes'; end
    if ~isfield(cfg_ref,'box');                 cfg_ref.box                 ='no'; end
    if ~isfield(cfg_ref,'interactive');         cfg_ref.interactive         ='yes'; end
    if ~isfield(cfg_ref,'zoom');                cfg_ref.zoom                ='no'; end
    if ~isfield(cfg_ref,'comment');             cfg_ref.comment             =''; end
    if ~isfield(cfg_ref,'xlim');                cfg_ref.xlim                ='maxmin'; end
    if ~isfield(cfg_ref,'ylim');                cfg_ref.ylim                ='maxmin'; end
    if ~isfield(cfg_ref,'fontsize');            cfg_ref.fontsize            =8; end
    if ~isfield(cfg_ref,'linewidth');           cfg_ref.linewidth           =2; end
    if ~isfield(cfg_ref,'side')                 cfg_ref.side = 'cEEGridLayout';end
    if ~isfield(cfg_ref,'layout')
        layoutFile=load([currentpath filesep 'Layouts' filesep cfg_ref.side]);
        fields=fieldnames(layoutFile);
        cfg_ref.layout= layoutFile.(fields{1});
    end
    fieldnames_cfg= fieldnames(cfg);
    arg_string='';
    %compare inputs from gui to default values
    for i=1:numel(fieldnames_cfg)
        %check if field is string or number
        if ischar(cfg.(fieldnames_cfg{i}))
            if ~strcmp(cfg.(fieldnames_cfg{i}),cfg_ref.(fieldnames_cfg{i}))
                %['found difference in cfg.' fieldnames_cfg{i}]
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', ''' cfg.(fieldnames_cfg{i}) '''']);
            end
            
        elseif numel(cfg_ref.(fieldnames_cfg{i}))>1
            if numel(cfg.(fieldnames_cfg{i}))== numel(cfg_ref.(fieldnames_cfg{i}))
                if cfg.(fieldnames_cfg{i})~=cfg_ref.(fieldnames_cfg{i})
                    arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', [' num2str(cfg.(fieldnames_cfg{i})) ']' ]);
                end
            else
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', [' num2str(cfg.(fieldnames_cfg{i})) ']' ]);
            end
        elseif cfg.(fieldnames_cfg{i})~=cfg_ref.(fieldnames_cfg{i})
                %['found difference in cfg.' fieldnames_cfg{i}]
                arg_string=strcat(arg_string,[', ''' fieldnames_cfg{i} ''', ' num2str(cfg.(fieldnames_cfg{i})) ]);
            
        end
    end
    plot_call=[',[]' arg_string]; % Plot call containing the parameters that differ from default values
