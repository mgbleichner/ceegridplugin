function ceegrid_quality_check(INEEG)
% You can make certain assumption regarding the between channel correlations. See Denk et al. 2018
% for the grid with the reference electrodes. There is a negative correlation between
% the between channel angle (relative to the reference electrode) and the channel correlation. 
% for the left grid. The between channel distance is the best indicated for between channel correlations. 
% The larger the distance the smaler the between channel correlation. 
REJ=2;
% cut data into regular Epochs
EEG = eeg_regepochs(INEEG, 'recurrence', 1, 'eventtype', '999');
% remove extreme events
EEG = pop_jointprob(EEG,1,1:size(EEG.data,1),REJ,REJ,0,0);
EEG = eeg_rejsuperpose(EEG,1,1,1,1,1,1,1,1);
EEG = pop_eegthresh(EEG,1,[1:EEG.nbchan] ,-20,20,-0.5,1,0,0);
EEG = pop_rejepoch(EEG,EEG.reject.rejglobal,0);

for k= 1 :EEG.trials
    betweenChannelCorrelation(:,:,k)=corrcoef(EEG.data(:,:,k)');
    betweenChannelCov(:,:,k)=cov(squeeze(EEG.data(:,:,k))');
end

figure('units','normalized','outerposition',[0 0 1 1])
% subplot(121)
% imagesc(median(betweenChannelCov,3))
% set(gcf,'Color','w')
% title('Between Channel Covariance')
% set(gca,'XTickLabel', {EEG.chanlocs.labels},'XTick', 1:EEG.nbchan)
% set(gca,'YTickLabel', {EEG.chanlocs.labels},'YTick', 1:EEG.nbchan)
% title(['Based on ' num2str(EEG.trials) ' 1 second trials'])
% axis square
% colorbar

subplot(3,2,1)
imagesc(median(betweenChannelCorrelation,3))
set(gcf,'Color','w')
title('Between Channel Correlation')
set(gca,'XTickLabel', {EEG.chanlocs.labels},'XTick', 1:EEG.nbchan)
set(gca,'YTickLabel', {EEG.chanlocs.labels},'YTick', 1:EEG.nbchan)
axis square
title(['Based on ' num2str(EEG.trials) ' 1 second trials'])
colorbar
%%
%% Angle Distance Resolved
clear el distances angles

el(1,:)=[1778 231];%starting (top right)
el(2,:)=[1669 76 ];
el(3,:)=[1491 76];
el(4,:)=[1384 231];
el(5,:)=[1356 353];
el(6,:)=[1356 475];
el(7,:)=[1384 592];
el(8,:)=[1491 749];
el(9,:)=[1669 749];
el(10,:)=[1778 592];
%a=max(el)-min(el)
%figure;scatter(el(:,1),el(:,2))

for k=1:10
    
    for l=1:10
        v=el(k,:);
        u=el(l,:);
        %compute distance between electrodes
        distances(k,l)=pdist([u; v])/10;
        %compute angle between electrodes
        angles(k,l) =rad2deg(atan2(abs((u(2)-v(2))),abs(u(1)-v(1))));

        if k==l
            angles(k,l)=NaN;
        end
        %angles relative to reference

        P0=el(6,:);
        P1=el(k,:);
        P2=el(l,:);

        n1=(P2-P0)/norm(P2-P0);
        n2=(P1-P0)/norm(P1-P0);

        angle3 = rad2deg(atan2(norm(det([n2; n1])), dot(n1, n2)));
        relativeRefAngles(k,l)=angle3;
        
        % midpoint difference
        P1mid=[(P0(1)+P1(1))/2,(P0(2)+P1(2))/2];
        P2mid=[(P0(1)+P2(1))/2,(P0(2)+P2(2))/2];
        midPointDifference(k,l)=pdist([P1mid; P2mid]);
        % not very interesting da sehr stumpfe und sehr spitze winkel
        % jeweils eine kleine fläche haben. 
        areaPoint(k,l)=polyarea([P0(1) P1(1) P2(1)],[P0(2) P1(2) P2(2)]);
        % channelnames
        anglesNames(k,l)=str2num([num2str(k) num2str(l)]);

    end
end


relativeRefAnglesLeftOnly=relativeRefAngles([1:10],[1:10]);
DistancesLeftOnly=distances([1:10],[1:10]);
anglesNamesLeft=anglesNames([1:10],[1:10]);



relativeRefAnglesReOnly=relativeRefAngles([1 2 3 4 7 8 9 10],[1 2 3 4 7 8 9 10]);
midPointDifferenceOnly=midPointDifference([1 2 3 4 7 8 9 10],[1 2 3 4 7 8 9 10]);
areaPointOnly=areaPoint([1 2 3 4 7 8 9 10],[1 2 3 4 7 8 9 10]);
DistancesReOnly=distances([1 2 3 4 7 8 9 10],[1 2 3 4 7 8 9 10]);
anglesNames=anglesNames([1 2 3 4 7 8 9 10],[1 2 3 4 7 8 9 10]);


cMidDist=midPointDifferenceOnly(~tril(ones(8,8)));
cDist=DistancesReOnly(~tril(ones(8,8)) );
cAng=relativeRefAnglesReOnly(~tril(ones(8,8)) );
cArea=areaPointOnly(~tril(ones(8,8)) ); 
medianBCC=median(betweenChannelCorrelation(1:8,1:8,:),3);
cNames=anglesNames(~tril(ones(8,8)));
cCor=medianBCC(~tril(ones(8,8)) );

subplot(3,2,4)
hold all
scatter(cAng,cCor,'k','LineWidth',2)

 b = num2str(cNames); c = cellstr(b);
 dx = 0.1; dy = mean(cCor)*0.01; % displacement so the text does not overlay the data points
text(double(cAng+dx), double(cCor+dy), c);

[r,h]=corr(cAng,cCor,'type','Spearman');
xf = [min(cAng), max(cAng)];
plot(xf, polyval(polyfit(cAng,cCor,1), xf));
xlabel('Interelectrode Angle [degree]')
ylabel('Correlation coefficient [r]')
axis square
set(gcf,'Color','w')
title(['Between Channel Correlation; r = ' num2str(r)] )

subplot(3,2,6)
scatter(cDist,cCor,'k','LineWidth',2)
hold all
 b = num2str(cNames); c = cellstr(b);
 dx = 0.1; dy = mean(cCor)*0.01; % displacement so the text does not overlay the data points
text(double(cDist+dx), double(cCor+dy), c);
[r,h]=corr(cDist,cCor,'type','Spearman');
xf = [min(cDist), max(cDist)];
plot(xf, polyval(polyfit(cDist,cCor,1), xf));

xlabel('Interelectrode Distance [mm]')
ylabel('Correlation coefficient [r]')
axis square
set(gcf,'Color','w')
title(['Between Channel Correlation; r = ' num2str(r)] )
%%
%cMidDist=midPointDifferenceOnly(~tril(ones(10,10)));
cDist=DistancesLeftOnly(~tril(ones(10,10)) );
cAng=relativeRefAnglesLeftOnly(~tril(ones(10,10)) );

medianBCC=median(betweenChannelCorrelation(9:18,9:18,:),3);
cNames=anglesNamesLeft(~tril(ones(10,10)));
cCor=medianBCC(~tril(ones(10,10)) );

subplot(3,2,5)
hold all
scatter(cDist,cCor)

[r,h]=corr(cDist,cCor,'type','Spearman');

xlabel('Interelectrode Distance [mm]')
ylabel('Correlation coefficient [r]')
axis square
set(gcf,'Color','w')
title(['Between Channel Correlation; r = ' num2str(r)] )
ylim([0 1])
xf = [min(cDist), max(cDist)];
plot(xf, polyval(polyfit(cDist,cCor,1), xf));